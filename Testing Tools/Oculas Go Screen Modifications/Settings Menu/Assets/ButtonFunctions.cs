﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ButtonFunctions : MonoBehaviour
{

    public List<Text> Showcase = new List<Text>();
    public bool refreshButton;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(refreshButton == true)
        {
            for (int i = 0; i < Showcase.Count; i++)
            {
                Showcase[i].text = OVRManager.display.displayFrequency.ToString();
            }
        }
        else
        {
            for (int i = 0; i < Showcase.Count; i++)
            {
                Showcase[i].text = OVRManager.tiledMultiResLevel.ToString();
            }
        }
    }


    public void ChangeRefreshRate(float refreshRate)
    {
        OVRManager.display.displayFrequency = refreshRate;
    }

    public void ChangeFFR(int rate)
    {
        OVRManager.tiledMultiResLevel = (OVRManager.TiledMultiResLevel)rate; 
    }

}
