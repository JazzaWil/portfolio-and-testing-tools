﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class GameManager : MonoBehaviour
{
    public Transform platformGenerator;//Reference to our platform generator
    private Vector3 platformGeneratorStartPoint;//The starting position of the platform generator

    public PlayerController player;//Reference to our player
    private Vector3 playerStartPoint;//Starting position of the player

    private ObjectDestroyer[] platformArray;

    [Header("Scoring")]


    public TMP_Text coinTextScene;
    public TMP_Text distenceRunTextScene;

    [Space]

    public GameObject deathScreen;
    public TMP_Text coinTextDeath;
    public TMP_Text coinTotalTextDeath;
    public TMP_Text distenceTextDeath;
    public TMP_Text title;

    [Space]

    public bool scoreIncreasing = true;
    public float scorePerSecond;
    public int scoreCoins;
    public float scoreDistence;


    // Start is called before the first frame update
    void Start()
    {
        platformGeneratorStartPoint = platformGenerator.position;
        playerStartPoint = player.transform.position;
        
    }

    // Update is called once per frame
    void Update()
    {
        if(scoreIncreasing && player.gameStarted)
        {
            coinTextScene.gameObject.SetActive(true);
            distenceRunTextScene.gameObject.SetActive(true);
            scoreDistence += scorePerSecond * Time.deltaTime;
            coinTextScene.text = scoreCoins.ToString();
            distenceRunTextScene.text = scoreDistence.ToString("F0") + "M";
        }
    }

    public void ResetGame()
    {
        platformArray = FindObjectsOfType<ObjectDestroyer>();

        for (int i = 0; i < platformArray.Length; i++)
        {
            platformArray[i].gameObject.SetActive(false);
        }

        player.transform.position = playerStartPoint;
   

        player.ResetValues();
        player.gameStarted = true;
        platformGenerator.position = platformGeneratorStartPoint;

        scoreDistence = 0;
        scoreCoins = 0;
        scoreIncreasing = true;
        title.gameObject.SetActive(false);
    }

    public void DeathScreen()
    {
        deathScreen.SetActive(true);
        coinTextScene.gameObject.SetActive(false);
        distenceRunTextScene.gameObject.SetActive(false);
        scoreIncreasing = false;

        distenceTextDeath.text = scoreDistence.ToString("F0");

        if (PlayerPrefs.HasKey("HighScore") && PlayerPrefs.GetFloat("HighScore") < scoreDistence)
        {
            PlayerPrefs.SetFloat("HighScore", scoreDistence);
        }
        else if (!PlayerPrefs.HasKey("HighScore"))
        {
            PlayerPrefs.SetFloat("HighScore", scoreDistence);
        }

        coinTextDeath.text = scoreCoins.ToString();

        if (PlayerPrefs.HasKey("Coin"))
        {
            PlayerPrefs.SetInt("Coin", scoreCoins += PlayerPrefs.GetInt("Coin"));
        }
        else if (!PlayerPrefs.HasKey("Coin"))
        {
            PlayerPrefs.SetInt("Coin", scoreCoins);
        }

        coinTotalTextDeath.text = PlayerPrefs.GetInt("Coin").ToString();

        player.gameStarted = false;
        player.moveSpeed = 0;
        player.myRigidbody.velocity = Vector2.zero;
        title.gameObject.SetActive(true);
    }

    public void MainMenu()
    {
        platformArray = FindObjectsOfType<ObjectDestroyer>();

        for (int i = 0; i < platformArray.Length; i++)
        {
            platformArray[i].gameObject.SetActive(false);
        }

        player.transform.position = playerStartPoint;

        player.gameStarted = false;
        platformGenerator.position = platformGeneratorStartPoint;

        scoreDistence = 0;
        scoreCoins = 0;
        scoreIncreasing = true;
    }


    public void giveCoins(int amount)
    {
        scoreCoins += amount;
    }
}
