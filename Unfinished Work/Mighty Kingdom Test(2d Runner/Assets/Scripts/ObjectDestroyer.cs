﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectDestroyer : MonoBehaviour
{
    public GameObject platformDestructionPoint;//Point at where platforms start being destroyed

    // Start is called before the first frame update
    void Start()
    {
        platformDestructionPoint = GameObject.Find("Object Destruction Point ");//Finds the destruction point in the scene
    }

    // Update is called once per frame
    void Update()
    {
        if(transform.position.x < platformDestructionPoint.transform.position.x)//Checks if it can destroy any object thats less then the destruction point 
        {
            gameObject.SetActive(false);
        }
    }
}
