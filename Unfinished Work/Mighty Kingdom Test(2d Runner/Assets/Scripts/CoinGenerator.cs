﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinGenerator : MonoBehaviour
{
    public ObjectPooler coinPool;

    public float distenceBetweenCoins;


    public void SpawnCoins(Vector3 startPosition)
    {
        GameObject tempCoin = coinPool.GetPooledObject();
        tempCoin.transform.position = startPosition;
        tempCoin.SetActive(true);

        GameObject tempCoin2 = coinPool.GetPooledObject();
        tempCoin2.transform.position = new Vector3(startPosition.x - distenceBetweenCoins, startPosition.y, startPosition.z);
        tempCoin2.SetActive(true);

        GameObject tempCoin3 = coinPool.GetPooledObject();
        tempCoin3.transform.position = new Vector3(startPosition.x + distenceBetweenCoins, startPosition.y, startPosition.z);
        tempCoin3.SetActive(true);
    }
}
