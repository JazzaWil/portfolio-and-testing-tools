﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformGenerator : MonoBehaviour
{
    public Transform generationPoint;//Where we choose to spawn the new platform
    public float distenceBetweenMin;//A minimum value for our distence between each platform
    public float distenceBetweenMax;//A maximum value for our distence between each platform    public float distenceBetweenMin;//A minimum value for our distence between each platform
    public Transform maxHeightPoint;//The maximum point at which platforms can spawn
    public List<ObjectPooler> objectPools = new List<ObjectPooler>();//Our platform object pools
    public float maxHeightChange;//The amount at which platforms can move away from each other
    public float randomCoinThreshold;

    private float distenceBetweenPlatforms;//The distence between platforms when spawned
    private List<float> platformWidths = new List<float>();//The width of the platform
    private int platformSelector;//Which platform we are selecting
    private float heightMin;//A minimum value for our height of each platform
    private float heightMax;//A maximum value for our height of each platform
    private float heightChange;//The value applied to the platforms to chnage in height
    private CoinGenerator coinGenerator;




    // Start is called before the first frame update
    void Start()
    {
        Screen.SetResolution(1920, 1080,true);//sets resolution for device

        coinGenerator = FindObjectOfType<CoinGenerator>();

        for (int i = 0; i < objectPools.Count; i++)
        {
            platformWidths.Add(objectPools[i].GetPooledObject().GetComponent<BoxCollider2D>().size.x);//Gets the width of the platform from the collider
        }

        heightMin = transform.position.y;
        heightMax = maxHeightPoint.position.y;
    }

    // Update is called once per frame
    void Update()
    {
        if(transform.position.x < generationPoint.position.x)//Checks whether the position is less then the generation point
        {
            distenceBetweenPlatforms = Random.Range(distenceBetweenMin, distenceBetweenMax);//Grab a random distence for the platform
            platformSelector = Random.Range(0, objectPools.Count);//Get a random plaform

            heightChange = transform.position.y + Random.Range(maxHeightChange,-maxHeightChange);

            if(heightChange > heightMax)
            {
                heightChange = heightMax;
            }
            else if (heightChange < heightMin)
            {
                heightChange = heightMin;
            }

            transform.position = new Vector3(transform.position.x + (platformWidths[platformSelector] / 2) + distenceBetweenPlatforms, heightChange, transform.position.z);//set the generation point to be further away so new objects can spawn

            GameObject tempPlatform = objectPools[platformSelector].GetPooledObject();
            //set our platforms position and rotation then set it to active in scene
            tempPlatform.transform.position = transform.position;
            tempPlatform.transform.rotation = transform.rotation;
            tempPlatform.SetActive(true);

            if (Random.Range(0, 100) < randomCoinThreshold)
            {
                coinGenerator.SpawnCoins(new Vector3(transform.position.x,transform.position.y + 1f, transform.position.z));
            }

            transform.position = new Vector3(transform.position.x + (platformWidths[platformSelector] / 2), transform.position.y, transform.position.z);//set the generation point to be further away so new objects can spawn
        }
    } 
}
