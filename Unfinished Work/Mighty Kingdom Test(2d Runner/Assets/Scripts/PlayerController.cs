﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(BoxCollider2D))]
[RequireComponent(typeof(Animator))]


public class PlayerController : MonoBehaviour
{
    [Header("Speed Variables")]
    public float moveSpeed;//The speed at which the player moves
    public float moveSpeedDefault;//The default player speed
    public float speedMultiplier;//The multiplier at which the players speeds up
    public float speedMilestone;//Our milestone at when we increase the speed
    public float speedMilestoneDefault;//Our deault milestone'
    private float speedMilestoneIncrease;//The increase value for the speed milestone.

    [Space]

    [Header("Jump/Ground Variables")]
    public float jumpForce;//The force added to the player when jumping(How high the player can jump)
    public bool isGrounded;//Bool to check if players on a platform
    public LayerMask groundLayers;//A set of layers that defines what layers are platforms
    public float jumpTime;//The default jump time
    private float jumpTimeCounter;//The counter for when holding down how high the platyer will jump
    public float groundCheckRadius;//The radius we check for the ground
    public Transform groundCheck;//The transform at our feet
    private bool stoppedJumping;//Checks when we've stopped jumping
    private bool canDoubleJump;//Checks if we can double jump

    [Space]

    [Header("MISC")]
    public bool gameStarted;//Check if the player has started the game
    public GameManager gameManager;//Our Game Manager

    [HideInInspector]
    public Rigidbody2D myRigidbody; //The 2D Rigidbody attached to the player
   // private Collider2D myCollider; //The 2D Collider attached to the player
    private Animator myAnimator;//The Animator on the player




    // Start is called before the first frame update
    void Start()
    {
        myRigidbody = GetComponent<Rigidbody2D>();
      //  myCollider = GetComponent<Collider2D>();
        myAnimator = GetComponent<Animator>();
        jumpTimeCounter = jumpTime;
        speedMilestoneIncrease = speedMilestoneDefault = speedMilestone;
        moveSpeedDefault = moveSpeed;
        stoppedJumping = true;
        
    }

    // Update is called once per frame
    void Update()
    {
        myAnimator.SetBool("isGrounded", isGrounded);//Sets the grounded animation bool to the grounded bool in script so animations sync with player input


        if (gameStarted)
        {
            isGrounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, groundLayers);//Checks if we are grounded

            myRigidbody.velocity = new Vector2(moveSpeed, myRigidbody.velocity.y);//Setting the players speed

            if (isGrounded == true) // check if we are grounded 
            {
                jumpTimeCounter = jumpTime;
                canDoubleJump = true;
            }

#if UNITY_EDITOR
                if (Input.GetKeyDown(KeyCode.Space))// check if we are pressing the space button or touching the screen
#else
                if(Input.GetTouch(0).phase == TouchPhase.Began)
#endif
                {
                if (isGrounded == true)
                { 
                    myRigidbody.velocity = new Vector2(moveSpeed, jumpForce);//Setting the players jump and excuting the jump
                    stoppedJumping = false;

                }

                if(!isGrounded && canDoubleJump)
                {
                    myRigidbody.velocity = new Vector2(moveSpeed, jumpForce);//Setting the players jump and excuting the jump
                    canDoubleJump = false;
                    stoppedJumping = false;
                }
            }
#if UNITY_EDITOR
            if (Input.GetKey(KeyCode.Space) && !stoppedJumping)
#else
                 if ((Input.GetTouch(0).phase == TouchPhase.Stationary || Input.GetTouch(0).phase == TouchPhase.Moved) && !stoppedJumping)
#endif
            {
                if (jumpTimeCounter > 0)
                {
                    myRigidbody.velocity = new Vector2(moveSpeed, jumpForce);//Setting the players jump and excuting the jump
                    jumpTimeCounter -= Time.deltaTime;
                }

            }
#if UNITY_EDITOR
            if (Input.GetKeyUp(KeyCode.Space))
#else
                 if (Input.GetTouch(0).phase == TouchPhase.Ended || Input.GetTouch(0).phase == TouchPhase.Canceled)
#endif
            {
                jumpTimeCounter = 0;
                stoppedJumping = true;
            }


            if (transform.position.x > speedMilestone)
            {
                speedMilestone += speedMilestoneIncrease;
                speedMilestoneIncrease = speedMilestoneIncrease * speedMultiplier;
                moveSpeed = moveSpeed * speedMultiplier;
            }

        }
    }

    public void StartGame()
    {
        if (gameStarted == false)//Checks if we have started the game if not we change this bool and set our animator to start the run animation instead of the idle.
        {
            gameStarted = true;
            ResetValues();
            myAnimator.SetBool("gameStarted", true);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "KillBox")
        {
            gameManager.DeathScreen();
        }
    }

    public void ResetValues()
    {
        moveSpeed = moveSpeedDefault;
        speedMilestone = speedMilestoneIncrease = speedMilestoneDefault;
    }
}
