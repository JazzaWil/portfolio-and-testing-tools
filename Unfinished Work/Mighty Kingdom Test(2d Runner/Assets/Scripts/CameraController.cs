﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public PlayerController player;//The Player to target
    private float distenceToMove;//Distence to move towards player
    private Vector3 lastPlayerPosition;//The last known player position

    // Start is called before the first frame update
    void Start()
    {
        lastPlayerPosition = player.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        distenceToMove = player.transform.position.x - lastPlayerPosition.x ;

        transform.position = new Vector3(transform.position.x + distenceToMove, transform.position.y, transform.position.z);

        lastPlayerPosition = player.transform.position;
    }
}
