﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class TimelineScript : MonoBehaviour
{
    private PlayableDirector TimelineFinalStageTimeline;
    public GameObject timeFinalStageTimeline;
  

	// Use this for initialization
	void Start ()
    {
        TimelineFinalStageTimeline = GetComponent<PlayableDirector>();
	}

    // Update is called once per frame
    void OnTriggerExit(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            TimelineFinalStageTimeline.Stop();
        }

    }

     void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {

            TimelineFinalStageTimeline.Play();
        }
    }
}
