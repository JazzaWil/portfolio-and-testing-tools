﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AutomaticSceneChange : MonoBehaviour
{
    //Script to move scene after a cutsene plays

    public string TargetScene = "Main Scene";

    public float WaitTime = 1.0f;

    private float Timer = 0f;


	// Use this for initialization
	void Start ()
    {
        Timer = 0;
	}
	
	// Update is called once per frame
	void Update ()
    {
        Timer += Time.deltaTime;

        if(Timer > WaitTime)
        {
            SceneManager.LoadScene(TargetScene, LoadSceneMode.Single);
        }
    }
}
