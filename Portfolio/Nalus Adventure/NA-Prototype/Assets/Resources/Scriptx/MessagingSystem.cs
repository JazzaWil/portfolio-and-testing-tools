﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public struct Damage
{
    public float DamageValue { get; set; }
    public PowerUpTypes Type { get; set; }
    public Vector3 KnockBackValue { get; set; }
}

//Message to send damage
public interface IDamageMessage: IEventSystemHandler
{
    void SendDamage(Damage damage);
}

public interface ISpawnEnemy: IEventSystemHandler
{
    void ChangeSpawn(SpawnType newType);
}
