﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Rewired;


public class PlayerController : MonoBehaviour
{
    //movment and player facing
    public float speed = 10;
    public static Vector3 hitPoint;
    private Plane m_Plane;
    public Rigidbody rigiBody;

    [Tooltip("The Type of the Current Power Up")]
    public static PowerUpTypes CurrentPower = PowerUpTypes.Default;
    private SpawnType LastPower = SpawnType.None;

    private float meleeMaxCooldown;//cooldown time of melee attack
    private float meleeCooldownCounter;//counter for melee cooldown

    private float rangeMaxCooldown;//cooldown time of range attack
    private float rangeCooldownCounter;//counter for range cooldown
    
    //change to get componenet on awake & make private
    public GameObject Melee_Prefab;//drag prefabs for these attacks into the inspector
    public GameObject Range_Prefab;//drag prefabs for these attacks into the inspector

    [Tooltip("Offset when a powerup is spawned")]
    public Vector3 PowerUpOffset = new Vector3(2, 0, 2);

    [Tooltip("How long the dash lasts")]
    public float DashDuration = 0.1f;

    [Tooltip("Deadzone for movment")]
    public float MovementSensitivity = 0.35f;

    //Dash Variables
    public float dashTimer;
    private float defaultDashTimer;
    public Coroutine dashWaitTime;
    public float dashSpeed;
    private float tempspeed;

    [Header("Basic Attack Settings")]
    public float Default_MeleeCD;
    public float PowerUp1_MeleeCD;
    public float PowerUp2_MeleeCD;
    public float PowerUp3_MeleeCD;
    public float Default_RangeCD;
    public float PowerUp1_RangeCD;
    public float PowerUp2_RangeCD;
    public float PowerUp3_RangeCD;

    //the vertical and horizontal directions from the camera
    Vector3 CameraVert;
    Vector3 CameraHorizontal;

    private ObjectPool pool = null;

    [Header("Hats")]
    public GameObject Hardhat = null;
    public GameObject Partyhat = null;
    public GameObject Headband = null;

    //animation stuff
    private Animator animator;

    private Camera MainCamera;

    GameObject MeleeAttack;
    AttackActor attackActor;

    float naluSpeed;

    public static bool controllerOn = true;
    Vector3 controllerDir;

    private Player player;
    [Tooltip("This is the current Rewired player")]
    public int playerId = 0;
    private Vector3 moveVector;
    private bool melee;
    private bool range;
    private bool dash;




    void Start()
    {
        MainCamera = Camera.main;

        animator = GetComponent<Animator>();
        defaultDashTimer = dashTimer;
        dashTimer = 0;

        meleeMaxCooldown = Default_MeleeCD;
        rangeMaxCooldown = Default_RangeCD;

        //get camera componetarino
        //Camera MainCamera = GetComponentInChildren<Camera>();
        //Camera MainCamera = Camera.main;
        //setting the camera directions and ignoring the Y axis
        CameraVert = MainCamera.transform.forward;
        CameraVert.y = 0;
        CameraVert.Normalize();
        CameraHorizontal = MainCamera.transform.right;
        CameraHorizontal.y = 0;
        CameraHorizontal.Normalize();
        rigiBody = GetComponent<Rigidbody>();

        pool = FindObjectOfType<ObjectPool>();

        if(pool == null)
        {
            Debug.Log("Object pool not found!");
        }

        Hardhat.SetActive(false);
        Partyhat.SetActive(false);
        Headband.SetActive(false);

        MeleeAttack = Instantiate(Melee_Prefab, transform.position, Quaternion.identity);
        attackActor = MeleeAttack.GetComponent<AttackActor>();
        MeleeAttack.transform.SetParent(gameObject.transform);
        MeleeAttack.SetActive(false);


        player = ReInput.players.GetPlayer(playerId);
    }

    private void Update()
    {
    }

    void FixedUpdate()
    {
        GetInput();
        Movement();
        dashTimer -= Time.deltaTime;
        Attacks();
    }

    void Movement()
    {

 

        if (moveVector.x != 0.0f || moveVector.z != 0.0f)
        {
            gameObject.transform.position += (moveVector * Time.deltaTime) * speed;
        }
        if (dash == true)
        {
            if (dashWaitTime == null && dashTimer <= 0)
            {
                dashWaitTime = StartCoroutine(Wait(DashDuration));
                dashTimer = defaultDashTimer;
            }
        }
        naluSpeed = rigiBody.velocity.magnitude + moveVector.magnitude;
        animator.SetFloat("Blend", naluSpeed);

        // Debug.Log(Input.mousePosition);
        if (controllerOn == false)
        {
            Ray ray = MainCamera.ScreenPointToRay(Input.mousePosition);
            m_Plane = new Plane(Vector3.up, new Vector3(0, transform.position.y, 0));//creates a fictional plane on level with the player

            float enter = 0.0f;
            if (m_Plane.Raycast(ray, out enter))//casts ray to plane
            {
                hitPoint = ray.GetPoint(enter);//sets hitpoint to ray hit location
                
            }
        }
        else 
        {
            controllerDir = new Vector3(0, 0, 0);

            if (player.GetAxis("Aiming X") <= -0.1f * MovementSensitivity || player.GetAxis("Aiming X") >= 0.1f * MovementSensitivity)
            {
                controllerDir += CameraHorizontal * player.GetAxis("Aiming X");
            }
            if (player.GetAxis("Aiming Y") <= -0.1f * MovementSensitivity || player.GetAxis("Aiming Y") >= 0.1f * MovementSensitivity)
            {
                controllerDir += CameraVert * player.GetAxis("Aiming Y");

            }
           hitPoint = transform.position + controllerDir;
        }

        //Debug.Log(hitPoint);
        transform.LookAt(hitPoint);//faces toward mouse or controller
    }

    private void GetInput()
    {
        // Get the input from the Rewired Player. All controllers that the Player owns will contribute, so it doesn't matter
        // whether the input is coming from a joystick, the keyboard, mouse, or a custom controller.

        moveVector.x = player.GetAxis("Move Horizontal"); // get input by name or action id
        moveVector.z = player.GetAxis("Move Vertical");
        melee = player.GetButtonDown("Melee");
        range = player.GetButton("Range");
        dash = player.GetButtonDown("Dash");
    }

    private IEnumerator Wait(float seconds)
    {
        //calculate original speed then calulate dash speed, wait, set speed back to default
        tempspeed = speed;
        speed *= dashSpeed;
        yield return new WaitForSeconds(seconds);
        speed = tempspeed;
        dashWaitTime = null;
    }

    void Attacks()
    {
        //Instantiate melee attack prefab 
        //change to an always existing child, turn on/off
        if (melee == true && meleeCooldownCounter == meleeMaxCooldown) 
        {
            animator.SetTrigger("Attack");
            //GameObject tempAttack = Instantiate(Melee_Prefab, transform.position, Quaternion.identity);
            //tempAttack.transform.SetParent(gameObject.transform);
            MeleeAttack.SetActive(true);
            attackActor.testSetup();
            meleeCooldownCounter = 0;
        }

        //Instantiate range attack prefab 
        //replace with object pool
        //Debug.Log(range);
        if (range == true && rangeCooldownCounter == rangeMaxCooldown)
        {
            if (CurrentPower == PowerUpTypes.Fast && player.GetButtonDown("range") == true)
            {
                Instantiate(Range_Prefab, transform.position, Quaternion.identity);
                rangeCooldownCounter = 0;
                
            }
            if (CurrentPower != PowerUpTypes.Fast)
            {
                Instantiate(Range_Prefab, transform.position, Quaternion.identity);
                rangeCooldownCounter = 0;
                
            }
        }

        //Attack Cooldown
        if (meleeCooldownCounter != meleeMaxCooldown)//if cooldown is max save time by not running assignement operators
        {
            if (meleeCooldownCounter > meleeMaxCooldown)
            {
                meleeCooldownCounter = meleeMaxCooldown;
            }
            else
            {
                meleeCooldownCounter += Time.deltaTime;
            }
        }

        //Range Cooldown
        if (rangeCooldownCounter != rangeMaxCooldown)//if cooldown is max save time by not running assignement operators
        {
            if (rangeCooldownCounter > rangeMaxCooldown)
            {
                rangeCooldownCounter = rangeMaxCooldown;
            }
            else
            {
                rangeCooldownCounter += Time.deltaTime;
            }
        }
    }


    public void SetCurrrentType(PowerUpTypes NewType)
    {
        if(CurrentPower != NewType)
        {
            CurrentPower = NewType;

            //SpawnPowerUp if needed
            if (LastPower != SpawnType.None)
            {
                pool.SpawnFromPool(LastPower, transform.position + PowerUpOffset);
                PowerUpOffset *= -1;
            }

            //sets cooldown variables to the specific powerup values
            switch (CurrentPower)
            {
                case PowerUpTypes.Default:
                    meleeMaxCooldown = Default_MeleeCD;
                    rangeMaxCooldown = Default_RangeCD;
                    LastPower = SpawnType.None;

                    Hardhat.SetActive(false);
                    Partyhat.SetActive(false);
                    Headband.SetActive(false);

                    break;
                case PowerUpTypes.Strong:
                    meleeMaxCooldown = PowerUp1_MeleeCD;
                    rangeMaxCooldown = PowerUp1_RangeCD;
                    LastPower = SpawnType.PowerPower;

                    Hardhat.SetActive(true);
                    Partyhat.SetActive(false);
                    Headband.SetActive(false);

                    break;

                case PowerUpTypes.Fast:
                    meleeMaxCooldown = PowerUp2_MeleeCD;
                    rangeMaxCooldown = PowerUp2_RangeCD;
                    LastPower = SpawnType.PowerFast;

                    Hardhat.SetActive(false);
                    Partyhat.SetActive(false);
                    Headband.SetActive(true);

                    break;

                case PowerUpTypes.Strategic:
                    meleeMaxCooldown = PowerUp3_MeleeCD;
                    rangeMaxCooldown = PowerUp3_RangeCD;
                    LastPower = SpawnType.PowerStrat;

                    Hardhat.SetActive(false);
                    Partyhat.SetActive(true);
                    Headband.SetActive(false);

                    break;

            }
        }
    }

    //private void OnTriggerEnter(Collider other)
    //{
    //    Debug.Log("hit something");
    //}


}
