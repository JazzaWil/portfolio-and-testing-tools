﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class Coin : MonoBehaviour
{

    [Tooltip("Amount of score of the coin")]
    public int ScoreAmount = 1;

    [Tooltip("How fast the coin falls towards the floor")]
    public float FallSpeed = 1.0f;

    private Vector3 FallSpeedVector;

    [System.Serializable]
    public class PickUpEvent : UnityEvent<GameObject, SpawnType> { }

    [System.Serializable]
    public class ScoreEvent : UnityEvent<int> { }

    public ScoreEvent AddScore = null;

    public PickUpEvent onPickUp = null;

    //Whether or not the the coin needs to fall
    private bool hitfloor = false;

    // Use this for initialization
    void Awake()
    {
        //Set up the fall speed
        FallSpeedVector = new Vector3(0, FallSpeed, 0);

        //Set event to update the UI
        UImanager uiManager = FindObjectOfType<UImanager>();
        if (uiManager != null)
        {
            AddScore.AddListener(new UnityAction<int>(uiManager.UpdateScore));
        }

        //Set event to return coin to object pool
        ObjectPool objectPool = FindObjectOfType<ObjectPool>();
        if (objectPool != null)
        {
            onPickUp.AddListener(new UnityAction<GameObject, SpawnType>(objectPool.ReturnToPool));   
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        //Player adds coins to score and return coin to pool
        if (other.tag == "Player")
        {
            AddScore.Invoke(ScoreAmount);
            onPickUp.Invoke(gameObject, SpawnType.Coin);
            hitfloor = false;
        }

        //Turn off falling if it hits the floor
        if (other.tag == "Object")
        {
            hitfloor = true;
        }
    }
 
       
    public void FixedUpdate()
    {
        //Fall downwards
        if(!hitfloor)
        {
            transform.position -= FallSpeedVector;
        }
        
    }

}
