﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnterDamage : MonoBehaviour
{
    [Range (0.0f, 100.0f)]
    public float OnEnterDamage = 10;

    private Damage Damage;

    public bool StayDamage = false;

    public bool KnockBack = false;

    public float KnockBackScale = 1.0f;

    private void Start()
    {
        Damage.DamageValue = OnEnterDamage;
        Damage.KnockBackValue = new Vector3(0, 0, 0);
        Damage.Type = PowerUpTypes.Default;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" && !StayDamage)
        {
            if(KnockBack)
            {
                Damage.KnockBackValue = other.transform.forward * -1 * KnockBackScale;
            }
            
            other.SendMessage("SendDamage", Damage);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player" && StayDamage)
        {
            if (KnockBack)
            {
                Damage.KnockBackValue = other.transform.forward * -1 * KnockBackScale;
            }

            other.SendMessage("SendDamage", Damage);
        }
    }

}
