﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnkillableEnemy : MonoBehaviour, IDamageMessage
{
    Vector3 IntialPosition = new Vector3(0, 0, 0);

    public void Start()
    {
        IntialPosition = transform.position;   
    }

    public void SendDamage(Damage damage)
    {
    }

    public void Respawn()
    {
        transform.position = IntialPosition;
    }

}
