﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class WaveController : MonoBehaviour
{
    public enum WaveState {Start, PhaseOne, PhaseTwo, PhaseThree, Finish };

    [System.Serializable]
    public class Wave
    {
        [Tooltip("Name of the wave")]
        public string WaveName = "Wave";
        [Tooltip("Total Enemies of a given type that spawn")]
        public int WaveSize = 0;
        [Tooltip("Type of enemy the wave spawns from the objectpool")]
        public SpawnType WaveType = SpawnType.None;
        [Tooltip("Time until next wave is active, min wave cooldown is used if this number is too low")]
        public float SpawnCooldown = 1.0f;
        [Tooltip("Whether or not coins rain from the sky during wave")]
        public bool RewardWave = false;
        [Tooltip("Number of spawners used in waves, MUST BE THE SAME SIZE AS SPAWNERS")]
        public List<bool> activewaves = new List<bool>(5);
    }

    #region GeneralVariables

    //Stores all of the waves that will occur
    private List<Wave> Waves;

    //Current teleporter/Coin Spawner
    private GameObject Teleporter = null;
    private GameObject CoinSpawner = null;

    //Store references to the spawner script on each spawner
    private List<EnemySpawn> SpawnerScripts = new List<EnemySpawn>();

    //State of the wave controller
    private WaveState CurrentState = WaveState.Start;

    //Private variables to keep track of current spawning
    private int StartPos = 0;
    private int EndPos = 0;
    private int WaveCount = 0;
    private int CurrentActiveWave = 0;
    private int CurrentEnemyCount = 0;
    private int CurrentEnemyMax = 0;
    private float Timer = 1;
    private int CurrentWave = 0;

    //Minimum wait time between waves
    [Tooltip("Min wait time between waves, should never be above spawn time of spawners")]
    public float MinWaitTime = 1.0f;

    #endregion

    #region Phase1
    [Header("Phase1")]
    [Tooltip("Coin Spawner provides coins on bonus waves")]
    public GameObject CoinSpawnerPhase1 = null;

    [Tooltip("Teleporter to Next Arena")]
    public GameObject TeleporterPhase1 = null;

    [Tooltip("List of waves of enemies")]
    public List<Wave> WavesPhase1;

    [Tooltip("List of spawners used to spawn enemies in waves")]
    public List<GameObject> SpawnersPhase1 = null;

    private int StartIndexPhase1 = 0;
    private int EndIndexPhase1 = 0;
    private int Phase1WaveCount = 0;
    private int Phase1EnemyCount = 0;

    #endregion

    #region Phase2
    [Header("Phase2")]
    [Tooltip("Coin Spawner provides coins on bonus waves")]
    public GameObject CoinSpawnerPhase2 = null;

    [Tooltip("Teleporter to Next Arena")]
    public GameObject TeleporterPhase2 = null;

    [Tooltip("List of waves of enemies")]
    public List<Wave> WavesPhase2;

    [Tooltip("List of spawners used to spawn enemies in waves")]
    public List<GameObject> SpawnersPhase2 = null;

    private int StartIndexPhase2 = 0;
    private int EndIndexPhase2 = 0;
    private int Phase2WaveCount = 0;
    private int Phase2EnemyCount = 0;

    #endregion

    #region
    [Header("Phase3")]
    [Tooltip("Coin Spawner provides coins on bonus waves")]
    public GameObject CoinSpawnerPhase3 = null;

    [Tooltip("Teleporter to Next Arena")]
    public GameObject TeleporterPhase3 = null;

    [Tooltip("List of waves of enemies")]
    public List<Wave> WavesPhase3;

    [Tooltip("List of spawners used to spawn enemies in waves")]
    public List<GameObject> SpawnersPhase3 = null;

    private int StartIndexPhase3 = 0;
    private int EndIndexPhase3 = 0;
    private int Phase3WaveCount = 0;
    private int Phase3EnemyCount = 0;
    #endregion

    // Use this for initialization
    void Start ()
    {
        if(MinWaitTime <= 0)
        {
            MinWaitTime = 0.1f;
        }

        //Intial state
        CurrentState = WaveState.Start;

        //Calculates the index of the start of the first wave's spawners
        StartIndexPhase1 = 0;

        //Create a list of the spawner scripts from the referenced gameobjects
        for (int i=0; i<SpawnersPhase1.Count; i++)
        {
            SpawnerScripts.Add(SpawnersPhase1[i].GetComponent<EnemySpawn>());
        }

        //Calculate the start and end indexes for phase 1/2
        EndIndexPhase1 = SpawnerScripts.Count;
        StartIndexPhase2 = SpawnerScripts.Count;

        //Create a list of the spawner scripts from the referenced gameobjects
        for (int i = 0; i < SpawnersPhase2.Count; i++)
        {
            SpawnerScripts.Add(SpawnersPhase2[i].GetComponent<EnemySpawn>());
        }

        //Calculate the start and end indexes for phase 2/3
        EndIndexPhase2 = SpawnerScripts.Count;
        StartIndexPhase3 = SpawnerScripts.Count;

        //Create a list of the spawner scripts from the referenced gameobjects
        for (int i = 0; i < SpawnersPhase3.Count; i++)
        {
            SpawnerScripts.Add(SpawnersPhase3[i].GetComponent<EnemySpawn>());
        }

        //Calulate the final index for the list
        EndIndexPhase3 = SpawnerScripts.Count;

        //If the spawnerscript is not empty set the first cooldown
        if (SpawnerScripts.Count > 0)
        {
            Timer = SpawnerScripts[0].WaitTime;

            if(Timer < MinWaitTime)
            {
                Timer = MinWaitTime;
            }
        }

        //Count up enemies for Wave1
        for(int i=0; i<WavesPhase1.Count; i++)
        {
            Phase1EnemyCount += WavesPhase1[i].WaveSize;
        }

        //Count up enemies for Wave2
        for (int i = 0; i < WavesPhase2.Count; i++)
        {
            Phase2EnemyCount += WavesPhase2[i].WaveSize;
        }

        //Count up enemies for Wave3
        for (int i = 0; i < WavesPhase3.Count; i++)
        {
            Phase3EnemyCount += WavesPhase3[i].WaveSize;
        }

        //Initialize list of the combined waves
        Waves = new List<Wave>();

        //Add all the lists together
        //This is so that the different rooms waves can be displayed differently in the inspector
        Waves.AddRange(WavesPhase1);
        Phase1WaveCount = Waves.Count;
        Waves.AddRange(WavesPhase2);
        Phase2WaveCount = Waves.Count;
        Waves.AddRange(WavesPhase3);
        Phase3WaveCount = Waves.Count;

        //Turn off gameObjects which spawn later
        TeleporterPhase1.SetActive(false);
        TeleporterPhase2.SetActive(false);
        TeleporterPhase3.SetActive(false);
        CoinSpawnerPhase1.SetActive(false);
        CoinSpawnerPhase2.SetActive(false);
        CoinSpawnerPhase3.SetActive(false);

        //Go to state change management function
        //Technically code for setting phase one could be in start (allows states to be looped this way though)
        ChangeState();

    }
	
	// Update is called once per frame
	void Update ()
    {
        //When wave cooldown is over
        if(Timer < 0 && CurrentWave < WaveCount)
        {
            //Find the number of spawners to be used for spawning
            int CountOfActive = 0;

            for (int i = 0; i < (Waves[CurrentWave].activewaves.Count); i++)
            {
                if (Waves[CurrentWave].activewaves[i])
                {
                    CountOfActive++;
                }
            }

            //Calculate the amount for each to spawner to spawn
            int SizePerSpawner = Waves[CurrentWave].WaveSize / CountOfActive;

            //Calculate the remainder from the amount set in the inspector - the first number
            int extra = Waves[CurrentWave].WaveSize % CountOfActive;

            //Increase the size of the spawning enemies until all the extra enemies that need to be spawned are added
            if(extra > 0)
            {
                SizePerSpawner++;
            }
            else
            {
                //If there are no extras we set extra to -1
                //This is because when it is exactly 0 we decrease the sizeperspawn variable
                extra = -1;
            }

            //Spawn the required enemies
            for (int i = StartPos; i < EndPos; i++)
            {
                //If the spawner is active
                if(Waves[CurrentWave].activewaves[CurrentActiveWave])
                {
                    //Count down the number of extra spawns
                    if (extra > 0)
                    {
                        extra--;
                    }
                    else if(SizePerSpawner > 0 && extra == 0)
                    {
                        //If all the extra spawns are done, reduced the size per spawner
                        SizePerSpawner--;
                        //set extra to -1 to prevent this section from being activated a second time
                        extra = -1;
                    }

                    //Set the spawner to spawn the enemies
                    SpawnerScripts[i].StartSpawning(Waves[CurrentWave].WaveType, SizePerSpawner);
                }
                CurrentActiveWave++;
            }
            CurrentActiveWave = 0;

            //If the reward wave box is checked have 10 coins rain from the sky
            if (Waves[CurrentWave].RewardWave)
            {
                CoinSpawner.SetActive(true);
                CoinSpawner.SendMessage("ChangeSpawn", SpawnType.Coin);
            }

            //set the timer
            Timer = Waves[CurrentWave].SpawnCooldown;

            //Make sure the spawning time isn't too low
            if (Timer < MinWaitTime)
            {
                Timer = MinWaitTime;
            }

            //Increase the current wave
            CurrentWave++;

        }
        else
        {
            //Else tick down the timer
            Timer = Timer - Time.deltaTime;
        }

	}

    //Function to call in events which advances waaves
    public void ToggleActive(bool used)
    {
        if(!used)
        {
            Timer = MinWaitTime;
            CurrentEnemyCount = 0;
            ChangeState();
        }
    }

    //Function to be called by enemies when they die
    public void CountDefeated(GameObject temp, SpawnType spawnType)
    {
        if(spawnType != SpawnType.None)
        {
            CurrentEnemyCount++;

            if (CurrentEnemyCount >= CurrentEnemyMax)
            {
                Teleporter.SetActive(true);
            }
        }

    }

    public void ChangeState()
    {

        switch (CurrentState)
        {
            case WaveState.Start:
                //Set starting spawner indexes
                StartPos = StartIndexPhase1;
                EndPos = EndIndexPhase1;

                //Set pointers to first spawner / teleporter
                CoinSpawner = CoinSpawnerPhase1;
                Teleporter = TeleporterPhase1;

                WaveCount = Phase1WaveCount;
                CurrentEnemyMax = Phase1EnemyCount - 1;

                //Set starting state
                CurrentState = WaveState.PhaseOne;
                break;

            case WaveState.PhaseOne:

                //Update Spawner indexes in list
                StartPos = StartIndexPhase2;
                EndPos = EndIndexPhase2;

                //Update current references to teleported and coin spawner                   
                CoinSpawner = CoinSpawnerPhase2;
                Teleporter = TeleporterPhase2;

                //Update counts of waves and enemies inside of waves
                WaveCount = Phase2WaveCount;
                CurrentEnemyMax = Phase2EnemyCount - 1;

                //Move to next state
                CurrentState = WaveState.PhaseTwo;

                break;

            case WaveState.PhaseTwo:

                //Update Spawner indexes in list
                StartPos = StartIndexPhase3;
                EndPos = EndIndexPhase3;

                //Update current references to teleported and coin spawner 
                CoinSpawner = CoinSpawnerPhase3;
                Teleporter = TeleporterPhase3;

                //Update counts of waves and enemies inside of waves
                WaveCount = Phase3WaveCount;
                CurrentEnemyMax = Phase3EnemyCount - 1;

                //Move to next state
                CurrentState = WaveState.PhaseThree;

                break;
            case WaveState.PhaseThree:
                CurrentState = WaveState.Finish;
                break;

            case WaveState.Finish:
                Debug.Log("Finished");
                break;
        }
    }
}
