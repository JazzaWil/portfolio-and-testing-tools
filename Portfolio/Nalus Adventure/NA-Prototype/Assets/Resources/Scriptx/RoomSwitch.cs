﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomSwitch : MonoBehaviour
{

    public GameObject room;

    private void Awake()
    {
        room.SetActive(false);
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.tag == "Player")
        {
            room.SetActive(false);
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        { 
            room.SetActive(true);
        }

    }

}
