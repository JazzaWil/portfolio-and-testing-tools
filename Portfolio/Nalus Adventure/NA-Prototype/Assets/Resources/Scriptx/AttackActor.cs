﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
///Might add small pause on start and end of attack to make it more visible
///
/// </summary>
public class AttackActor : MonoBehaviour
{
    #region Designer Vars



    [Header("Basic Attack Settings")]
    public float Default_SwingArea;//width of swing in degrees
    public float Default_SwingSpeed;//speed swing is performed
    public float Default_Damage;//not implemted yet - damage

    //powerup1 - speed //swap these around later
    [Header("Powerup 1(Strong) Attack Settings")]
    public float PowerUp1_StartingRadius;
    public float PowerUp1_MaxRadius;
    public float PowerUp1_Damage;
    public float PowerUp1_Speed;
    //powerup2 - aoe strength
    [Header("Powerup 2(Fast) Attack Settings")]
    public float PowerUp2_SwingArea;//width of swing in degrees
    public float PowerUp2_SwingSpeed;//speed swing is performed
    public float PowerUp2_Damage;//not implemted yet - damage
    public float PowerUp2_SwingCount = 3;//not implemted yet - damage

    //powerup3 - stab
    [Header("Powerup 3(strat) Attack Settings")]
    public float PowerUp3_SwingLength;
    public float PowerUp3_SwingWidth;
    public float PowerUp3_Duration;//???
    public float PowerUp3_SwingSpeed;
    public float PowerUp3_HoldDuration;
    public float PowerUp3_Damage;
    private float PowerUp3_OriginalScale = 0.1f;
    private Vector3 PowerUp3_Position;
    private Vector3 PowerUp3_test;


    [Header("All swing Attack Settings")]
    public float DistanceFromPlayer = 1.5f;//distance attack gameObject is from player


    private float attackDamage;

    SphereCollider AOE_Collider;


    //powerup3



    #endregion
    Damage damage;
    public GameObject ThisparticleEmitter;

    public GameObject swingParticles;
    private ParticleSystem swingPartSystem = null;
    
    
    private ParticleSystem swingPart;

    private float deltaRotate;
    private int PowerUp2_CurrentSwing = 0;
    private float PowerUp3_HoldDurationTimer;
    
    
    //stuff for raycasting
    Plane m_Plane;
    Vector3 hitPoint;
    PlayerController playerActor;

    MeshRenderer meshRenderer;

    PowerUpTypes attackType = new PowerUpTypes();//changed from public
    //var dropDown = myEnum.Item1;
    void Start()
    {
        playerActor = gameObject.GetComponentInParent<PlayerController>();
        meshRenderer = gameObject.GetComponent<MeshRenderer>();
        meshRenderer.enabled = false;
        swingPartSystem = GetComponentInChildren<ParticleSystem>();
        testSetup();
    }

    public void testSetup()
    {
        attackType = PlayerController.CurrentPower;
        deltaRotate = 0;
        gameObject.GetComponentInChildren<ParticleSystem>().Clear(true);

        //swingPartSystem.transform.SetParent(gameObject.transform);
        switch (attackType)
        {
            case PowerUpTypes.Default:
                DefaultAttackStart();
                break;
            case PowerUpTypes.Strong:
                Attack1Start();
                break;
            case PowerUpTypes.Fast:
                Attack2Start();
                break;
            case PowerUpTypes.Strategic:
                Attack3Start();
                break;
        }
    }
    void Update()
    {
        
        switch (attackType)
        {
            case PowerUpTypes.Default:
                DefaultAttackUpdate();
                break;
            case PowerUpTypes.Strong:
                Attack1Update();
                break;
            case PowerUpTypes.Fast:
                Attack2Update();
                break;
            case PowerUpTypes.Strategic:
                Attack3Update();
                break;
        }
    }

    void DefaultAttackStart()//Single swing
    {
        transform.localPosition = new Vector3(0, 0, 0);
        
        //attackType = transform.parent.GetComponent<PlayerController>().CurrentPower;
        attackDamage = Default_Damage;
        m_Plane = new Plane(Vector3.up, new Vector3(0, transform.parent.position.y, 0));//creates a fictional plane on level with the player

        //Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);//creates ray from mouse to cam
        
        //float enter = 0.0f;
        //if (m_Plane.Raycast(ray, out enter))//casts ray to plane
        //{
        //    hitPoint = ray.GetPoint(enter);//sets hitpoint to ray hit location
        //}
        hitPoint = PlayerController.hitPoint;
        transform.LookAt(hitPoint);//faces swing toward mouse

        transform.RotateAround(transform.parent.position, new Vector3(0, 1, 0), 0.5f * Default_SwingArea);//rotates the starting pos back half of the swing area
        transform.Translate(Vector3.forward * DistanceFromPlayer, Space.Self);//translates swing forward (as to not be in center of player)
                                                                              // swingPart.enableEmission = true;
        if(swingPartSystem != null)
        {
            swingPartSystem.Pause(false);
        }
            
    }
    void DefaultAttackUpdate()//Single swing
    {
        transform.RotateAround(transform.parent.position, Vector3.up, Default_SwingArea * Time.deltaTime * -Default_SwingSpeed);//swing around player(parent) by X * delta time on the y axis (vec3.up)

        deltaRotate += Default_SwingArea * Time.deltaTime * Default_SwingSpeed;//keep track of delta rotation

        if (deltaRotate >= Default_SwingArea)//checking if attack has completed its rotation
        {
            //swingPart.enableEmission = false;
            gameObject.SetActive(false);
            
        }
    }

    void Attack1Start()//AOE 
    {
        transform.localPosition = new Vector3(0, 0, 0);
        //part test
        Instantiate(ThisparticleEmitter, transform.position, Quaternion.identity);
        //tempAttack.transform.SetParent(gameObject.transform);
        //tempAttack.transform.localScale = new Vector3(1, 1, 1);
        //particleEmitter.transform.localScale = new Vector3(PowerUp1_MaxRadius, 1, PowerUp1_MaxRadius); attackDamage = PowerUp1_Damage;
        //particleEmitter.GetComponent<ParticleSystem>().size
        //disables default collider
        Collider m_Collider;
        m_Collider = GetComponent<Collider>();
        m_Collider.isTrigger = false;
        if(swingPartSystem != null)
        {
            swingPartSystem.Clear();
            swingPartSystem.Pause(true);
        }

        //adds sphere collider
        AOE_Collider = gameObject.AddComponent(typeof(SphereCollider)) as SphereCollider;
        AOE_Collider.isTrigger = true;
        AOE_Collider.enabled = true;
        AOE_Collider.radius = PowerUp1_StartingRadius;
    }
    void Attack1Update()//playerbased aoe
    {
        AOE_Collider.radius += PowerUp1_Speed * Time.deltaTime;

        if (AOE_Collider.radius > PowerUp1_MaxRadius)
        {
            Destroy(AOE_Collider);
            gameObject.SetActive(false);
        }
    }

    void Attack2Start()//triple swing
    {
        
        attackDamage = PowerUp2_Damage;
        transform.localPosition = new Vector3(0, 0, 0);
        PowerUp2_CurrentSwing = 0;
        m_Plane = new Plane(Vector3.up, new Vector3(0, transform.parent.position.y, 0));//creates a fictional plane on level with the player
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);//creates ray from mouse to cam
        float enter = 0.0f;

        if (m_Plane.Raycast(ray, out enter))//casts ray to plane
        {
            hitPoint = ray.GetPoint(enter);//sets hitpoint to ray hit location
        }
        transform.LookAt(hitPoint);//faces swing toward mouse

        if(swingPartSystem != null)
        {
            swingPartSystem.Pause(false);
        }

        transform.Translate(Vector3.forward * DistanceFromPlayer, Space.Self);//translates swing forward (as to not be in center of player)
        transform.RotateAround(transform.parent.position, new Vector3(0, -1, 0), 0.5f * PowerUp2_SwingArea); //0.5f * PowerUp2_SwingArea);//rotates the starting pos back half of the swing area
    }
    void Attack2Update()//triple swing
    {
        if (PowerUp2_CurrentSwing % 2 == 0)
        {
            transform.RotateAround(transform.parent.position, Vector3.up, PowerUp2_SwingArea * Time.deltaTime * PowerUp2_SwingSpeed);//swing around player(parent) by X * delta time on the y axis (vec3.up)
        }

        if (PowerUp2_CurrentSwing % 2 == 1)
        {
            transform.RotateAround(transform.parent.position, Vector3.up, (PowerUp2_SwingArea * Time.deltaTime) * -PowerUp2_SwingSpeed);//swing around player(parent) by X * delta time on the y axis (vec3.up)
        }

        deltaRotate += PowerUp2_SwingArea * Time.deltaTime * PowerUp2_SwingSpeed;//keep track of delta rotation

        if (deltaRotate >= PowerUp2_SwingArea)//checking if attack has completed its rotation
        {
            PowerUp2_CurrentSwing++;
            deltaRotate = 0;
        }

        if (PowerUp2_CurrentSwing >= PowerUp2_SwingCount)
        {
            gameObject.SetActive(false);
        }
    }

    void Attack3Start()
    {
        attackDamage = PowerUp3_Damage;
        transform.localPosition = new Vector3(0, 0, 0);

        if (swingPartSystem != null)
        {
            swingPartSystem.Pause(false);
        }

        PowerUp3_HoldDurationTimer = PowerUp3_HoldDuration;
        m_Plane = new Plane(Vector3.up, new Vector3(0, transform.parent.position.y, 0));//creates a fictional plane on level with the player

        //Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);//creates ray from mouse to cam

        //float enter = 0.0f;
        //if (m_Plane.Raycast(ray, out enter))//casts ray to plane
        //{
        //    hitPoint = ray.GetPoint(enter);//sets hitpoint to ray hit location
        //}
        hitPoint = PlayerController.hitPoint;
        transform.LookAt(hitPoint);//faces swing toward mouse

        transform.Translate(Vector3.forward * DistanceFromPlayer, Space.Self);//translates swing forward (as to not be in center of player)


        Vector3 v = new Vector3(0.25f, 0.25f, PowerUp3_OriginalScale);//fix
        transform.localScale = v;
    }
    void Attack3Update()
    {
        if (transform.localScale.z < PowerUp3_SwingLength)//checking if not a max lenghth
        {
            PowerUp3_test.z += Time.deltaTime * PowerUp3_SwingSpeed;
            transform.localScale += PowerUp3_test;
            transform.position = transform.parent.position + transform.forward * (transform.localScale.z / 2.0f + PowerUp3_OriginalScale / 2.0f);

        }
        else
        {//if at max length hold for duration
            PowerUp3_HoldDurationTimer -= Time.deltaTime;
            if (PowerUp3_HoldDurationTimer <= 0)
            {
                gameObject.SetActive(false);
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy")
        {
            //Debug.Log("Hit Melee");
            damage.DamageValue = attackDamage;
            damage.KnockBackValue = transform.parent.forward;
            damage.Type = attackType;
            other.gameObject.SendMessage("SendDamage", damage);
        }
    }
}
