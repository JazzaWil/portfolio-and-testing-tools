﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public enum PowerUpTypes { Default, Strong, Fast, Strategic };

public class Character : MonoBehaviour, IDamageMessage
{

    // This is a custom event class that will work with fucntions with a single string argument
    // must be declared as Serializable to show in the Inspector
    [System.Serializable]
    public class DeathEvent : UnityEvent<GameObject, SpawnType> { }

    [System.Serializable]
    public class DamageEvent : UnityEvent<float> { }


    private float CharacterHealth;

    public float MaxHealth = 100.0f;

    public DeathEvent onDead = null;

    public DamageEvent onDamage = null;

    public SpawnType PoolTag;

    private Vector3 SpawnLocation = new Vector3(0, 1, 0);

    private Vector3 knockback = new Vector3(0, 0, 0);
    private Vector3 oldpos = new Vector3(0, 0, 0);
    private float KnockBackTimer;

    public GameObject DropItem = null;
    public float respawnTimerNumber;
    public Coroutine respawnTimer;



    public void Awake()
    {
        if(DropItem != null)
        {
            DropItem.SetActive(false);
        }

        CharacterHealth = MaxHealth;
        SpawnLocation = gameObject.transform.position;

        WaveController wave = FindObjectOfType<WaveController>();

        //setup events
        UImanager uiManager = FindObjectOfType<UImanager>();
        if (uiManager != null)
        {
            //Sets up the events for players, which are seperate from the enemies
            if (gameObject.tag == "Player")
            {
                //Displays Death Message on Death
                onDead.AddListener(new UnityAction<GameObject, SpawnType>(uiManager.TempDeathMessage));

                //Updates the UI health
                onDamage.AddListener(new UnityAction<float>(uiManager.UpdateHealth));
            }
        }

        ObjectPool objectPool = FindObjectOfType<ObjectPool>();
        if (objectPool != null)
        {
            //Sets up the Enemy so when it is killed it is added to the object pool
            if (gameObject.tag == "Enemy")
            {
                onDead.AddListener(new UnityAction<GameObject, SpawnType>(objectPool.ReturnToPool));
                if(wave!=null)
                {
                    onDead.AddListener(new UnityAction<GameObject, SpawnType>(wave.CountDefeated));
                }
            }
        }
    }

    //turn on the death message and teleport the player to spawnpoint position
    public void Respawn()

    {
            respawnTimer = StartCoroutine(Wait(respawnTimerNumber));
            gameObject.transform.position = SpawnLocation;
        
    }
    private IEnumerator Wait (float waitForSeconds)
    {
        yield return new WaitForSeconds(waitForSeconds);
    }

    public void SendDamage(Damage damage)
    {
        CharacterHealth -= damage.DamageValue;
        
        if (CharacterHealth <= 0.0f)
        {
            Vector3 temp = gameObject.transform.position;
            //If the character is a player invoke the death events with the player's arguments
            if (gameObject.tag == "Player")
            {
                onDead.Invoke(gameObject, SpawnType.None);
                onDamage.Invoke(0.0f);
                CharacterHealth = MaxHealth;
                Respawn();
            }
            else
            {
                //Else invoke the death events with the enemies values
                onDead.Invoke(gameObject, PoolTag);
                CharacterHealth = MaxHealth;
            }

            //Drop an item! Such as coins or powerups
            if (DropItem != null)
            {
                DropItem.transform.position = temp;
                DropItem.SetActive(true);
            }
        }
        else
        {
            SendKnockBack(damage);
            onDamage.Invoke(CharacterHealth);
        }
    }
    void SendKnockBack(Damage damage)
    {
        knockback = damage.KnockBackValue;
        oldpos = transform.position;
        KnockBackTimer = 0.05f;
        //transform.position += damage.KnockBackValue;
    }

    private void Update()
    {
        if (KnockBackTimer > 0)
        {
            transform.position = Vector3.Lerp(transform.position, transform.position + knockback, Time.deltaTime * 70);
            KnockBackTimer -= Time.deltaTime;
        }

    }

    public void setNewRespawn(Vector3 newRespawn)
    {
        SpawnLocation = newRespawn;
    }
}
