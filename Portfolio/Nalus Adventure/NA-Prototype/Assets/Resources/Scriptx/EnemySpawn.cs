﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;



public class EnemySpawn : MonoBehaviour, ISpawnEnemy
{
    [Tooltip("Current Type to be spawned")]
    public SpawnType currentSpawnType = SpawnType.None;

    private SpawnType nextSpawnType = SpawnType.None;

    [Tooltip("Max Number of Spawns until the spawner sets itself to spawning nothing")]
    public int MaxSpawnNumber = 10;

    private int NextMaxSpawnNumber = 10;

    public Vector3 SpawnOffset = new Vector3 (0f, 1.5f, 0f);

    private Vector3 CurrentSpawnLocation = new Vector3 (0,0,0);

    [Tooltip("Radius of the circle of spawns")]
    public float SpawnRadius = 4;

    private int CurrentSpawnNumber = 0;

    public float WaitTime = 0.2f;
    private float timer = 0.2f;

    private ObjectPool objectPool = null;

    public bool RepeatSpawns = false;


    void Awake()
    {
        objectPool = FindObjectOfType<ObjectPool>();
    }

    void Start()
    {
        timer = WaitTime;
    }

    void FixedUpdate ()
    {
        //If we have spawned all the enemies we need
        if(CurrentSpawnNumber >= MaxSpawnNumber)
        {
            //If we have a second set of enemies queued, starting spawning those
            if(nextSpawnType != SpawnType.None)
            {
                currentSpawnType = nextSpawnType;
                MaxSpawnNumber = NextMaxSpawnNumber;
                nextSpawnType = SpawnType.None;
            }
            else
            {
                if(!RepeatSpawns)
                {
                    //stop spawning enemies
                    currentSpawnType = SpawnType.None;
                }

            }
            //reset current number of spawned enemies
            CurrentSpawnNumber = 0;
        }

        if(currentSpawnType != SpawnType.None)
        {
            timer -= Time.deltaTime;
            //If it is time to spawn, spawn enemy
            if (timer <= 0)
            {
                //random position is circle around the spawner
                Vector2 spawnPositionRaw = Random.insideUnitCircle * SpawnRadius;



                //////if an agent is one which is on a navmesh, we make sure that they are spawned onto one
                //if(currentSpawnType == SpawnType.Soldier || currentSpawnType == SpawnType.Tank)
                //{
                //    NavMeshHit hit;
                //    NavMesh.SamplePosition(CurrentSpawnLocation, out hit, 1.0f, NavMesh.AllAreas);

                //    CurrentSpawnLocation = hit.position;
                //}
                CurrentSpawnLocation = SpawnOffset + gameObject.transform.position + new Vector3(spawnPositionRaw.x, 0, spawnPositionRaw.y);
                timer = WaitTime;

                

                //returns the gameobject that was spawned, if spawning failued retusn null
                GameObject temp = objectPool.SpawnFromPool(currentSpawnType, CurrentSpawnLocation);

                //if an object was spawned up count
                //stops coming up against object pool limit from counting to spawn total
                if(temp!=null)
                {
                    CurrentSpawnNumber++;
                }

            }
        }
	}

    //Changes what enemies are spawning and the amount of spawns
    public void StartSpawning(SpawnType newType, int newSpawnLimit)
    {
        //If currently not spawning anything change spawn type
        if(currentSpawnType == SpawnType.None)
        {
            MaxSpawnNumber = newSpawnLimit;

            currentSpawnType = newType;
            CurrentSpawnNumber = 0;
        }
        else
        {
            //If currently spawn something, queue another spawn (overwrites existig queued spawn if it exists)
            nextSpawnType = newType;
            NextMaxSpawnNumber = newSpawnLimit;
        }
    }

    //change just the type of spawn but not the limit - a message interface
    public void ChangeSpawn(SpawnType newType)
    {
        //If currently not spawning anything change spawn type
        if (currentSpawnType == SpawnType.None)
        {
            currentSpawnType = newType;
            CurrentSpawnNumber = 0;
        }
        else
        {
            //If currently spawn something, queue another spawn (overwrites existig queued spawn if it exists)
            nextSpawnType = newType;
        }

    }
}
