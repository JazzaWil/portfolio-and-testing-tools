﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpinningTop : MonoBehaviour
{
    //Start Position
    private Vector3 origin_pos;

    //Scale factor for the function
    private float scale;

   [Tooltip("Scaling Factor for Figure Eight Movement")]
    public float Scale1 = 2.0f;

    [Tooltip("Scaling Factor for Figure Eight Movement")]
    public float Scale2 = 3.0f;

    [Tooltip("Scaling Factor for Figure Eight Movement")]
    public float distance = 1.5f;

    void Start ()
    {
        origin_pos = transform.position;
    }

    void Update ()
    {
        //Formula to calculate the figure of 8 path
        scale = Scale1 / (Scale2 - Mathf.Cos(2 * Time.time));
        transform.position = origin_pos + new Vector3(scale * Mathf.Cos(Time.time), 0, scale * (Mathf.Sin(2 * Time.time) / 2)) * distance;

    }
}
