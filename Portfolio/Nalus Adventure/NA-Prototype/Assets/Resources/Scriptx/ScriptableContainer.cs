﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



[CreateAssetMenu(fileName = "Container", menuName = "Container", order = 1)]
public class ScriptableContainer : ScriptableObject
{
    public SaveFile saveFile = new SaveFile { Health = 100.0f, SceneName = "Room123", Score = 0, Type = PowerUpTypes.Default };

    public bool Loaded = false;
}
