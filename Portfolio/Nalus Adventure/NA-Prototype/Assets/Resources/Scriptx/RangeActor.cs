﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Rewired;
public class RangeActor : MonoBehaviour
{
    PowerUpTypes attackType = new PowerUpTypes();
    public GameObject thisParticleEmitter;
    public GameObject chargeParticleEmitter;
    public GameObject laserExplode;
    public GameObject ConeParticleEmitter;
    private GameObject chargeEmit;
    private GameObject laserEmit1;

    private bool PowerUp3_firing;
    private float attackDamage;
    private float distanceTravled;
    private bool canPierce = false;
    private bool damageOverTime = false;

    [Header("Basic Attack Settings")]
    public float Default_Damage = 100;
    public float Default_Speed = 10;
    public float Default_Range = 10;
    public bool Default_Pierce;

    [Header("Powerup 1(Strong) Attack Settings")]
    public float PowerUp1_Damage = 100;
    public float PowerUp1_Speed = 5;
    public bool PowerUp1_Pierce = true;
    public float PowerUp1_Range = 5;
    public float PowerUp1_Size = 3;

    [Header("Powerup 2(Fast) Attack Settings")]
    public float PowerUp2_Damage = 100;
    public float PowerUp2_Range = 9;
    public float PowerUp2_Width = 3;
    public float PowerUp2_ChargeTime = 0.5f;
    private float PowerUp2_ChargeDelta = 0;
    public float PowerUp2_MaxDuration = 1;

    [Header("Powerup 3(strat) Attack Settings")]
    public float PowerUp3_Damage = 100;
    public float PowerUp3_Duration = 0.3f;
    public bool PowerUp3_holdPos;
    //raycasting variables for aiming at mouse
    Plane m_Plane;
    GameObject playerActor;

    //mesh for attack 3(cone)
    public Mesh mesh;
    private MeshCollider meshCollider;

    //for updated laser
    Vector3 hitPointOld;
    private BoxCollider laserMesh;

    ParticleSystem bulletTrail;
    
    Damage damage;

    private Player player;
    [Tooltip("This is the current Rewired player")]
    public int playerId = 0;
    public bool range;
    
    

    void Start()
    {
        m_Plane = new Plane(Vector3.up, new Vector3(0, transform.position.y, 0));//create plane on level with player (vec3.up should be replaced with vec3(0, player.y, 0) for readability)

        //Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        transform.LookAt(PlayerController.hitPoint);//set rotate to face mouse raycast hit
        bulletTrail = GetComponentInChildren<ParticleSystem>();
        //playerActor = GameObject.Find("Nalu");
        playerActor = GameObject.FindGameObjectWithTag("Player");
        //attackType = playerActor.CurrentPower;
        attackType = PlayerController.CurrentPower;
        //attackType = GameObject.Find("Nalu").GetComponent.GetComponent<PlayerActor>().CurrentPower;;
        bulletTrail.Clear();
        bulletTrail.Pause(false);

        switch (attackType)
        {
            case PowerUpTypes.Default:
                DefaultAttackStart();//simple projectile
                break;
            case PowerUpTypes.Strong:
                Attack1Start();//large projectile
                break;
            case PowerUpTypes.Fast:
                Attack4Start();//laser
                break;
            case PowerUpTypes.Strategic:
                Attack3Start();//cone
                break;
        }
        player = ReInput.players.GetPlayer(playerId);

    }

    void Update()
    {
        switch (attackType)
        {
            case PowerUpTypes.Default:
                DefaultAttackUpdate();
                break;
            case PowerUpTypes.Strong:
                Attack1Update();
                break;
            case PowerUpTypes.Fast:
                Attack4Update();
                gameObject.transform.SetParent(playerActor.transform);
                break;
            case PowerUpTypes.Strategic:
                Attack3Update();
                break;
        }
        range = player.GetButton("range");

    }

    void DefaultAttackStart()
    {
        //startup variables
        attackDamage = Default_Damage;
        canPierce = Default_Pierce;
        transform.LookAt(PlayerController.hitPoint);
        bulletTrail.Clear();
        bulletTrail.Pause(false);
    }
    void DefaultAttackUpdate()
    {

        transform.Translate(Vector3.forward * Default_Speed * Time.deltaTime, Space.Self);
        distanceTravled += Default_Speed * Time.deltaTime;
        
        if (distanceTravled >= Default_Range)
        {
            Destroy(gameObject);
        }
    }

    void Attack1Start()
    {
        //startup variables
        attackDamage = PowerUp1_Damage;
        canPierce = PowerUp1_Pierce;
        transform.localScale = transform.localScale * PowerUp1_Size;
        bulletTrail.Clear();
        bulletTrail.Pause(false);
    }
    void Attack1Update()
    {
        //move forward
        transform.Translate(Vector3.forward * Default_Speed * Time.deltaTime);
        distanceTravled += PowerUp1_Speed * Time.deltaTime;

        //destroy at max range
        if (distanceTravled >= PowerUp1_Range)
        {
            Destroy(gameObject);
        }
    }

    void Attack2Start()
    {
        GameObject tempAttack = Instantiate(thisParticleEmitter, transform.position, Quaternion.identity);
        tempAttack.transform.rotation = gameObject.transform.rotation;
        tempAttack.transform.position = gameObject.transform.position;
        tempAttack.transform.SetParent(gameObject.transform);
        laserMesh = new BoxCollider();
        attackDamage = PowerUp2_Damage;
        canPierce = true;
        damageOverTime = true;
        transform.SetParent(playerActor.transform);
        laserMesh = gameObject.AddComponent<BoxCollider>();
        laserMesh.size = new Vector3(PowerUp2_Width, 1, PowerUp2_Range);
        //transform.Rotate(transform.parent.transform.up, -90);
        //transform.Translate(Vector3.forward * PowerUp2_Range/2, Space.Self);
        laserMesh.center = transform.localPosition + (Vector3.forward * PowerUp2_Range / 2);
        laserMesh.isTrigger = true;
        hitPointOld = PlayerController.hitPoint;
        SphereCollider s = GetComponent<SphereCollider>();
        s.enabled = false;
        bulletTrail.Clear();
        bulletTrail.Pause(true);

        //GetComponentInChildren<ParticleSystem>().Pause();
    }
    void Attack2Update()
    {
        //Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        //float enter = 0.0f;
        //float angle;
        transform.localPosition = new Vector3(0, 0, 0);
        //angle = Vector3.SignedAngle(transform.position - hitPointOld, transform.position - PlayerController.hitPoint, Vector3.up);
        //hitPointOld = PlayerController.hitPoint;
        //transform.RotateAround(transform.parent.position, transform.up, angle);
        //transform.LookAt(PlayerController.hitPoint);
        //if (!Input.GetButton("Fire2") || Input.GetButton("Fire2"))
        if (!Input.GetKey(KeyCode.Mouse1) && !Input.GetButton("Fire2"))
        {
            Destroy(gameObject);
        }
    }
    #region laser charge test

    void Attack4Start()
    {
        attackDamage = PowerUp2_Damage;
        canPierce = true;
        damageOverTime = true;
        chargeEmit = Instantiate(chargeParticleEmitter, transform.position, Quaternion.identity);
        chargeEmit.transform.rotation = gameObject.transform.rotation;
        chargeEmit.transform.position = gameObject.transform.position;
        chargeEmit.transform.SetParent(gameObject.transform);
        PowerUp3_firing = false;
        PowerUp2_MaxDuration += PowerUp2_ChargeTime;
        bulletTrail.Clear();
        bulletTrail.Pause(true);
    }

    void Attack4Charged()
    {
        GameObject exp = Instantiate(laserExplode, transform.position, Quaternion.identity);
        exp.transform.rotation = gameObject.transform.rotation;
        exp.transform.position = gameObject.transform.position;
        exp.transform.SetParent(gameObject.transform);
        laserEmit1 = Instantiate(thisParticleEmitter, transform.position, Quaternion.identity);
        laserEmit1.transform.rotation = gameObject.transform.rotation;
        laserEmit1.transform.position = gameObject.transform.position;
        laserEmit1.transform.SetParent(gameObject.transform);
        laserMesh = new BoxCollider();
        transform.SetParent(playerActor.transform);
        laserMesh = gameObject.AddComponent<BoxCollider>();
        laserMesh.size = new Vector3(PowerUp2_Width, 1, PowerUp2_Range);
        laserMesh.center = transform.localPosition + (Vector3.forward * PowerUp2_Range / 2);
        laserMesh.isTrigger = true;
        hitPointOld = PlayerController.hitPoint;
        SphereCollider s = GetComponent<SphereCollider>();
        s.enabled = false;
        //Destroy(chargeEmit);
        

    }

    void Attack4Update()
    {
        //Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        //float enter = 0.0f;
        //float angle;
        transform.localPosition = new Vector3(0, 0, 0);


        PowerUp2_ChargeDelta += Time.deltaTime;
        if (PowerUp2_ChargeDelta >= PowerUp2_ChargeTime && !PowerUp3_firing)
        {
            Attack4Charged();
            PowerUp3_firing = true;
        }
        //angle = Vector3.SignedAngle(transform.position - hitPointOld, transform.position - PlayerController.hitPoint, Vector3.up);
        //hitPointOld = PlayerController.hitPoint;
        //transform.RotateAround(transform.parent.position, transform.up, angle);
        //transform.LookAt(PlayerController.hitPoint);
        //if (!Input.GetButton("Fire2") || Input.GetButton("Fire2"))
        //if (!Input.GetKey(KeyCode.Mouse1) && !Input.GetButton("Fire2"))
        if (player.GetButtonUp("range")) 
        {
            Destroy(gameObject);
        }
        if (PowerUp2_ChargeDelta > PowerUp2_MaxDuration)
        {
            Destroy(gameObject);
        }
    }
    #endregion
    //commit
    void Attack3Start()
    {
        attackDamage = PowerUp3_Damage;

        //comment this out to make hitbox invisible
        GetComponent<MeshFilter>().mesh = Instantiate(mesh);
        GetComponent<MeshRenderer>().enabled = false;

        //adding mesh collider to this object
        meshCollider = gameObject.AddComponent<MeshCollider>();
        meshCollider.sharedMesh = mesh;

        //making follow player
        transform.SetParent(playerActor.transform);
        transform.Rotate(transform.parent.transform.up, 90);

        bulletTrail.Clear();
        bulletTrail.Pause(false);

        if (PowerUp3_holdPos)
        {
            transform.SetParent(null);
        }
        
        //prevents imediate deletion on hit
        canPierce = true;
        meshCollider.convex = true;
        meshCollider.isTrigger = true;

        GameObject coneParticle = Instantiate(ConeParticleEmitter, transform.position, gameObject.transform.rotation);
        coneParticle.transform.SetParent(gameObject.transform);
        //uncomment this to disable player follow
        //transform.SetParent(null);
    }
    void Attack3Update()
    {
        //destroy after hold duration is up
        PowerUp3_Duration -= Time.deltaTime;
        if (PowerUp3_Duration <= 0)
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy")
        {

            damage.DamageValue = attackDamage;
            damage.KnockBackValue = transform.forward;
            damage.Type = attackType;
            other.gameObject.SendMessage("SendDamage", damage);
            //not implemented yet
            //other.gameObject.SendMessage("SendKnockBack", /*Knockback variable*/);

            //check if attack is destroyed on contact
            if (!canPierce)
            {
                DestroyObject(gameObject);
            }
        }
    }
    void OnCollisionStay(Collision other)
    {
        if (other.collider.tag == "Enemy" && damageOverTime == true)
        {
            //Debug.Log("Hit Range");

            //not implemented yet
            //other.gameObject.SendMessage("SendKnockBack", /*Knockback variable*/);

            damage.DamageValue = attackDamage;
            damage.KnockBackValue = transform.parent.forward;
            damage.Type = attackType;
            other.gameObject.SendMessage("SendDamage", damage);
        }
    }
}
