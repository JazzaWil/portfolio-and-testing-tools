﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public enum EnemyState
{
    SeekPlayer,
    Patrol,
    Roaming,
    None
}

public enum AttackState
{
    Melee,
    Range
}

public class EnemyController : MonoBehaviour
{
    public EnemyState enemyState;
    public AttackState attackState;
    private bool positionStartup = false;
    //private bool attackStartup = false;

    private GameObject Player = null;

    private NavMeshAgent agent;

    int layerMask = 1 << 8;

    public GameObject patrolPoint1;
    public GameObject patrolPoint2;
    private RaycastHit hit;
    float distenceBeforeMelee = 10;

    public float rangeCooldown;
    private float rangeCounter;

    static private ObjectPool pool = null;

    void Start()
    {
        //Setup player and agent variables
        Player = GameObject.FindGameObjectWithTag("Player");
        agent = gameObject.GetComponent<NavMeshAgent>();

        if(pool == null)
        {
            pool = FindObjectOfType<ObjectPool>();
        }

    }

    void Update()
    {
        layerMask = ~layerMask;

        if (enemyState == EnemyState.SeekPlayer)
        {
            SeekPlayer();
        }
        if (enemyState == EnemyState.Roaming)
        {
            SeekPlayer();
            positionStartup = false;
        }
        if (positionStartup == false)
        {
            if (enemyState == EnemyState.Patrol)
            {
                agent.SetDestination(patrolPoint1.transform.position);
                positionStartup = true;
            }

        }


        Attack();
    }

    //Constantly Follows Player
    public void SeekPlayer()
    {
        agent.SetDestination(Player.transform.position);
    }

    //Seeks player if it enters it's TriggerField
    private void OnTriggerEnter(Collider other)
    {
        if (enemyState == EnemyState.Roaming || enemyState == EnemyState.Patrol)
        {
            //Checks if the collided field is the player
            if (other.gameObject == Player)
            {
                enemyState = EnemyState.Roaming;
                //If the enemy is in roaming mode it will follow the player as long as its in range

            }
            else//If the trigger is not colliding with the player it will go betweem two points
            {
                if (other.gameObject == patrolPoint1)
                {
                    Debug.Log("Patroling to point 2");
                    agent.SetDestination(patrolPoint2.transform.position);
                }
                else if (other.gameObject == patrolPoint2)
                {
                    Debug.Log("Patroling to point 1");
                    agent.SetDestination(patrolPoint1.transform.position);
                }

            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject == Player)
        {
            if (enemyState == EnemyState.Roaming || enemyState == EnemyState.Patrol)
            {
                positionStartup = false;
                enemyState = EnemyState.Patrol;
            }
        }

    }

    private void Attack()
    {
        // Bit shift the index of the layer (8) to get a bit mask
        //int layerMask = 1 << 8;

        // This would cast rays only against colliders in layer 8.
        // But instead we want to collide against everything except layer 8. The ~ operator does this, it inverts a bitmask.
        //
        //RaycastHit hit;

        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, Mathf.Infinity, layerMask))
        {
            Debug.DrawRay(transform.position, hit.point, Color.yellow);
            Debug.DrawLine(transform.position, transform.position + (transform.forward * 10));
            if (hit.distance < distenceBeforeMelee)
            {
                attackState = AttackState.Melee;
            }
            else
            {
                attackState = AttackState.Range;
            }
        }

        if (attackState == AttackState.Melee)
        {
            //Do melee
        }
        else if(attackState == AttackState.Range)
        {
            //Do Range
            rangeCounter += Time.deltaTime;
            if (rangeCounter > rangeCooldown)
            {
                pool.SpawnFromPool(SpawnType.EnemyProjectile, transform.position);
                rangeCounter = 0;
            }
        }


    }

}