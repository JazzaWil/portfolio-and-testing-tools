﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class WinState : MonoBehaviour
{
    [System.Serializable]
    public class SceneChangeEvent : UnityEvent<string> { }

    public SceneChangeEvent onSceneChange = null;

    [System.Serializable]
    public class TeleportEvent : UnityEvent<bool> { }

    public TeleportEvent onTeleport = null;

    [Tooltip("Name of the scene to tranisition to, scene must be added into the build")]
    public string SceneName = "Programmers";

    [Tooltip("A GameObject to teleport the player to if SceneTransition is false")]
    public GameObject NextRoom = null;

    public bool SceneTransition = false;

    public bool ChangeRespawn = false;

    GameObject Player = null;
    Character CharacterScript = null;

    private bool Used = false;

    public float WaitTime = 10f;

    private void Start()
    {
        Player = GameObject.FindGameObjectWithTag("Player");

        CharacterScript = Player.GetComponent<Character>();

        UImanager UIManager = FindObjectOfType<UImanager>();

        WaveController waveController = FindObjectOfType<WaveController>();

        if (UIManager != null)
        {
            onSceneChange.AddListener(new UnityAction<string>(UIManager.GoToScene));
            onTeleport.AddListener(new UnityAction<bool>(UIManager.TeleportEvent));
        }
        else
        {
            Debug.Log("No scene manager");
        }

        if(waveController != null)
        {
            onTeleport.AddListener(new UnityAction<bool>(waveController.ToggleActive));
        }
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            onTeleport.Invoke(Used);
            StartCoroutine(Wait(other.gameObject));            
        }
    }

    private IEnumerator Wait(GameObject target)
    {
        yield return new WaitForSecondsRealtime(WaitTime);

        Teleport(target);
    }

    private void Teleport(GameObject target)
    {

        if (SceneTransition)
        {
            if (SceneName != null)
            {
                onSceneChange.Invoke(SceneName);
            }

        }
        else
        {
            //Temporary Teleport to next room in scene
            if (NextRoom != null)
            {
                target.transform.position = NextRoom.transform.position;

                if (ChangeRespawn)
                {
                    CharacterScript.setNewRespawn(NextRoom.transform.position);
                }

                Used = true;
            }
        }
    }

}
