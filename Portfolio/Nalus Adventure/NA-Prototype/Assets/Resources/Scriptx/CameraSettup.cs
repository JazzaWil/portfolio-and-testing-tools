﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSettup : MonoBehaviour
{
    private GameObject player;
    private Vector3 offset;
    // Use this for initialization
    void Start()
    {
        transform.LookAt(transform.parent.position);
        player = transform.parent.gameObject;
        offset = transform.position - transform.parent.position;
        transform.SetParent(null);
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = offset + player.transform.position;
        transform.LookAt(player.transform.position);
    }
}
