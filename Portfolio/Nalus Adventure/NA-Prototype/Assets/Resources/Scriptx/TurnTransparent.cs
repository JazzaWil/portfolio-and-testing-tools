﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnTransparent : MonoBehaviour
{
    private Color[] OriginalColour;

    private Color[] Transparent;

    private MeshRenderer CurrentMeshRenderer = null;

    public float TransparentAlpha = 0.1f;

    // Use this for initialization
    void Start ()
    {
        CurrentMeshRenderer = GetComponent<MeshRenderer>();

        OriginalColour = new Color[CurrentMeshRenderer.materials.Length];
        Transparent = new Color[CurrentMeshRenderer.materials.Length];

        for (int i=0; i<CurrentMeshRenderer.materials.Length; i++)
        {
            //store original colours
            OriginalColour[i] = CurrentMeshRenderer.materials[i].color;

            //create a copy of the colour with given new transparency
            Transparent[i] = CurrentMeshRenderer.materials[i].color;
            Transparent[i].a = TransparentAlpha;
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "TransparencyDetect")
        {


            for (int i = 0; i < CurrentMeshRenderer.materials.Length; i++)
            {
                //Change renderer mode to fade
                CurrentMeshRenderer.materials[i].SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
                CurrentMeshRenderer.materials[i].SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
                CurrentMeshRenderer.materials[i].SetInt("_ZWrite", 0);
                CurrentMeshRenderer.materials[i].DisableKeyword("_ALPHATEST_ON");
                CurrentMeshRenderer.materials[i].EnableKeyword("_ALPHABLEND_ON");
                CurrentMeshRenderer.materials[i].DisableKeyword("_ALPHAPREMULTIPLY_ON");
                CurrentMeshRenderer.materials[i].renderQueue = 3000;

                //Swap material to transparent one
                CurrentMeshRenderer.materials[i].color = Transparent[i];
            }

        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.tag == "TransparencyDetect")
        {


            for (int i = 0; i < CurrentMeshRenderer.materials.Length; i++)
            {
                //Change renderer mode to opaque
                CurrentMeshRenderer.materials[i].SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
                CurrentMeshRenderer.materials[i].SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.Zero);
                CurrentMeshRenderer.materials[i].SetInt("_ZWrite", 1);
                CurrentMeshRenderer.materials[i].DisableKeyword("_ALPHATEST_ON");
                CurrentMeshRenderer.materials[i].DisableKeyword("_ALPHABLEND_ON");
                CurrentMeshRenderer.materials[i].DisableKeyword("_ALPHAPREMULTIPLY_ON");
                CurrentMeshRenderer.materials[i].renderQueue = -1;

                //Swap material to original colour
                CurrentMeshRenderer.materials[i].color = OriginalColour[i];
            }

        }
    }
}
