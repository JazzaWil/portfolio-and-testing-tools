﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Beam : MonoBehaviour
{

    public BeamRender beamRender;
    public Transform start;
    public Material material;
    public Color color = Color.white;
    public BeamParticleSys particles;

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
