﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PowerUp : MonoBehaviour
{
    public enum PickUpState {PickUp, WaitingToPickUp, WaitingToDespawn }

    private PickUpState CurrentState = PickUpState.PickUp;

    [Tooltip("The code for the powerup")]
    public PowerUpTypes CurrentPower = PowerUpTypes.Default;

    private SpawnType SpawnType = SpawnType.None;

    //Event that occurs when the powerup is picked up by the player 
    [System.Serializable]
    public class PickUpEvent : UnityEvent<GameObject, SpawnType> { }

    [System.Serializable]
    public class PickUpPowerEvent : UnityEvent<PowerUpTypes> { }

    public PickUpEvent onPowerEvent;

    public PickUpPowerEvent onPickUpPowerEvent;

    private float Timer = 0.0f;

    public float PickUpWaitTime = 0.2f;

    public float DespawnTime = 1.0f;

    private ObjectPool objectPool = null;



    void Awake ()
    {
        UImanager uiManager = FindObjectOfType<UImanager>();

        PlayerController playerActor = FindObjectOfType<PlayerController>();

        objectPool = FindObjectOfType<ObjectPool>();

        //Sets up the event so that the UI is updated when a pickup is obtained by the player
        if (uiManager != null)
        {
            onPickUpPowerEvent.AddListener(new UnityAction<PowerUpTypes> (uiManager.UpdatePowerUp));
        }

        //Sets up an event so that the player's powerup type is updated on pickup
        if(playerActor != null)
        {
            onPickUpPowerEvent.AddListener(new UnityAction<PowerUpTypes>(playerActor.SetCurrrentType));
        }

        //Add event to add powerup to object pool
        if(objectPool != null)
        {
            onPowerEvent.AddListener(new UnityAction<GameObject, SpawnType>(objectPool.ReturnToPool));
        }

        switch (CurrentPower)
        {
            case PowerUpTypes.Fast:
                SpawnType = SpawnType.PowerFast;
                break;
            case PowerUpTypes.Strong:
                SpawnType = SpawnType.PowerPower;
                break;
            case PowerUpTypes.Strategic:
                SpawnType = SpawnType.PowerStrat;
                break;
        }

    }

    void Update ()
    {

        switch(CurrentState)
        {
            case PickUpState.PickUp:
                Timer = 0;
                break;
            
            case PickUpState.WaitingToPickUp:

                Timer += Time.deltaTime;
                if (Timer > PickUpWaitTime)
                {
                    CurrentState = PickUpState.WaitingToDespawn;
                }

                break;

            case PickUpState.WaitingToDespawn:

                Timer += Time.deltaTime;
                if (Timer > DespawnTime)
                {
                    CurrentState = PickUpState.PickUp;
                    objectPool.ReturnToPool(gameObject, SpawnType);
                }
                break;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(CurrentState == PickUpState.PickUp || CurrentState == PickUpState.WaitingToDespawn)
        {
            if (other.tag == "Player")
            {
                //Calls the events registed by on the object
                onPickUpPowerEvent.Invoke(CurrentPower);

                onPowerEvent.Invoke(gameObject, SpawnType);
                CurrentState = PickUpState.WaitingToPickUp;
                Timer = 0;
            }
        }


    }
}
