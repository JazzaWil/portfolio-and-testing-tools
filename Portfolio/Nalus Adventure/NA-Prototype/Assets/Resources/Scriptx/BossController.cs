﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class BossController : MonoBehaviour
{
    public enum Phases
    {
        Phase1,
        Phase2,
        Phase3,
        Waiting
    }

    public Phases phase;
    public Phases previousPhase;

    private GameObject Player = null;

    private NavMeshAgent agent;

    public int normalAttackCounter;
    public int defaultNormalAttackCounter;
    public float normalAttackTimer;
    public float defaultNormalAttackTimer;
    public float waitingTimer;
    public float defaultWaitingTimer;


    public List<GameObject> Nodes;

    // Use this for initialization
    void Start()
    {
        //Setup player and agent variables
        Player = GameObject.FindGameObjectWithTag("Player");
        agent = gameObject.GetComponent<NavMeshAgent>();
        //Goes through and adds all the weak points to a list
        GameObject[] Node;
        Node = GameObject.FindGameObjectsWithTag("Node");
        for (int i = 0; i < Node.Length; i++)
        {
            Nodes.Add(Node[i]);
        }

        defaultNormalAttackTimer = normalAttackTimer;
        defaultWaitingTimer = waitingTimer;
        normalAttackCounter = defaultNormalAttackCounter;
    }

    // Update is called once per frame
    void Update()
    {
        //These pieces of code simply just tick down two different timers
        if (phase == Phases.Waiting)
        {
            waitingTimer -= Time.deltaTime;
        }
        normalAttackTimer -= Time.deltaTime;

        //This set of ifs simply checks the vulnerable points of the boss
        //Then switches the phases depending on how many "nodes" there are
        if (Nodes.Count == 3)
        {
            normalAttackTimer = defaultNormalAttackCounter;
            phase = Phases.Phase1;
        }
        else
        if (Nodes.Count == 2)
        {
            normalAttackTimer = defaultNormalAttackCounter;
            phase = Phases.Phase1;
        }
        else
        if (Nodes.Count == 1)
        {
            normalAttackTimer = defaultNormalAttackCounter;
            phase = Phases.Phase1;
        }
        if (Nodes.Count == 0)
        {
            //Do some event to kill object
        }


        switch (phase)
        {
            case Phases.Waiting:
                if (waitingTimer <= 0)
                {
                    phase = previousPhase;
                }
                break;

            case Phases.Phase1:
                if (normalAttackCounter > 0)
                {
                    if (normalAttackTimer <= 0)
                    {
                        //Do normaL attack
                    }
                }
                else if (normalAttackCounter == 0)
                {
                    //Do special attack 1 
                    previousPhase = phase;
                    phase = Phases.Waiting;
                }
                break;

            case Phases.Phase2:
                if (normalAttackCounter > 0)
                {
                    if (normalAttackTimer <= 0)
                    {
                        //Do normaL attack
                    }
                }
                else if (normalAttackCounter == 0)
                {
                    //Do special attack 2 
                    previousPhase = phase;
                    phase = Phases.Waiting;
                }
                break;

            case Phases.Phase3:
                if (normalAttackCounter > 0)
                {
                    if (normalAttackTimer <= 0)
                    {
                        //Do normaL attack
                    }
                }
                else if (normalAttackCounter == 0)
                {
                    //Do special attack 3 
                    previousPhase = phase;
                    phase = Phases.Waiting;
                }
                break;

        }
    }
}
