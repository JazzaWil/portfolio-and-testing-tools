﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using System.IO;
using System;
using Rewired;
public enum UIState { MainMenu, ControllerSelect, Pause, Game };

[Serializable]
public struct SaveFile
{
    public float Health { get; set; }
    public int Score { get; set; }
    public PowerUpTypes Type { get; set; }
    public string SceneName { get; set; }

}

public class UImanager : MonoBehaviour
{
    #region PublicVariables

    [Tooltip("Sets starting state for the menu")]
    public UIState CurrentState = UIState.Game;

    [Header("UI Elements")]

    [Tooltip("UI text to show the current score")]
    public Text ScoreText = null;

    [Tooltip("UI text for the death message")]
    public Text DeathMessage = null;

    [Tooltip("UI Image for health")]
    public Image HealthBar = null;

    [Tooltip("UI text for the powerup")]
    public Image PowerUpImage = null;

    [Tooltip("Image shown when Nalu HP is empty")]
    public Image DizzyImage = null;

    [Header("Icons for Powerups")]

    [Tooltip("Sprite for strength powerup")]
    public Sprite StrengthPowerIcon = null;

    [Tooltip("Sprite for speed powerup")]
    public Sprite SpeedPowerIcon = null;

    [Tooltip("Sprite for stratergy powerup")]
    public Sprite StratPowerIcon = null;

    [Tooltip("Sprite for default powerup")]
    public Sprite DefaultPowerIcon = null;

    [Header("GameObjects to hide Menu Items")]

    [Tooltip("All Menu Items")]
    public GameObject Menu = null;

    [Tooltip("All Objects Relating to Game UI")]
    public GameObject CharacterInfo = null;

    [Tooltip("UI Objects that allow controller / keyboard select")]
    public GameObject ControllerOptions = null;

    public GameObject LevelSelect = null;

    public Image TeleportEffect = null;

    [Header("Variables to effect UI elements")]

    [Tooltip("How Fast the health decreases after a hit")]
    public float HealthDecreaseSpeed = 1.0f;

    [Tooltip("TargetAlpha for teleport tint effect")]
    public float targetAlpha = 0.8f;

    public float TimeToFade = 1.0f;

    [Tooltip("Amount of time for which the dizzy image is shown")]
    public float DizzyDisplayTime = 1.5f;

    public string FirstLevelName = "MasterScene";

    public bool OpenSurvey = false;
    public string SurveyURL = "https://www.google.com/";

    #endregion

    #region PrivateVariables

    private float timescale = 1.0f;

    public ScriptableContainer container = null;

    private SaveFile save;

    private string location;

    private float NewHealthBarFill = 1.0f;

    private Color TeleportColour;

    private bool Active = false;

    private bool menuButton = false;
    private Player player;
    [Tooltip("This is the current Rewired player")]
    public int playerId = 0;
    #endregion

    //Set up all default value for UI if all of the components exist
    public void Awake()
    {
        player = ReInput.players.GetPlayer(playerId);


        //Loading the game through a pause menu causes the time scale to be set to 0, so it needs to be set here
        Time.timeScale = 1;

        if (ScoreText != null)
        {
            ScoreText.text = "000";
        }

        if (DeathMessage != null)
        {
            DeathMessage.text = "You Died";
            DeathMessage.enabled = false;
        }

        if (HealthBar != null)
        {
            HealthBar.fillAmount = 1.0f;
        }

        if (PowerUpImage != null)
        {
            PowerUpImage.sprite = DefaultPowerIcon;
        }

        if (DizzyImage != null)
        {
            DizzyImage.enabled = false;
        }

        if(TeleportEffect != null)
        {
            TeleportColour = TeleportEffect.color;
        }

        //Container contains all the data needed between scenes
        //Loaded is a boolean which determines whether or not the scriptable object has had data loaded into it
        if(!container.Loaded)
        {
            save = container.saveFile;
            container.Loaded = true;
        }
        else
        {
            //If container has not been accessed before the values inside are set to default values
            save.Health = 100.0f;
            save.Score = 0;
            save.Type = PowerUpTypes.Default;
            save.SceneName = "Room123";
        }
        
    
        switch(CurrentState)
        {
            case UIState.ControllerSelect:

                if (Menu != null)
                {
                    Menu.SetActive(false);
                }

                if (CharacterInfo != null)
                {
                    CharacterInfo.SetActive(false);
                }

                if (ControllerOptions != null)
                {
                    ControllerOptions.SetActive(true);
                }

                Time.timeScale = 0;

                break;
            case UIState.Game:

                //If not on main menu hide the menu
                if (Menu != null)
                {
                    Menu.SetActive(false);
                }

                if (ControllerOptions != null)
                {
                    ControllerOptions.SetActive(false);
                }

                if (CharacterInfo != null)
                {
                    CharacterInfo.SetActive(true);
                }

                if(LevelSelect!=null)
                {
                    LevelSelect.SetActive(false);
                }

                Time.timeScale = 1;

                break;
            case UIState.MainMenu:

                //We hide the character infomation UI elements
                if (CharacterInfo != null)
                {
                    CharacterInfo.SetActive(false);
                }

                if(ControllerOptions != null)
                {
                    ControllerOptions.SetActive(false);
                }

                if (Menu != null)
                {
                    Menu.SetActive(false);
                }

                if (LevelSelect != null)
                {
                    LevelSelect.SetActive(true);
                }

                //Pause the game
                Time.timeScale = 0;

                break;
            case UIState.Pause:
                //Never Start The Game Paused
                CurrentState = UIState.Game;

                //If not on main menu hide the menu
                if (Menu != null)
                {
                    Menu.SetActive(false);
                }

                if (ControllerOptions != null)
                {
                    ControllerOptions.SetActive(false);
                }

                if (LevelSelect != null)
                {
                    LevelSelect.SetActive(false);
                }

                Time.timeScale = 1;

                break;
        }



        //Create the local save location of the device - note this will only work on windows machines
        location = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "NaluAdventureSave/Save.XML");
    }

    public void Update()
    {
        menuButton = player.GetButtonDown("Menu");

        if (menuButton == true)
        {
            //You can't pause the game if there is no button to resume it
            if (Menu != null)
            {
                TogglePause();
            }
        }
    }

    //Check for pause in fixed update to avoid an error where toggle pause would be called over and over again
    public void FixedUpdate()
    {
        //Slowly empty/fill health when health is changed
        if (HealthBar != null)
        {
            HealthBar.fillAmount = Mathf.Lerp(HealthBar.fillAmount, (NewHealthBarFill), Time.deltaTime * HealthDecreaseSpeed);
        }

        if(TeleportEffect != null)
        {
            if(Active)
            {
                TeleportColour.a = Mathf.Lerp(TeleportColour.a, targetAlpha, Time.deltaTime* TimeToFade);

                if (TeleportColour.a >= (targetAlpha - 0.1f))
                {
                    Active = false;
                    TeleportColour.a = 0;
                }

                TeleportEffect.color = TeleportColour;
            }

        }
    }

    //Toggles Pause / Game states
    private void TogglePause()
    {
        switch(CurrentState)
        {
            //State where the game is running, transition to pause state
            case UIState.Game:
                timescale = Time.timeScale;
                Time.timeScale = 0;
                Menu.SetActive(true);

                CurrentState = UIState.Pause;
                break;

            //State where the game is paused an extra UI is shown, tranisition to game state
            case UIState.Pause:
                //Set the timescale to original
                Time.timeScale = timescale;
                Menu.SetActive(false);

                CurrentState = UIState.Game;
                break;
            
            //State where the game is on main menu mode, tranisition to game state
            case UIState.MainMenu:
                CharacterInfo.SetActive(true);
                Menu.SetActive(false);
                Time.timeScale = timescale;

                CurrentState = UIState.Game;
                break;

            default:
                break;
        }
    }

    //Scene tranisition to another scene
    public void GoToScene(string sceneName)
    {
        container.saveFile = save;
        container.saveFile.SceneName = sceneName;

        if (save.SceneName != SceneManager.GetActiveScene().name)
        {
            SceneManager.LoadScene(sceneName, LoadSceneMode.Single);
        }

        if (OpenSurvey)
        {
            Application.OpenURL(SurveyURL);
        }

    }

    #region UpdateUIFunctions

    //Update the UI health bar for the player
    public void UpdateHealth(float newHealth)
    {
        //update loop will lerp the healthbar to the new value
        NewHealthBarFill = (newHealth / 100.0f);
    }

    //Update the UI display of the player's score
    public void UpdateScore(int newScore)
    {
        save.Score += newScore;

        int currentscore = save.Score;

        if (ScoreText != null)
        {
            if(currentscore < 10)
            {
                ScoreText.text = "00" + currentscore;
            }
            else if(currentscore > 9 && currentscore < 100)
            {
                ScoreText.text = "0" + currentscore;
            }
            else
            {
                ScoreText.text = "" + currentscore;
            }
        }
        else
        {
            Debug.Log("Score Text is Null");
        }
    }

    //Updates the death message - dizzy image should be used instead
    public void UpdateDeathMessage(string newMessage, bool enable)
    {
        if (DeathMessage != null)
        {
            DeathMessage.enabled = enable;
            DeathMessage.text = newMessage;

            if(enable)
            {
                StartCoroutine(Wait(1.0f));
            }
        }
        else
        {
            Debug.Log("Death Message is Null");
        }
    }

    //Shows up a Dizzy Nalu upon respawing to last checkpoint
    public void ToggleDizzyImage(bool enable)
    {
        if(enable)
        {
            DizzyImage.enabled = true;

            StartCoroutine(Wait(DizzyDisplayTime));
        }
        else
        {
            DizzyImage.enabled = false;
        }
    }

    //Updates the UI to show the icon of the newly obtained powerup
    public void UpdatePowerUp(PowerUpTypes newType)
    {
        if (PowerUpImage != null)
        {
            switch(newType)
            {
                case PowerUpTypes.Default:
                    PowerUpImage.sprite = DefaultPowerIcon;
                    break;

                case PowerUpTypes.Fast:
                    PowerUpImage.sprite = SpeedPowerIcon;
                    break;

                case PowerUpTypes.Strategic:
                    PowerUpImage.sprite = StratPowerIcon;
                    break;

                case PowerUpTypes.Strong:
                    PowerUpImage.sprite = StrengthPowerIcon;
                    break;
            }

            save.Type = newType;
            
        }
        else
        {
            Debug.Log("PowerUpText is Null");
        }
    }

    //Waits for a short time before resetting the UI to a non dead state
    private IEnumerator Wait(float seconds)
    {
        yield return new WaitForSecondsRealtime(seconds);

        ToggleDizzyImage(false);
        NewHealthBarFill = 1.0f;
    }

    //A temporary function to interface with the on dead event
    public void TempDeathMessage(GameObject notneeded, SpawnType message)
    {
        ToggleDizzyImage(true);
    }

    public void TeleportEvent(bool toggle)
    {
        Active = true;
    }

    #endregion

    #region ButtonFunctions

    public void SaveButton()
    {
        SaveGame();
    }

    public void LoadButton()
    {
        LoadGame();

        //Loading into another scene will automatically update the UI
        if(save.SceneName != SceneManager.GetActiveScene().name)
        {
            GoToScene(save.SceneName);
        }
        else
        {
            //If we are in the same scene we need to update the UI ourselves
            //It should be noted that HP is saved, but we ignore it's value

            //Update the score UI with an increase of 0
            UpdateScore(0);

            UpdatePowerUp(save.Type);
        }

    }

    public void StartButton()
    {
        GoToScene(FirstLevelName);
    }

    public void ResumeButton()
    {
        TogglePause();
    }

    public void ExitButton()
    {
        Application.Quit();
    }

    public void ControllerButton()
    {
        Time.timeScale = 1;
        PlayerController.controllerOn = true;
        ControllerOptions.SetActive(false);
        CharacterInfo.SetActive(true);
        CurrentState = UIState.Game;
    }

    public void KeyBoardButton()
    {
        Time.timeScale = 1;
        PlayerController.controllerOn = false;
        ControllerOptions.SetActive(false);
        CharacterInfo.SetActive(true);
        CurrentState = UIState.Game;
    }

    public void ShowControllerSelect()
    {
        ControllerOptions.SetActive(true);
        Time.timeScale = 0;
        CurrentState = UIState.ControllerSelect;
    }

    #endregion

    #region Save&Load

    private void LoadGame()
    {
        if (File.Exists(location))
        {
            Stream stream = File.Open(location, FileMode.Open);

            XmlSerializer formatter = new XmlSerializer(typeof(SaveFile));

            //if stream isn't empty try to deserialise
            if (stream.Length > 0)
            {
                try
                {
                    save = (SaveFile)formatter.Deserialize(stream);
                }
                catch (System.Runtime.Serialization.SerializationException)
                {
                    save = new SaveFile();
                    Debug.Log("Cannot Read SaveFile");
                }
                finally
                {

                }
            }

            stream.Close();

            container.saveFile = save;
        }

        
    }

    private void SaveGame()
    {
        TextWriter stream = new StreamWriter(location);

        XmlSerializer formatter = new XmlSerializer(typeof(SaveFile));

        formatter.Serialize(stream, save);
        stream.Close();
    }

    #endregion
}
