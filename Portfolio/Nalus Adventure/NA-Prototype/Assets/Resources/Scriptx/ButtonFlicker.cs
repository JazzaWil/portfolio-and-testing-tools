﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ButtonFlicker : MonoBehaviour
 {
   public Image startText;
    public float timer;
    public float defaultTimer;
    public Color oldColour;
    public bool hoverCheck;
    // Use this for initialization
    void Start ()
    {
        startText = GetComponent<Image>();
        defaultTimer = timer;
        oldColour = startText.color;
	}
	
	// Update is called once per frame
	void Update ()
    {
       if(hoverCheck == true)
        {
            timer -= Time.fixedUnscaledDeltaTime;
            oldColour.a = timer;
            startText.color = oldColour;
            if (timer <= -0.1)
            {
                timer = defaultTimer;
            }
        }
      

    }
}
