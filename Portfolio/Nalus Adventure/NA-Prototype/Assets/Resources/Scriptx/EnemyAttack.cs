﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttack : MonoBehaviour
{

    private float attackDamage;
    private float distanceTravled;
    private bool canPierce = false;
    //private bool damageOverTime = false;
    [Header("Basic Attack Settings")]
    public float Default_Damage = 10;
    public float Default_Speed = 10;
    public float Default_Range = 40;
    public float Default_Spread = 5;
    public bool Default_Pierce;
    public Vector3 target;
    private Damage damage;

    private bool Aimed = false;

    static private GameObject Player = null;

    static private ObjectPool pool = null;

    // Use this for initialization
    void Start()
    {
        DefaultAttackStart();

        pool = FindObjectOfType<ObjectPool>();

        Player = GameObject.FindGameObjectWithTag("Player");

        if(Player == null)
        {
            Debug.Log("Player Not Found!");
        }

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        DefaultAttackUpdate();

    }
    void DefaultAttackStart()
    {
        if(!Aimed)
        {
            if(Player == null)
            {
                Player = GameObject.FindGameObjectWithTag("Player");
            }

            target = Player.transform.position;
            Aimed = true;
        }

        //startup variables
        attackDamage = Default_Damage;
        canPierce = Default_Pierce;
        transform.LookAt(target);
        transform.Rotate(Vector3.up, Random.Range(-Default_Spread, Default_Spread));
        
    }
    void DefaultAttackUpdate()
    {
        
        transform.Translate(Vector3.forward * Default_Speed * Time.deltaTime);
        transform.position = new Vector3(transform.position.x, target.y, transform.position.z);
        distanceTravled += Default_Speed * Time.deltaTime;

        if (distanceTravled >= Default_Range)
        {
            distanceTravled = 0;
            pool.ReturnToPool(gameObject, SpawnType.EnemyProjectile);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {

            damage.DamageValue = attackDamage;
            damage.KnockBackValue = new Vector3(transform.forward.x, 0, transform.forward.z);
                
            
            damage.Type = PowerUpTypes.Default;
            other.gameObject.SendMessage("SendDamage", damage);
            
            //check if attack is destroyed on contact
            if (!canPierce)
            {
                Aimed = false;
                pool.ReturnToPool(gameObject, SpawnType.EnemyProjectile);
            }
        }

        if(other.tag == "Object")
        {
            //check if attack is destroyed on contact
            if (!canPierce)
            {
                Aimed = false;
                pool.ReturnToPool(gameObject, SpawnType.EnemyProjectile);
            }
        }
    }
}
