﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour
{
    [Range(0.0f, 10.0f)]
    public float RotationX = 5.0f;

    [Range(0.0f, 10.0f)]
    public float RotationY = 5.0f;

    [Range(0.0f, 10.0f)]
    public float RotationZ = 5.0f;

    //If the axis has rotation enabled
    public bool x = false;
    public bool y = false;
    public bool z = false;

    //Sets all the axis rotations which are disabled to 0
    private void Awake()
    {
        if(!x)
        {
            RotationX = 0.0f;
        }

        if (!y)
        {
            RotationY = 0.0f;
        }

        if (!z)
        {
            RotationZ = 0.0f;
        }
    }

    void FixedUpdate ()
    {
        transform.Rotate(RotationX, RotationY, RotationZ);
    }
}
