﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public enum SpawnType { None, Tank, Soldier, Coin, PowerPower, PowerFast, PowerStrat, EnemyProjectile, PlayerProjectileDefault, PlayerProjectilePower, GoldSoldier, SteelSoldier};

public class ObjectPool : MonoBehaviour
{
    [Tooltip("Object Pool")]
    public static Dictionary<SpawnType, Queue<GameObject>> PoolDictionary;

    //Contents of the object pool
    public List<Pool> pools;



    //Pool Class, all the values are set in the inspector (to define the pools in advance)
    [System.Serializable]
    public class Pool
    {
        //Tag that the pool will be stored by
        public SpawnType Tag;
        //Prefab that the pooled objects use
        public GameObject Prefab;
        //Size of the pool
        public int Size;
    }




    //Sets up the object pool
    void Start()
    {

        
        NavMeshHit hit;
        NavMesh.SamplePosition(Vector3.zero, out hit, 100.0f, NavMesh.AllAreas);

        //Create the dictionary - this allows us to have as many pools as we need and retrieve them via a tag
        PoolDictionary = new Dictionary<SpawnType, Queue<GameObject>>();

        //For each of the pools initialise all of the contents
        foreach (Pool pool in pools)
        {
            Queue<GameObject> objectPool = new Queue<GameObject>();

            for (int i = 0; i < pool.Size; i++)
            {
                GameObject currentSpawn = Instantiate(pool.Prefab, hit.position, pool.Prefab.transform.rotation);

                //Turn off all of the contents, so we only turn them on when they are needed
                currentSpawn.SetActive(false);

                objectPool.Enqueue(currentSpawn);
            }

            PoolDictionary.Add(pool.Tag, objectPool);
        }

    }

    //Spawns a GameObject from the pool with the given tag at the given position
    public GameObject SpawnFromPool(SpawnType tag, Vector3 pos)
    {
        //Check that the tag exists in the pool
        if (PoolDictionary.ContainsKey(tag))
        {
            //Don't do anything if the pool is empty
            if (PoolDictionary[tag].Count == 0)
            {
                return null;
            }

            GameObject toSpawn = PoolDictionary[tag].Dequeue();

            toSpawn.transform.position = pos;

            toSpawn.SetActive(true);

            return toSpawn;
        }
        else
        {
            Debug.LogWarning("Pool doesn't exist, tag: " + tag);
        }

        return null;
    }

    //put an object back into the object pool - called by the object
    public void ReturnToPool(GameObject enque, SpawnType pooltag)
    {
        if(pooltag != SpawnType.None)
        {
            try
            {
                PoolDictionary[pooltag].Enqueue(enque);
            }
            catch (KeyNotFoundException)
            {
                Debug.Log("Key Not Found!" + pooltag);
            }
            finally
            {
                enque.SetActive(false);
            }
        }
        else
        {
            Debug.Log("Attempted to return untagged gameobject to pool");
            enque.SetActive(false);
        }
    }
}
