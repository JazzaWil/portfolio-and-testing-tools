﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillHitbox : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        Entity en = other.GetComponentInParent<Entity>();

        if(en != null)
        {
            en.Kill();
        }
    }
}
