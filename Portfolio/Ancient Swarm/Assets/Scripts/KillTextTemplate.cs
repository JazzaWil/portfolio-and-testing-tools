﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KillTextTemplate : MonoBehaviour
{
    public Text[] killer;
    public Image[] weapon;
    public Text[] victim;
}