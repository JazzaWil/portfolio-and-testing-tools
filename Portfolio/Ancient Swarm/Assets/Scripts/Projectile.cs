﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using NaughtyAttributes;

[RequireComponent(typeof(AudioSource))]
public class Projectile : MonoBehaviour
{
    [ShowIf("ShowHitSounds")]
    public AudioClip[] hitSounds;

    [HideInInspector] public AudioSource audioSource;

    [HideInInspector] public float damage;
    [HideInInspector] public float hitForce;

    [HideInInspector] public DamageFlags damageFlags;

    [HideInInspector] public GameObject owner;

    // Start is called before the first frame update
    public void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void Init(DamageFlags setDamageFlags, float setDamage, float setHitForce, GameObject setOwner)
    {
        damageFlags = setDamageFlags;
        damage = setDamage;
        hitForce = setHitForce;
        owner = setOwner;
    }

    public virtual void OnCollisionEnter(Collision collision)
    {
        ObjectHit(collision.gameObject);
    }

    public virtual void ObjectHit(GameObject obj)
    {
        Projectile pr = obj.GetComponent<Projectile>();

        if (pr == null)
        {
            bool destroy = true;

            if (obj.tag != "Shield")
            {
                Entity en = obj.GetComponentInParent<Entity>();

                if (en != null)
                {
                    if (damageFlags.flags[(int)en.type] && owner.GetInstanceID() != en.gameObject.GetInstanceID())
                    {
                        if (obj.tag == "Head")
                        {
                            DamageEntity(en, true);
                        }
                        else
                        {
                            DamageEntity(en, false);
                        }

                        if (en.IsDead())
                        {
                            foreach (Rigidbody rb in en.GetComponentsInChildren<Rigidbody>())
                            {
                                rb.AddForce(transform.forward * hitForce);
                            }
                        }
                    }
                    else
                    {
                        destroy = false;
                    }
                }
                else
                {
                    Rigidbody rb = obj.transform.GetComponent<Rigidbody>();

                    if (rb != null)
                    {
                        PushRigidbody(rb);
                    }
                }

                if (destroy)
                {
                    PlayHitSound();
                }
            }
        }
    }

    public virtual void DamageEntity(Entity en, bool headshot)
    {
        if (headshot)
        {
            en.Damage(damage * damageFlags.headShotMultiplier, owner);
        }
        else
        {
            en.Damage(damage, owner);
        }
    }

    public virtual void PushRigidbody(Rigidbody rb)
    {
        rb.AddForceAtPosition(transform.forward * hitForce, transform.position);
    }

    public virtual void PlayHitSound()
    {
        if (AudioRandomiser.PlayAudio(audioSource, hitSounds))
        {
            gameObject.SetActive(false);
            Destroy(gameObject, audioSource.clip.length);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public virtual bool ShowHitSounds()
    {
        return true;
    }
}
