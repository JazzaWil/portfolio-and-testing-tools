﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleSeek : MonoBehaviour
{
    Transform player;
    public float force;

    ParticleSystem particle;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("PointCounterBackground").transform;
        particle = GetComponent<ParticleSystem>();
    }

    // Update is called once per frame
    void LateUpdate()
    {
        ParticleSystem.Particle[] particles = new ParticleSystem.Particle[particle.particleCount];
        particle.GetParticles(particles);

        for (int i = 0; i < particles.Length; i++)
        {
            ParticleSystem.Particle p = particles[i];

            Vector3 directionTarget = (player.position - p.position).normalized;

            Vector3 seekForce = directionTarget * force * Time.deltaTime;

            p.velocity += seekForce;

            particles[i] = p;
        }

        particle.SetParticles(particles, particles.Length);

    }

    public void SetPlayer(GameObject playerObject)
    {
        player = playerObject.transform;
    }
}
