﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnubisWeaponBehaviour : WeaponBehaviour
{
    private AudioSource audioSource;
    private float defaultVolume;

    public void Start()
    {
        base.Start();

        audioSource = weaponController.GetAudioSource(0);
        defaultVolume = audioSource.volume;
    }

    // Update is called once per frame
    void Update()
    {
        if (weaponController.playerController.isGamePaused)
        {
            audioSource.volume = 0.0f;
        }
        else
        {
            audioSource.volume = defaultVolume;
        }

        if (!audioSource.isPlaying)
        {
            if (firing)
            {
                //play fire loop sound
                audioSource.loop = true;
                weaponController.SetNextAudioSource(0);

                PlayExtra1Sound();
            }
            else
            {
                audioSource.loop = false;
            }
        }
    }

    public override void PlayFireSound()
    {
        audioSource.loop = false;
        weaponController.SetNextAudioSource(0);
        base.PlayFireSound();
        firing = true;
        weaponController.firing = true;
    }

    public override void PlayExtra2Sound()
    {
        audioSource.loop = false;
        weaponController.SetNextAudioSource(0);

        //play end fire sound
        base.PlayExtra2Sound();
        firing = false;
    }
}