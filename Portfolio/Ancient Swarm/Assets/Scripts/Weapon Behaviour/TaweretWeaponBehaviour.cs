﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TaweretWeaponBehaviour : WeaponBehaviour
{
    public Transform spikes;
    public float spikesSpinSpeed;

    // Start is called before the first frame update
    public override void Start()
    {
        shiftBarrelEnd = false;
        base.Start();
    }

    // Update is called once per frame
    void Update()
    {
        spikes.eulerAngles += Vector3.forward * spikesSpinSpeed * Time.deltaTime;
    }
}