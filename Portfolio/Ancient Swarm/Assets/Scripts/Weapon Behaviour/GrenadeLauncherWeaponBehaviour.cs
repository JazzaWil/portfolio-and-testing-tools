﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pixelplacement;

public class GrenadeLauncherWeaponBehaviour : WeaponBehaviour
{
    public Transform grenadePivot;
    public GameObject[] grenades;

    public float rotateDuration;

    private float rotationAmount;

    // Start is called before the first frame update
    void Start()
    {
        base.Start();

        if (attributes.clipSize[0] == 0)
        {
            rotationAmount = 0;
        }
        else
        {
            rotationAmount = 360.0f / attributes.clipSize[0];
        }
    }

    void LateUpdate()
    {
        uint clipCheck = weaponController.CurrentClip();

        for (int i = 0; i < grenades.Length; i++)
        {
            grenades[i].SetActive(clipCheck > i);
        }
    }

    public override void EndReloading()
    {
        base.EndReloading();
        grenadePivot.localEulerAngles = new Vector3(-rotationAmount * (weaponController.CurrentClipSize() - weaponController.CurrentClip()), 0, 0);
    }

    public override void PlayFireSound()
    {
        base.PlayFireSound();
        Tween.Rotate(grenadePivot, new Vector3(-rotationAmount, 0, 0), Space.Self, rotateDuration, 0, Tween.EaseInOutStrong);
    }
}
