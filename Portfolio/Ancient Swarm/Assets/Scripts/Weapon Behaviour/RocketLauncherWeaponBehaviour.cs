﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocketLauncherWeaponBehaviour : WeaponBehaviour
{
    public MeshRenderer grenade;

    void LateUpdate()
    {
        if (!weaponController.reloading)
        {
            grenade.enabled = weaponController.CurrentClip() != 0;
        }
    }
}