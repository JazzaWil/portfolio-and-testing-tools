﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdolAnim : MonoBehaviour
{
    private WeaponController weaponController;
    public GameObject audioSource;

    // Start is called before the first frame update
    void Start()
    {
        weaponController = gameObject.transform.GetComponentInParent<WeaponController>();
        Instantiate(audioSource, transform.position, new Quaternion());
    }

    public void WaveStart()
    {
        weaponController.playerController.waveSpawner.SetActive(true);
        weaponController.playerController.idol.StartWave();
    }

    public void AnimEnd()
    {
        weaponController.IdolUnequip();
    }
}
