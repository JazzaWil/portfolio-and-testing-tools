﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pixelplacement;

public class RevolverWeaponBehaviour : WeaponBehaviour
{
    public Transform chamber;
    public float rotateDuration;

    private float rotationAmount;

    // Start is called before the first frame update
    void Start()
    {
        base.Start();
        rotationAmount = 360.0f / attributes.clipSize[0];
    }

    public override void PlayFireSound()
    {
        base.PlayFireSound();

        float rotate;

        if (dualWeapon)
        {
            rotate = rotationAmount;
        }
        else
        {
            rotate = -rotationAmount;
        }

        Tween.Rotate (chamber, new Vector3 (rotate, 0, 0), Space.Self, rotateDuration, 0, Tween.EaseInOutStrong);
    }
}
