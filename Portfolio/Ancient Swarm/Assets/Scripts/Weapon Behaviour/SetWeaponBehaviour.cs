﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetWeaponBehaviour : WeaponBehaviour
{
    public GameObject lightningEffect;

    [Header("Components")]
    public Transform leftEar;
    public Transform rightEar;
    public Transform leftEarBase;
    public Transform rightEarBase;
    public Transform leftEarContact;
    public Transform rightEarContact;
    public Transform spinPivot;
    public Transform[] gems;

    [Header("Effects")]
    public float leftEarBobSpeed;
    public float leftEarBobHeight;
    public float rightEarBobSpeed;
    public float rightEarBobHeight;
    public float pivotSpinSpeed;
    public float pivotSpinFireSpeed;
    public float gemBobSpeed;
    public float gemBobHeight;
    public float gemBobFrequency;
    public Vector2 gemSpinSpeed;
    public float lightningDestroyTimer;
    public Vector2 lightningTimerRange;

    private Vector3 leftEarPos;
    private Vector3 rightEarPos;

    private float leftEarBobAngle = 0;
    private float rightEarBobAngle = Mathf.PI / 2;
    private float gemBobAngle = 0;
    private List<Vector3> gemSpeeds;
    private float lightningTimer = 0;
    private float lightningTimerMax = 0;

    private List<GameObject> lightningFire;

    private bool charging = false;

    // Start is called before the first frame update
    void Start()
    {
        base.Start();

        lightningFire = new List<GameObject>();
        gemSpeeds = new List<Vector3>();

        for (int i = 0; i < gems.Length; i++)
        {
            gemSpeeds.Add(new Vector3(Random.Range(gemSpinSpeed.x, gemSpinSpeed.y), Random.Range(gemSpinSpeed.x, gemSpinSpeed.y), 0));
        }

        leftEarPos = leftEar.localPosition;
        rightEarPos = rightEar.localPosition;

        GameObject leftLightning = Instantiate(lightningEffect);
        leftLightning.transform.SetParent(leftEarBase.parent);
        leftLightning.GetComponent<Lightning>().SetPoints(leftEarBase, leftEarContact);

        GameObject rightLightning = Instantiate(lightningEffect);
        rightLightning.transform.SetParent(rightEarBase.parent);
        rightLightning.GetComponent<Lightning>().SetPoints(rightEarBase, rightEarContact);

        lightningTimerMax = Random.Range(lightningTimerRange.x, lightningTimerRange.y);
    }

    // Update is called once per frame
    void Update()
    {
        if (charging)
        {
            spinPivot.localEulerAngles += new Vector3(0, pivotSpinFireSpeed * Time.deltaTime, 0);
        }
        else
        {
            gemBobAngle += gemBobSpeed * Time.deltaTime;

            for (int i = 0; i < gems.Length; i++)
            {
                gems[i].localPosition = new Vector3(gems[i].localPosition.x, Mathf.Sin(gemBobAngle + (i / (float)gems.Length) * gemBobFrequency) * gemBobHeight, gems[i].localPosition.z);
                gems[i].localEulerAngles += gemSpeeds[i] * Time.deltaTime;
            }

            spinPivot.localEulerAngles += new Vector3(0, pivotSpinSpeed * Time.deltaTime, 0);

            leftEarBobAngle += leftEarBobSpeed * Time.deltaTime;
            rightEarBobAngle += rightEarBobSpeed * Time.deltaTime;

            leftEar.localPosition = leftEar.right * Mathf.Sin(leftEarBobAngle) * leftEarBobHeight + leftEarPos;
            rightEar.localPosition = rightEar.right * Mathf.Sin(rightEarBobAngle) * rightEarBobHeight + rightEarPos;

            lightningTimer += Time.deltaTime;

            if (lightningTimer > lightningTimerMax)
            {
                GameObject lightning = Instantiate(lightningEffect);
                Destroy(lightning, lightningDestroyTimer);

                int gemIndex = Random.Range(0, gems.Length - 1);

                if (gemIndex == gems.Length - 1)
                {
                    lightning.GetComponent<Lightning>().SetPoints(gems[gemIndex].transform, gems[0].transform);
                }
                else
                {
                    lightning.GetComponent<Lightning>().SetPoints(gems[gemIndex].transform, gems[gemIndex + 1].transform);
                }

                lightningTimer = 0;
                lightningTimerMax = Random.Range(lightningTimerRange.x, lightningTimerRange.y);
            }
        }
    }

    public override void PlayFireSound()
    {
        base.PlayFireSound();

        foreach (GameObject lightning in lightningFire)
        {
            Destroy(lightning, lightningDestroyTimer);
        }

        lightningFire.Clear();
        charging = false;
    }

    public override void PlayExtra1Sound()
    {
        base.PlayExtra1Sound();

        for (int i = 0; i < gems.Length; i++)
        {
            lightningFire.Add(Instantiate(lightningEffect));

            if (i == gems.Length - 1)
            {
                lightningFire[lightningFire.Count - 1].GetComponent<Lightning>().SetPoints(gems[i].transform, gems[0].transform);
            }
            else
            {
                lightningFire[lightningFire.Count - 1].GetComponent<Lightning>().SetPoints(gems[i].transform, gems[i + 1].transform);
            }
        }

        charging = true;
    }

    public override void PlayExtra2Sound()
    {
        base.PlayExtra2Sound();

        for (int i = 0; i < gems.Length; i++)
        {
            lightningFire.Add(Instantiate(lightningEffect));
            lightningFire[lightningFire.Count - 1].GetComponent<Lightning>().SetPoints(gems[i].transform, spinPivot);
        }
    }
}