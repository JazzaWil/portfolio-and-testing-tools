﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

[RequireComponent(typeof(Animator))]
public class WeaponBehaviour : MonoBehaviour
{
    public Weapon attributes;

    public Transform barrelEnd;
    public Transform muzzleFlashParent;

    public MeshFilter meshFilter;

    public bool dualWeapon = false;

    [HideInInspector] public WeaponController weaponController;
    [HideInInspector] public bool firing = false;

    protected bool shiftBarrelEnd = true;

    // Start is called before the first frame update
    public virtual void Start()
    {
        weaponController = gameObject.transform.GetComponentInParent<WeaponController>();

        if (shiftBarrelEnd)
        {
            Vector3 weaponScale = weaponController.transform.localScale;
            weaponController.transform.localScale = Vector3.one;
            Vector3 scaledPosition = barrelEnd.position;
            weaponController.transform.localScale = weaponScale;
            barrelEnd.position = scaledPosition;

            if (muzzleFlashParent == null)
            {
                muzzleFlashParent = Instantiate(barrelEnd, barrelEnd.parent);
                muzzleFlashParent.localPosition = barrelEnd.localPosition;
            }
        }
    }

    public virtual void PlayFireSound()
    {
        weaponController.SpawnProjectiles();
        AudioRandomiser.PlayAudio(weaponController.GetAudioSource(), attributes.fire);
    }

    public virtual void PlayUnloadSound()
    {
        AudioRandomiser.PlayAudio(weaponController.GetAudioSource(), attributes.unload);
    }

    public virtual void PlayReloadSound()
    {
        AudioRandomiser.PlayAudio(weaponController.GetAudioSource(), attributes.reload);
    }

    public virtual void PlayEquipSound()
    {
        AudioRandomiser.PlayAudio(weaponController.GetAudioSource(), attributes.equip);
    }

    public virtual void PlayUnequipSound()
    {
        AudioRandomiser.PlayAudio(weaponController.GetAudioSource(), attributes.unequip);
    }

    public virtual void PlayExtra1Sound()
    {
        AudioSource au = weaponController.GetAudioSource();
        au.clip = attributes.extra1;
        au.Play();
    }

    public virtual void PlayExtra2Sound()
    {
        AudioSource au = weaponController.GetAudioSource();
        au.clip = attributes.extra2;
        au.Play();
    }

    public virtual void PlayExtra3Sound()
    {
        AudioSource au = weaponController.GetAudioSource();
        au.clip = attributes.extra3;
        au.Play();
    }

    public virtual void CanFire()
    {
        if (weaponController.DualWielding())
        {
            if ((dualWeapon && weaponController.canFire) || (!dualWeapon && weaponController.canFire2))
            {
                weaponController.firing = false;
            }
        }
        else
        {
            weaponController.canFire = true;
            weaponController.canFire2 = true;
            weaponController.firing = false;
        }

        weaponController.reloading = false;
        weaponController.equipping = false;
    }

    public virtual void DualEquip()
    {
        weaponController.canFire = true;
    }

    public virtual void DualCanFire()
    {
        if (weaponController.DualWielding())
        {
            if (dualWeapon)
            {
                weaponController.canFire = true;
            }
            else
            {
                weaponController.canFire2 = true;
            }
        }
    }

    public virtual void EndReloading()
    {
        weaponController.EndReload();
    }
}