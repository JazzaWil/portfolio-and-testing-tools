﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destructible : Entity
{
    public GameObject objectToSpawn;

    public override void Die(GameObject attacker = null)
    {
        GameObject obj = Instantiate(objectToSpawn, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }
}
