﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CreateAssetMenu(fileName = "New Damage Flags", menuName = "Damage Flags")]
public class DamageFlags : ScriptableObject
{
    public float headShotMultiplier = 1;

    //[NamedArray(new string[] { "Default", "Player", "Enemy", "Barricade" })]
    [Tooltip("0 - Default\n1 - Player\n2 - Enemy\n3 - Barricade")]
    public bool[] flags = new bool[(int)Entity.Type.TYPE_COUNT];

    /*public class NamedArrayAttribute : PropertyAttribute
    {
        public readonly string[] names;
        public NamedArrayAttribute(string[] names) { this.names = names; }
    }

    [CustomPropertyDrawer(typeof(NamedArrayAttribute))]
    public class NamedArrayDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect rect, SerializedProperty property, GUIContent label)
        {
            try
            {
                int pos = int.Parse(property.propertyPath.Split('[', ']')[1]);
                EditorGUI.ObjectField(rect, property, new GUIContent(((NamedArrayAttribute)attribute).names[pos]));
            }
            catch
            {
                EditorGUI.ObjectField(rect, property, label);
            }
        }
    }*/
}