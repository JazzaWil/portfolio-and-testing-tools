﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Rewired;

public class MenuManager : MonoBehaviour
{
    CursorLockMode wantedMode;

    private AudioSource audioSource;
    private Player player;

    
    private void Start()
    {
        audioSource = this.GetComponent<AudioSource>();
        player = ReInput.players.GetPlayer(0);
        wantedMode = CursorLockMode.Confined;
    }

    // Update is called once per frame
    void Update()
    {
        if (player.controllers.joystickCount != 0)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
        else
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }

    public void ExitGameClicked()
    {
        Application.Quit();    
    }

    public void gotoScene(string name)
    {
        SceneManager.LoadScene(name);
    }
}
