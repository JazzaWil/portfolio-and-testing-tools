﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dynamite : Explosive
{
    [Tooltip("")]
    public float explosionTimer;
    public float rotationSpeed;

    private bool rotate = true;
    private float timer;

    // Start is called before the first frame update
    void Start()
    {
        base.Start();
        timer = explosionTimer;
    }

    // Update is called once per frame
    void Update()
    {
        timer -= Time.deltaTime;

        if(!destroyed && timer <= 0)
        {
            Explode();
        }

        base.Update();
    }

    private void FixedUpdate()
    {
        if (rotate)
        {
            transform.eulerAngles -= Vector3.forward * rotationSpeed;
        }
    }

    public override void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag != "Player")
        {
            rigidbody.velocity = Vector3.zero;
            rotate = false;
        }
    }
}
