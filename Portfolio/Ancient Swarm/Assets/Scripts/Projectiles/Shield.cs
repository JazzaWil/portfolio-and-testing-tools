﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UtilityAI;

public class Shield : Projectile
{
    public AudioClip[] enemyHitSounds;

    public Transform shield;
    public Transform spikes;

    public GameObject skeletonPrefab;

    public float shieldRotationSpeed;
    public float spikesRotationSpeed;
    public float flyDistance;
    public float flySpeed;
    public float returnLerp;

    private GameObject player;

    private Vector3 startPosition;
    private Vector3 startRotation;

    private bool flyEnd = false;
    private float flyAngle = 0;

    // Start is called before the first frame update
    public void Start()
    {
        base.Start();
        startPosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        shield.eulerAngles += Vector3.forward * shieldRotationSpeed * Time.deltaTime;
        spikes.eulerAngles += Vector3.forward * spikesRotationSpeed * Time.deltaTime;

        if (flyEnd)
        {
            transform.position = Vector3.Lerp(transform.position, player.transform.position, returnLerp * Time.deltaTime);

            if (Vector3.Distance(transform.position, player.transform.position) < 1.0f)
            {
                WeaponController weaponController = player.GetComponent<PlayerController>().weaponController;
                weaponController.GiveAmmo(weaponController.shieldIndex, 1);
                weaponController.CurrentWeapon().GetComponent<Animator>().SetTrigger("FireEnd");

                Destroy(gameObject);
            }
        }
        else
        {
            flyAngle += flySpeed * Time.deltaTime;
            transform.position = startPosition + startRotation * Mathf.Sin(flyAngle) * flyDistance;

            if (flyAngle > 1.57f)
            {
                flyEnd = true;
            }
        }
    }

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.tag == "Shield")
        {
            Destroy(gameObject);
            return;
        }

        base.ObjectHit(collision.gameObject);

        Entity entity = collision.GetComponentInParent<Entity>();
        Mummy mummy = entity.GetComponent<Mummy>();

        if (entity == null)
        {
            flyEnd = true;
        }
        else if (mummy != null && entity.IsAlive())
        {
            GameObject skeleton = Instantiate(skeletonPrefab, collision.gameObject.transform.position, collision.gameObject.transform.rotation, collision.gameObject.transform.parent);
            Entity skeletonEntity = skeleton.GetComponent<Entity>();
            Skeleton skeletonScript = skeleton.GetComponent<Skeleton>();

            skeletonScript.sword.SetActive(false);
            skeletonScript.armScraps.SetActive(true);
            skeletonScript.lowerScraps.SetActive(true);

            skeletonScript.pointsEarned = entity.GetComponent<Mummy>().pointsEarned;
            skeletonScript.SetCrawling(mummy.IsCrawling());
            skeletonEntity.SetHealthPercentage(entity.GetHealthPercentage(), owner);

            Destroy(collision.gameObject);
        }
    }

    public override void PlayHitSound()
    {
        AudioRandomiser.PlayAudio(audioSource, enemyHitSounds);
    }

    public override bool ShowHitSounds()
    {
        return false;
    }

    public void SetOwner(GameObject owner)
    {
        player = owner;
        startRotation = player.GetComponent<PlayerController>().weaponController.transform.forward;
    }
}