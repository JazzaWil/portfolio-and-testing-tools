﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(Rigidbody))]
public class Explosive : Entity
{
    public float chainExplosionTimer = 0.25f;
    public float explosionRadius;
    public float explosionForce;

    public float explosionDestroyTimer;
    public float explosionScale;
    public GameObject[] explosions;

    public AudioClip[] explosionSounds;

    private AudioSource audioSource;
    private MeshRenderer meshRenderer;
    [HideInInspector] public new Rigidbody rigidbody;

    private float damage;
    private DamageFlags damageFlags;
    [HideInInspector] public bool destroyed = false;
    private List<Explosive> chainExplosives;
    private float chainExplosionTime;

    private GameObject owner;

    // Start is called before the first frame update
    public void Start()
    {
        base.Start();

        audioSource = GetComponent<AudioSource>();
        meshRenderer = GetComponentInChildren<MeshRenderer>();
        rigidbody = GetComponent<Rigidbody>();

        chainExplosives = new List<Explosive>();
    }

    // Update is called once per frame
    public void Update()
    {
        if(chainExplosives.Count != 0)
        {
            chainExplosionTime += Time.deltaTime;

            if (chainExplosionTime >= chainExplosionTimer)
            {
                foreach (Explosive ex in chainExplosives)
                {
                    ex.Explode();
                }

                chainExplosives.Clear();
                chainExplosionTime = 0;
            }
        }

        if(destroyed && !audioSource.isPlaying)
        {
            Destroy(transform.gameObject);
        }
    }

    public void Init(DamageFlags setDamageFlags, float setDamage, GameObject setOwner = null)
    {
        damageFlags = setDamageFlags;
        damage = setDamage;
        owner = setOwner;
    }

    public void Explode(int shieldInstanceID = -1)
    {
        if (!destroyed)
        {
            destroyed = true;
            RaycastHit[] hits = Physics.SphereCastAll(transform.position, explosionRadius, transform.forward);

            if (hits.Length != 0)
            {
                for (int i = 0; i < hits.Length; i++)
                {
                    float explosionDamage = damage - (damage * (Vector3.Distance(transform.position, hits[i].transform.position) / explosionRadius));

                    Explosive ex = hits[i].transform.GetComponent<Explosive>();

                    if (ex != null)
                    {
                        chainExplosives.Add(ex);
                    }
                    else
                    {
                        Entity en = hits[i].transform.GetComponentInParent<Entity>();

                        if (en != null && shieldInstanceID != en.GetInstanceID())
                        {
                            //if (damageFlags.flags[(int)en.type])
                            {
                                en.Damage(explosionDamage, owner, true);

                                if (en.IsDead())
                                {
                                    foreach (Rigidbody rb in en.GetComponentsInChildren<Rigidbody>())
                                    {
                                        rb.AddExplosionForce(explosionForce, transform.position, explosionRadius);
                                    }
                                }
                            }
                        }
                        else
                        {
                            Rigidbody rb = hits[i].transform.GetComponent<Rigidbody>();

                            if (rb != null)
                            {
                                rb.AddExplosionForce(explosionForce, transform.position, explosionRadius);
                            }
                        }
                    }
                }
            }

            AudioRandomiser.PlayAudio(audioSource, explosionSounds);

            GameObject explosion = Instantiate(explosions[(int)Math.Round(UnityEngine.Random.value * (explosions.Length - 1), MidpointRounding.ToEven)], transform.position, Quaternion.identity);
            explosion.transform.localScale = new Vector3(explosionScale, explosionScale, explosionScale);
            Destroy(explosion, explosionDestroyTimer);

            rigidbody.isKinematic = true;
            meshRenderer.enabled = false;
        }
    }

    public virtual void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag != "Player")
        {
            if (collision.transform.tag == "Shield")
            {
                Entity en = collision.transform.GetComponentInParent<Entity>();

                if (en != null)
                {
                    Explode(en.GetInstanceID());
                }
                else
                {
                    Explode();
                }
            }
            else
            {
                Explode();
            }
        }
    }

    public override void Die(GameObject attacker = null)
    {
        Explode();
    }
}