﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UtilityAI;

[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(BoxCollider))]
public class MeleeHitbox : Projectile
{
    public GameObject skeletonPrefab;

    [Header("Sounds")]
    public AudioClip[] enemyHitSounds;
    public AudioClip[] terrainHitSounds;

    private new BoxCollider collider;

    private bool enemyHit = false;
    private bool terrainHit = false;

    private bool frameWait = false;

    // Start is called before the first frame update
    void Start()
    {
        base.Start();
        collider = GetComponent<BoxCollider>();

        transform.position += transform.forward * (collider.size.z / 2);
    }

    // Update is called once per frame
    void Update()
    {
        if (frameWait)
        {
            collider.enabled = false;

            if (enemyHit)
            {
                AudioRandomiser.PlayAudio(audioSource, enemyHitSounds);

                enemyHit = false;
                Destroy(this, audioSource.clip.length);
            }
            else if (terrainHit)
            {
                Debug.Log("terrain");

                AudioRandomiser.PlayAudio(audioSource, terrainHitSounds);

                terrainHit = false;
                Destroy(this, audioSource.clip.length);
            }
            else
            {
                //Destroy(gameObject);
            }
        }
        else
        {
            frameWait = true;
        }
    }

    public void SetDamage(float setDamage)
    {
        damage = setDamage;
    }

    public void SetHitForce(float force)
    {
        hitForce = force;
    }

    void OnTriggerEnter(Collider collision)
    {
        if (collision.tag == "Shield")
        {
            Destroy(gameObject);
            return;
        }

        ObjectHit(collision.gameObject);

        Entity entity = collision.GetComponentInParent<Entity>();
        Mummy mummy = entity.GetComponent<Mummy>();

        if (entity == null)
        {
            terrainHit = true;
        }
        else if(mummy != null && entity.IsAlive())
        {
            GameObject skeleton = Instantiate(skeletonPrefab, collision.gameObject.transform.position, collision.gameObject.transform.rotation, collision.gameObject.transform.parent);
            Entity skeletonEntity = skeleton.GetComponent<Entity>();
            Skeleton skeletonScript = skeleton.GetComponent<Skeleton>();

            skeletonScript.sword.SetActive(false);
            skeletonScript.armScraps.SetActive(true);
            skeletonScript.lowerScraps.SetActive(true);

            skeletonScript.pointsEarned = entity.GetComponent<Mummy>().pointsEarned;
            skeletonScript.SetCrawling(mummy.IsCrawling());
            skeletonEntity.SetHealthPercentage(entity.GetHealthPercentage(), owner);

            Destroy(collision.gameObject);
        }
    }

    public override void DamageEntity(Entity en, bool headshot)
    {
        base.DamageEntity(en, headshot);
        enemyHit = true;
    }

    public override void PlayHitSound() {}

    public override bool ShowHitSounds()
    {
        return false;
    }
}