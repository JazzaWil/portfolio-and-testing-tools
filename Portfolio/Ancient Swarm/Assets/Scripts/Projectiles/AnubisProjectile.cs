﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SphereCollider))]
public class AnubisProjectile : Projectile
{
    private new SphereCollider collider;

    // Start is called before the first frame update
    void Start()
    {
        base.Start();
        collider = GetComponent<SphereCollider>();
    }

    void OnTriggerEnter(Collider collision)
    {
        if (collision.tag == "Shield")
        {
            Destroy(gameObject);
            return;
        }

        GameObject obj = collision.gameObject;
        Projectile pr = obj.GetComponent<Projectile>();

        if (pr == null)
        {
            bool destroy = true;

            Entity en = obj.GetComponentInParent<Entity>();

            if (en != null)
            {
                if (en.gameObject.GetInstanceID() != owner.GetInstanceID() && damageFlags.flags[(int)en.type])
                {
                    DamageEntity(en, false);

                    if (en.IsDead())
                    {
                        foreach (Rigidbody rb in en.GetComponentsInChildren<Rigidbody>())
                        {
                            rb.AddForce(transform.forward * hitForce);
                        }
                    }

                    destroy = false;
                }
            }
            else
            {
                Rigidbody rb = obj.transform.GetComponent<Rigidbody>();

                if (rb != null)
                {
                    PushRigidbody(rb);
                }
            }

            if (destroy)
            {
                PlayHitSound();
            }
        }
    }

    public override void DamageEntity(Entity en, bool headshot)
    {
        en.Kill(owner);
    }
}
