﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetProjectile : Projectile
{
    public GameObject lightningEffect;
    public uint chainCount;
    public float chainRange;
    public float magnetismRange;

    private List<Entity> enemies;
    private Vector3 lastPos;

    public void HitFail(Transform startPos, Vector3 endPos, bool wallHit)
    {
        if (wallHit)
        {
            transform.position = startPos.position;
            transform.LookAt(endPos);

            RaycastHit[] hits = Physics.SphereCastAll(transform.position, magnetismRange, transform.forward);

            if (hits.Length == 0)
            {
                HitFail(startPos, endPos, false);
            }
            else
            {
                int bestHit = -1;
                float bestDistance = float.PositiveInfinity;

                for (int i = 0; i < hits.Length; i++)
                {
                    Entity en = hits[i].transform.GetComponent<Entity>();

                    if (en != null && damageFlags.flags[(int)en.type] && hits[i].transform.gameObject.GetInstanceID() != owner.GetInstanceID())
                    {
                        float distance = Vector3.Distance(en.transform.position, endPos);

                        if (distance < bestDistance)
                        {
                            bestHit = i;
                            bestDistance = distance;
                        }
                    }
                }

                if (bestHit == -1)
                {
                    HitFail(startPos, endPos, false);
                }
                else
                {
                    ChainHit(startPos, hits[bestHit].transform.gameObject);
                }
            }
        }
        else
        {
            GameObject lightning = Instantiate(lightningEffect);
            lightning.GetComponent<Lightning>().SetPoints(startPos, endPos);
        }
    }

    public void ChainHit(Transform startPos, GameObject hit)
    {
        enemies = new List<Entity>();
        lastPos = startPos.position;

        Entity entity = hit.GetComponentInParent<Entity>();

        if (entity != null && damageFlags.flags[(int)entity.type])
        {
            ChainHit(hit, chainCount);
        }
        else
        {
            HitFail(startPos, hit.transform.position, true);
        }
        
        Destroy(gameObject);
    }

    public void ChainHit(GameObject hit, uint chainAmount)
    {
        Entity entity = hit.GetComponentInParent<Entity>();

        if (entity != null)
        {
            if (damageFlags.flags[(int)entity.type] && !enemies.Contains(entity) && hit.GetInstanceID() != owner.GetInstanceID())
            {
                GameObject lightning = Instantiate(lightningEffect);
                Lightning lightningScript = lightning.GetComponent<Lightning>();

                Vector3 entityPos = entity.GetCenter();

                lightningScript.SetPoints(lastPos, entityPos);
                lastPos = entityPos;

                DamageEntity(entity, false);
                enemies.Add(entity);

                if (entity.IsDead())
                {
                    foreach (Rigidbody rb in entity.GetComponentsInChildren<Rigidbody>())
                    {
                        rb.AddExplosionForce(hitForce, rb.transform.position, chainRange);
                    }
                }

                if (chainAmount != 0)
                {
                    RaycastHit[] raycastHits = Physics.SphereCastAll(entityPos, chainRange, transform.forward);

                    foreach (RaycastHit ray in raycastHits)
                    {
                        ChainHit(ray.transform.gameObject, chainAmount - 1);
                    }
                }
            }
        }
    }

    public override bool ShowHitSounds()
    {
        return false;
    }
}