﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laser : Projectile
{
    private LineRenderer lineRenderer;

    private Transform startPos;
    private Vector3 endPos;

    // Start is called before the first frame update
    void Awake()
    {
        lineRenderer = GetComponent<LineRenderer>();
        lineRenderer.positionCount = 2;
    }

    // Update is called once per frame
    void Update()
    {
        lineRenderer.SetPosition(0, startPos.position);
        lineRenderer.SetPosition(1, endPos);
    }

    public void SetPoints(Transform start, Vector3 end)
    {
        startPos = start;
        endPos = end;

        lineRenderer.SetPosition(0, startPos.position);
        lineRenderer.SetPosition(1, endPos);
    }
}
