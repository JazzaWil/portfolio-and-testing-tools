﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lightning : MonoBehaviour
{
    public float destroyTimer;
    public uint segmentCount = 12;
    public float positionRange = 0.15f;

    private LineRenderer lineRenderer;

    private Vector3 startPos;
    private Vector3 endPos;

    private Transform startTransform;
    private Transform endTransform;

    // Start is called before the first frame update
    void Awake()
    {
        lineRenderer = GetComponent<LineRenderer>();
        lineRenderer.positionCount = (int)segmentCount;

        if (destroyTimer != 0.0f)
        {
            Destroy(gameObject, destroyTimer);
        }
    }

    // Update is called once per frame
    void Update()
    {
        LightningUpdate();
    }

    public void SetPoints(Transform start, Transform end)
    {
        startTransform = start;
        endTransform = end;

        LightningUpdate();
    }

    public void SetPoints(Transform start, Vector3 end)
    {
        startTransform = start;
        endPos = end;

        LightningUpdate();
    }

    public void SetPoints(Vector3 start, Vector3 end)
    {
        startPos = start;
        endPos = end;

        LightningUpdate();
    }

    void LightningUpdate()
    {
        if (startTransform != null)
        {
            startPos = startTransform.position;
        }

        if (endTransform != null)
        {
            endPos = endTransform.position;
        }

        transform.position = startPos;
        transform.LookAt(endPos);

        lineRenderer.SetPosition(0, startPos);
        lineRenderer.SetPosition(lineRenderer.positionCount - 1, endPos);

        float length = Vector3.Distance(lineRenderer.GetPosition(0), lineRenderer.GetPosition(lineRenderer.positionCount - 1)) / (lineRenderer.positionCount - 1);

        for (int i = 1; i < segmentCount - 1; i++)
        {
            Vector3 segmentForward = i * length * transform.forward;
            Vector3 offset = new Vector3(Random.Range(-positionRange, positionRange), Random.Range(-positionRange, positionRange), 0);

            lineRenderer.SetPosition(i, segmentForward + Vector3.Cross(transform.forward, offset) + lineRenderer.GetPosition(0));
        }
    }
}
