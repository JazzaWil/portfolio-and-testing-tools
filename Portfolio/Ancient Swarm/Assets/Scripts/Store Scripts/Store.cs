﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

[RequireComponent(typeof(AudioSource))]
public class Store : MonoBehaviour
{
    public enum Type
    {
        WEAPON,
        DOOR,
        TRAP,
        UPGRADE
    }

    public StoreSettings storeSettings;
    public Type type;

    [ShowIf("IsNotUpgrade")]
    public uint cost;

    [ShowIf("IsWeapon")]
    public uint weaponIndex = 1;
    [ShowIf("IsWeapon")]
    public uint ammoCost;
    [ShowIf("IsWeapon")]
    [Tooltip("How much ammo the player receives")]
    public uint ammoCount;
    [ShowIf("IsDoor")]
    public Vector3 doorOpenPos;
    [ShowIf("IsTrap")]
    public Traps[] traps;
    [ShowIf("IsUpgrade")]
    public GameObject weaponMesh;

    private AudioSource audioSource;

    private Vector3 doorPos;
    private bool doorOpen = false;
    private float doorOpenAmount = 0;
    //public GameObject bulletUI;

    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();

        if(type == Type.DOOR)
        {
            doorPos = transform.position;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(doorOpen)
        {
            doorOpenAmount += storeSettings.doorOpenSpeed * Time.deltaTime;
            transform.position = doorOpenPos * doorOpenAmount + doorPos;

            if (doorOpenAmount > 1.0f)
            {
                transform.position = doorPos + doorOpenPos;
                Destroy(this);
            }
        }
    }

    public string GetOutputText(PlayerController player)
    {
        WeaponController weaponController = player.weaponController;
        string interactButton = player.GetButtonName(player.interactButton);

        int playerPoints = player.GetPoints();

        switch (type)
        {
            case Type.WEAPON:
                string outputText = "";
                string weaponName = weaponController.weaponAttributes[weaponIndex].name;

                if (weaponController.CanPickupWeapon(weaponIndex))
                {
                    //can buy
                    if (playerPoints >= cost)
                    {
                        if (!weaponController.HasWeapon(weaponIndex))
                        {
                            if (weaponController.InventoryFull())
                            {
                                outputText = "Replace " + weaponController.CurrentAttributes().name + " with " + weaponName + " for " + cost.ToString() + " Souls?\nHold " + interactButton;
                            }
                            else
                            {
                                outputText = "Buy " + weaponName + " for " + cost.ToString() + " Souls?\nHold " + interactButton;
                            }
                        }
                        else
                        {
                            outputText = "Buy Dual " + weaponName + "s for " + cost.ToString() + " Souls?\nHold " + interactButton;
                        }
                    }
                    //not enough money
                    else
                    {
                        if (!weaponController.HasWeapon(weaponIndex))
                        {
                            outputText = "You Need " + cost.ToString() + " Souls to Buy " + weaponName;
                        }
                        else
                        {
                            outputText = "You Need " + cost.ToString() + " Souls to Buy Dual " + weaponName + "s";
                        }
                    }
                }

                if (outputText != "")
                {
                    outputText += "\n\n";
                }

                if (weaponController.HasWeapon(weaponIndex) && !weaponController.weaponAttributes[weaponIndex].infiniteAmmo)
                {
                    //full ammo
                    if(weaponController.CurrentAmmo() == weaponController.CurrentAmmoCapacity())
                    {
                        outputText += weaponName + " Ammo is Full";
                    }
                    else
                    {
                        //can afford ammo
                        if (playerPoints >= ammoCost)
                        {
                            outputText += "Buy " + ammoCount.ToString() + " " + weaponName + " Ammo for " + ammoCost.ToString() + " Souls?\nPress " + interactButton;
                           // bulletUI.SetActive(true);
                        }
                        //cant afford ammo
                        else
                        {
                            outputText += "You Need " + ammoCost.ToString() + " Souls to Buy " + ammoCost + " " + weaponName + " Ammo";
                           // bulletUI.SetActive(true);
                        }
                    }
                }

                return outputText;
                break;

            case Type.UPGRADE:

                weaponMesh.GetComponent<MeshFilter>().mesh = weaponController.CurrentWeapon().GetComponent<WeaponBehaviour>().meshFilter.mesh;

                if (weaponController.GetUpgradeMax() != 1)
                {
                    if (weaponController.GetUpgrade() != weaponController.GetUpgradeMax() - 1)
                    {
                        uint currentCost = weaponController.CurrentAttributes().upgradeCost[weaponController.GetUpgrade()];

                        if (playerPoints >= currentCost)
                        {
                            return "Upgrade " + weaponController.CurrentAttributes().name + " for " + currentCost.ToString() + " Souls?\nPress " + interactButton;
                        }
                        else
                        {
                            return "You Need " + currentCost.ToString() + " Souls to Upgrade " + weaponController.CurrentAttributes().name;
                        }
                    }
                    else
                    {
                        return weaponController.CurrentAttributes().name + " Fully Upgraded";
                    }
                }
                break;

            case Type.DOOR:
                if (!doorOpen)
                {
                    if (playerPoints >= cost)
                    {
                        return "Open Door for " + cost.ToString() + " Souls?\nPress " + interactButton;
                    }
                    else
                    {
                        return "You Need " + cost.ToString() + " Souls to Open Door";
                    }
                }
                break;

            case Type.TRAP:
                if (playerPoints >= cost)
                {
                    return "Activate Trap for " + cost.ToString() + " Souls?\nPress " + interactButton;
                }
                else
                {
                    return "You Need " + cost.ToString() + " Souls to Activate Trap";
                }
                break;
        }

        return "";
    }

    public bool Interact(PlayerController player, bool hold)
    {
        int playerPoints = player.GetPoints();
        WeaponController weaponController = player.weaponController;

        switch (type)
        {
            case Type.WEAPON:

                if (hold)
                {
                    if (playerPoints >= cost && weaponController.GiveWeapon(weaponIndex) && !player.god)
                    {
                        player.TakePoints(cost);

                        if (storeSettings.weaponSound != null)
                        {
                            audioSource.clip = storeSettings.weaponSound;
                            audioSource.Play();
                        }

                        return true;
                    }
                }
                else
                {
                    if (playerPoints >= ammoCost && weaponController.HasWeapon(weaponIndex) && weaponController.GiveAmmo(weaponIndex, ammoCount) && !player.god)
                    {
                        player.TakePoints(ammoCost);

                        if (storeSettings.ammoSound != null)
                        {
                            audioSource.clip = storeSettings.ammoSound;
                            audioSource.Play();
                        }

                        return true;
                    }
                }
                
                break;

            case Type.UPGRADE:

                uint upgradeIndex = weaponController.GetUpgrade();

                if (upgradeIndex < weaponController.GetUpgradeMax() - 1)
                {
                    uint currentCost = weaponController.CurrentAttributes().upgradeCost[upgradeIndex];

                    if (playerPoints >= currentCost && weaponController.Upgrade())
                    {
                        if (storeSettings.upgradeSound != null)
                        {
                            audioSource.clip = storeSettings.upgradeSound;
                            audioSource.Play();
                        }

                        if(GetComponentInParent<Animator>() != null)
                        {
                            GetComponentInParent<Animator>().SetTrigger("Enchantment");
                        }

                        player.TakePoints(currentCost);
                        return true;
                    }
                }

                break;

            case Type.DOOR:

                if (!doorOpen)
                {
                    if (player.TakePoints(cost))
                    {
                        if (storeSettings.doorSound != null)
                        {
                            audioSource.clip = storeSettings.doorSound;
                            audioSource.Play();
                        }

                        doorOpen = true;
                        return true;
                    }
                }

                break;

            case Type.TRAP:

                //TODO: check if traps have finished
                if (player.TakePoints(cost))
                {
                    if (storeSettings.trapSound != null)
                    {
                        audioSource.clip = storeSettings.trapSound;
                        audioSource.Play();
                        GetComponentInChildren<Animator>().SetTrigger("Activate");
                    }

                    foreach(Traps trap in traps)
                    {
                        trap.Activate();
                    }

                    return true;
                }

                break;
        }

        if (storeSettings.failSound != null)
        {
            audioSource.clip = storeSettings.failSound;
            audioSource.Play();
        }

        return false;
    }

    public bool CanHold(PlayerController player)
    {
        return type == Type.WEAPON && player.weaponController.CanPickupWeapon(weaponIndex);
    }

    public bool IsWeapon()
    {
        return type == Type.WEAPON;
    }

    public bool IsNotUpgrade()
    {
        return type != Type.UPGRADE;
    }

    public bool IsDoor()
    {
        return type == Type.DOOR;
    }

    public bool IsTrap()
    {
        return type == Type.TRAP;
    }

    public bool IsUpgrade()
    {
        return type == Type.UPGRADE;
    }
}