﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Store Settings", menuName = "Settings/Store Settings")]
public class StoreSettings : ScriptableObject
{
    public float doorOpenSpeed;

    [Header("Sounds")]
    public AudioClip weaponSound;
    public AudioClip ammoSound;
    public AudioClip upgradeSound;
    public AudioClip doorSound;
    public AudioClip trapSound;
    public AudioClip failSound;
}