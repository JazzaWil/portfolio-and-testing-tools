﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class EnemyFall : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerStay(Collider other)
    {
        if(other.GetComponent<NavMeshAgent>())
        {
            UtilityAI.Action[] tempActions = other.GetComponents<UtilityAI.Action>();
            for (int i = 0; i < tempActions.Length; i++)
            {
                tempActions[i].enabled = false;
            }
            other.GetComponent<UtilityAI.Agent>().Kill(other.gameObject);
        }
    }
}
