﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class Entity : MonoBehaviour
{
    //DO NOT add a new type after TYPE_COUNT or crashes may occur
    public enum Type
    {
        DEFAULT,
        PLAYER,
        ENEMY,
        BARRICADE,
        TYPE_COUNT
    }

    public float maxHealth;
    [Tooltip("Do not set type to TYPE COUNT or errors may occur")]
    public Type type = Type.DEFAULT;
    public Transform overrideCenter;

    //keep private to prevent health related functions from breaking
    private float health;

    // Start is called before the first frame update
    public virtual void Start()
    {
        health = maxHealth;
        
        if (type == Type.BARRICADE)
        {
            type = Type.BARRICADE;
        }
    }

    public Vector3 GetCenter()
    {
        if (overrideCenter == null)
        {
            return transform.position;
        }

        return overrideCenter.position;
    }

    public Vector3 GetLocalCenter()
    {
        if (overrideCenter == null)
        {
            return transform.position;
        }

        return overrideCenter.localPosition;
    }

    public float GetHealth()
    {
        return health;
    }

    public virtual void SetHealth(float setHealth, GameObject attacker = null)
    {
        health = setHealth;

        if(health <= 0)
        {
            Die(attacker);
        }
    }

    public virtual void Heal(float healAmount)
    {
        if (healAmount > 0 && IsAlive())
        {
            health += healAmount;

            if (health > maxHealth)
            {
                health = maxHealth;
            }
        }
    }
    
    public void FullyHeal()
    {
        health = maxHealth;
    }

    public void Revive(float healAmount)
    {
        health += healAmount;

        if (health > maxHealth)
        {
            health = maxHealth;
        }
    }

    public float GetHealthPercentage()
    {
        return health / maxHealth;
    }

    public void SetHealthPercentage(float percentage, GameObject attacker = null)
    {
        health = maxHealth * percentage;

        if (health <= 0)
        {
            Die(attacker);
        }
    }

    public virtual void Kill(GameObject attacker = null)
    {
        if (health > 0)
        {
            health = 0;
            Die(attacker);
        }
    }

    public virtual void Damage(float damageAmount, GameObject attacker = null, bool selfDamage = false)
    {
        if ((selfDamage || attacker.GetInstanceID() != gameObject.GetInstanceID()) && damageAmount > 0 && (health > 0 || (health == 0 && maxHealth == 0)))
        {
            health -= damageAmount;

            if (health <= 0)
            {
                Die(attacker);
            }
        }
    }

    public virtual void Die(GameObject attacker = null) { }

    public bool IsAlive()
    {
        return health > 0;
    }

    public bool IsDead()
    {
        return health <= 0;
    }
}
