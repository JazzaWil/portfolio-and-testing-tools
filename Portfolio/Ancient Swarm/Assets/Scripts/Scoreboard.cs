﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Scoreboard : MonoBehaviour
{
    public ScoreTemplate scoreTemplate;
    public Vector3 boardOffset;
    public float boardLerp;
    public float textSpace;

    private RectTransform rectTransform;
    private Vector3 defaultPos;

    private bool displayBoard;
    private List<ScoreTemplate> scores;

    // Start is called before the first frame update
    void Start()
    {
        rectTransform = GetComponent<RectTransform>();
        scores = new List<ScoreTemplate>();

        defaultPos = scoreTemplate.GetComponent<RectTransform>().localPosition;
        scoreTemplate.gameObject.SetActive(false);

        rectTransform.localPosition += boardOffset;

        gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (displayBoard)
        {
            rectTransform.localPosition = Vector3.Lerp(rectTransform.localPosition, Vector3.zero, boardLerp * Time.deltaTime);
        }
    }

    public void AddPlayer(string playerName, int score, uint kills, uint deaths)
    {
        GameObject newText = Instantiate(scoreTemplate.gameObject, transform);
        ScoreTemplate scoreText = newText.GetComponent<ScoreTemplate>();

        scoreText.playerName.text = playerName;
        scoreText.score.text = score.ToString();
        scoreText.kills.text = kills.ToString();
        scoreText.deaths.text = deaths.ToString();

        scores.Add(scoreText);
    }

    public void Display()
    {
        displayBoard = true;

        //sort scores
        for (int j = 0; j <= scores.Count - 2; j++)
        {
            for (int i = 0; i <= scores.Count - 2; i++)
            {
                if (int.Parse(scores[i].score.text) < int.Parse(scores[i + 1].score.text))
                {
                    (scores[i], scores[i + 1]) = (scores[i + 1], scores[i]);
                }
            }
        }

        for (int i = 0; i < scores.Count; i++)
        {
            scores[i].GetComponent<RectTransform>().localPosition = defaultPos + Vector3.down * i * textSpace;
            scores[i].gameObject.SetActive(true);
        }

        gameObject.SetActive(true);
    }

    public void Replay()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void MainMenu()
    {
        SceneManager.LoadScene(0);
    }
}
