﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "wave_", menuName = "Mechanics/Wave Mechanics/waveObject")]
public class Wave : ScriptableObject
{
    [Tooltip("List of the Enemies in the wave")]
    public List<Enemies> enemyList = new List<Enemies>();
    [Tooltip("How often enemies from the list spawn at each point")]
    public int spawnRate;



}
