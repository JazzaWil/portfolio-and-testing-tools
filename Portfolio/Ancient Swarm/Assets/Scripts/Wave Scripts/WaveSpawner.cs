﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UtilityAI;
using TMPro;
using Pixelplacement;

public class WaveSpawner : MonoBehaviour
{
    [Header("Waves")]
    public bool onlyUseTestWave;
    public bool timedWaves;
    public Wave TestingWave;
    public Wave normalWave;
    public Wave bossWave;
    public Wave miniBossWave;
    public Wave deathmatchWave;
    float miniBossCounter;
    bool deathmatch;

    [Space]
    public int currentWave;
    public Text waveText;
    public Text waveTextBg;
    public Text waveText2;
    public Text waveTextBg2;



    [Header("Multipliers")]
    [Tooltip("How much the'amount' variable is multiplied by on the enemy objects")]
    public float Multiplier;
    [Tooltip("How much you add onto the multiplier each round")]
    public float addOnMultiplier;

    [Space]
    public List<Stats> basicEnemies;
    public List<Stats> bossEnemies;
    public int switchToHealthMultiplier;
    public int switchToMidSpeed;
    public int switchToSpeedCap;

    [Header("Basic Enemy Multipliers")]
    public float basicEnemiesHealth;
    public float basicEnemiesHealthAddOn;

    [Header("Boss Enemy Multipliers")]
    public float bossEnemiesHealth;
    public float bossEnemiesHealthAddOn;

    [Header("Counters")]
    public float timeBetweenWaves = 5f;
    public float waveCountdown;
    private float searchCountdown = 1f;
    public float waveLength = 120f;
    float waveLengthDefault;

    [Space]
    public AnimationCurve scaleCurve;
    public AnimationCurve positionCurve;
    Vector3 StartSize = Vector3.zero;
    public Vector3 EndSize = Vector3.zero;
    Vector3 StartPosition = Vector3.zero;
    Vector3 EndPosition1 = Vector3.zero;
    public Vector3 EndPosition = Vector3.zero;
    public optionsVariables settings;

    // public Rect uiTweenTo
    // public Vector3 endPos = Vector3.zero;

    public enum SpawnState
    {
        SPAWNING,
        WAITING,
        COUNTING
    };

    public SpawnState state = SpawnState.COUNTING;
    public Transform bossSpawnPoint;
    public List<Transform> currentSpawnPoints = new List<Transform>();

    [Header("Sounds")]
    public AudioClip normalWaveChange;
    public AudioClip bossWaveChange;
    public AudioClip miniBossWaveChange;
    public AudioClip waveIntermission;
    public AudioClip waveStart;
    public AudioSource audioSource;

    private void Start()
    {
        StartSize = waveText.transform.localScale;
        EndPosition = waveText.transform.localPosition;
        EndPosition1 = waveTextBg.transform.localPosition;

        waveCountdown = timeBetweenWaves;

        for (int i = 0; i < basicEnemies.Count; i++)
        {
            basicEnemies[i].health = basicEnemies[i].defaultHealth;
            basicEnemies[i].speed = basicEnemies[i].defaultSpeed;
        }
        for (int i = 0; i < bossEnemies.Count; i++)
        {
            bossEnemies[i].health = bossEnemies[i].defaultHealth;
            bossEnemies[i].speed = bossEnemies[i].defaultSpeed;
        }

        //audioSource = GetComponent<AudioSource>();
        waveLengthDefault = waveLength;
        timedWaves = settings.timedMode;

        deathmatch = FindObjectOfType<GameController>().gamemode == GameController.Gamemode.DEATHMATCH;

        if(timedWaves)
        {
            waveText2.gameObject.SetActive(true);
            waveTextBg2.gameObject.SetActive(true);
        }
    }

    private void Update()
    {
        if (state == SpawnState.SPAWNING || state == SpawnState.WAITING)
        {
            if (timedWaves)
            {
                waveLength -= Time.deltaTime;

                uint waveLengthInt = (uint)waveLength + 1;

                waveText.text = "Time Until Next Round : " + waveLengthInt;
                waveTextBg.text = "Time Until Next Round : " + waveLengthInt;
                waveText2.text = "Wave " + currentWave.ToString();
                waveTextBg2.text = "Wave " + currentWave.ToString();
            }
            else
            {
                waveText.text = "Wave " + currentWave.ToString();
                waveTextBg.text = "Wave " + currentWave.ToString();
            }
        }

        if (state == SpawnState.WAITING)
        {
            //Are Enemies Still Alive?
            if (!IsEnemyAlive() || waveLength <= 0)
            {
                WaveCompleted();
            }
            else
            {
                return;
            }
        }

        if (waveCountdown <= 0 || waveLength <= 0 && timedWaves)
        {
            if (state != SpawnState.SPAWNING)
            {
                //Add ons for speed and health
                if (currentWave <= switchToHealthMultiplier)
                {
                    for (int i = 0; i < basicEnemies.Count; i++)
                    {
                        basicEnemies[i].health += basicEnemiesHealthAddOn;
                        if (basicEnemies[i].health > basicEnemies[i].healthCap)
                        {
                            basicEnemies[i].health = basicEnemies[i].healthCap;
                        }
                    }
                    if (currentWave % 10 == 0)
                    {
                        for (int i = 0; i < bossEnemies.Count; i++)
                        {
                            bossEnemies[i].health += bossEnemiesHealthAddOn;
                            if (bossEnemies[i].health > bossEnemies[i].healthCap)
                            {
                                bossEnemies[i].health = bossEnemies[i].healthCap;
                            }
                        }
                    }

                }
                else
                {
                    for (int i = 0; i < basicEnemies.Count; i++)
                    {
                        basicEnemies[i].health *= basicEnemiesHealth;
                        if (basicEnemies[i].health > basicEnemies[i].healthCap)
                        {
                            basicEnemies[i].health = basicEnemies[i].healthCap;
                        }
                    }
                    if (currentWave % 10 == 0)
                    {
                        for (int i = 0; i < bossEnemies.Count; i++)
                        {
                            bossEnemies[i].health *= bossEnemiesHealth;
                            if (bossEnemies[i].health > bossEnemies[i].healthCap)
                            {
                                bossEnemies[i].health = bossEnemies[i].healthCap;
                            }
                        }
                    }
                }

                if (currentWave >= switchToMidSpeed)
                {
                    for (int i = 0; i < basicEnemies.Count; i++)
                    {
                        basicEnemies[i].speed = basicEnemies[i].midSpeed;
                    }
                    for (int i = 0; i < bossEnemies.Count; i++)
                    {
                        bossEnemies[i].speed = bossEnemies[i].midSpeed;
                    }
                }

                if (currentWave >= switchToSpeedCap)
                {
                    for (int i = 0; i < basicEnemies.Count; i++)
                    {
                        basicEnemies[i].speed = basicEnemies[i].speedCap;
                    }
                    for (int i = 0; i < bossEnemies.Count; i++)
                    {
                        bossEnemies[i].speed = bossEnemies[i].speedCap;
                    }
                }

#if UNITY_EDITOR
                if (onlyUseTestWave)
                {
                    StartCoroutine(SpawnWave(TestingWave));
                }
#endif
                //Start spawning the wave
                if (deathmatch == false)
                {

                    if (currentWave % 10 == 0 && !onlyUseTestWave)
                    {
                        miniBossCounter = 0;
                        audioSource.clip = bossWaveChange;
                        audioSource.Play();
                        StartCoroutine(SpawnWave(bossWave));
                    }
                    else if (currentWave % 10 == 5 && miniBossCounter >= 0 && !onlyUseTestWave)
                    {
                        audioSource.clip = miniBossWaveChange;
                        audioSource.Play();
                        StartCoroutine(SpawnWave(miniBossWave));
                    }
                    else if (!onlyUseTestWave)
                    {
                        miniBossCounter++;
                        audioSource.clip = normalWaveChange;
                        audioSource.Play();
                        StartCoroutine(SpawnWave(normalWave));
                    }
                }
                else
                {
                    StartCoroutine(SpawnWave(deathmatchWave));
                }
            }

        }
        else
        {
            waveCountdown -= Time.deltaTime;
            waveLength -= Time.deltaTime;
            if (waveCountdown < 0)
            {
                waveCountdown = 0;
            }
            if (waveLength < 0)
            {
                waveLength = 0;
            }
            if(timedWaves)
            {
                waveText.text = "Next Wave Incoming";
                waveTextBg.text = "Next Wave Incoming";
            }
            else
            {
                waveText.text = waveCountdown.ToString("F2");
                waveTextBg.text = waveCountdown.ToString("F2");
            }

            ActivateTweening();
        }
    }

    IEnumerator SpawnWave(Wave _wave)
    {
        //Spawn enemies
        state = SpawnState.SPAWNING;

        for (int i = 0; i < _wave.enemyList.Count; i++)
        {
            for (int j = 0; j < (_wave.enemyList[i].Amount * FindObjectOfType<GameController>().playerCount) * Multiplier; j++)
            {
                SpawnEnemy(_wave.enemyList[i].enemy);
                yield return new WaitForSeconds(1f / _wave.spawnRate);
            }
        }

        //Wait for player to kill enemies

        state = SpawnState.WAITING;


        yield break;
    }

    void SpawnEnemy(Agent _enemy)
    {
        if (currentSpawnPoints.Count == 0)
        {
            Debug.LogError("No spawn points referenced");
        }
        else
        {
            if (_enemy.GetComponent<Pharaoh>() != null || _enemy.GetComponent<MiniBoss>() != null)
            {
                Transform _sp = bossSpawnPoint;
                Instantiate(_enemy, _sp.position, _sp.rotation);
            }
            else
            {
                Transform _sp = currentSpawnPoints[Random.Range(0, currentSpawnPoints.Count)];
                Instantiate(_enemy, _sp.position, _sp.rotation);
            }

        }
    }

    bool IsEnemyAlive()
    {
        searchCountdown -= Time.deltaTime;

        if (searchCountdown <= 0f)
        {
            searchCountdown = 1f;

            if (GameObject.FindGameObjectWithTag("Enemy") == null)
            {
                return false;
            }
        }

        return true;
    }

    void WaveCompleted()
    {
        state = SpawnState.COUNTING;
        waveCountdown = timeBetweenWaves;
        waveLength = waveLengthDefault;
        //Begin new wave
        currentWave++;
        Multiplier += addOnMultiplier;
        audioSource.clip = waveIntermission;
        audioSource.Play();
    }

    IEnumerator WaitForSound()
    {
        print("Audio!" + audioSource.clip.name);
        yield return new WaitUntil(() => audioSource.isPlaying == false);
    }

    public void ActivateTweening()
    {
        Tween.LocalScale(waveText.transform, StartSize, EndSize, 1f, 0, scaleCurve);
        Tween.LocalScale(waveTextBg.transform, StartSize, EndSize, 1f, 0, scaleCurve);
        Tween.LocalPosition(waveText.transform, StartPosition, EndPosition, 1f, 0, positionCurve);
        Tween.LocalPosition(waveTextBg.transform, StartPosition, EndPosition1, 5f, 0, positionCurve);

        //add the tween to the centre of the screen here
    }

}
