﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

[RequireComponent(typeof(AudioSource))]
public class Pickup : MonoBehaviour
{
    public enum Type
    {
        AMMO,
        WEAPON
    }

    public Type type;
    public GameObject mesh;
    public new ParticleSystem particleSystem;

    [Tooltip("Set to 0 for no respawn")]
    public float respawnTimer;
    public AudioClip[] pickupSounds;

    [Range (0, 1)] public float ammoPercentage;

    [ShowIf("IsWeapon")]
    public uint weaponIndex;
    [ShowIf("IsWeapon")]
    public bool giveDualWeapons;

    private AudioSource audioSource;
    private float respawnTime = 0;
    private float bounceAngle = 0;

    private Vector3 startPos;
    private bool pickedUp = false;
    private bool destroy = false;

    private const float bounceHeight = 0.25f;
    private const float bounceSpeed = 2.0f;
    private const float rotateSpeed = 40.0f;

    // Start is called before the first frame update
    void Start()
    {
        startPos = mesh.transform.position;

        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (respawnTime > 0 && !destroy)
        {
            respawnTime -= Time.deltaTime;

            if (respawnTime <= 0)
            {
                mesh.SetActive(true);
                particleSystem.Play();

                pickedUp = false;
            }
        }

        bounceAngle += bounceSpeed * Time.deltaTime;
        mesh.transform.eulerAngles += Vector3.up * rotateSpeed * Time.deltaTime;
        mesh.transform.position = startPos + Vector3.up * Mathf.Sin(bounceAngle) * bounceHeight;
    }

    void OnTriggerStay(Collider collision)
    {
        if (!pickedUp)
        {
            PlayerController player = collision.GetComponent<PlayerController>();

            if (player != null)
            {
                pickedUp = true;

                switch (type)
                {
                    case Type.AMMO:
                        player.weaponController.GiveAmmo(ammoPercentage);
                        break;

                    case Type.WEAPON:
                        if (player.weaponController.CanPickupWeapon(weaponIndex, false))
                        {
                            pickedUp = player.weaponController.GiveWeapon(weaponIndex, giveDualWeapons);
                        }
                        else
                        {
                            player.weaponController.GiveAmmo(weaponIndex, ammoPercentage);
                        }
                        break;
                }

                if (pickedUp)
                {
                    AudioRandomiser.PlayAudio(audioSource, pickupSounds);

                    mesh.SetActive(false);
                    particleSystem.Stop();

                    if (respawnTimer == 0)
                    {
                        Destroy(gameObject, audioSource.clip.length);
                    }
                    else
                    {
                        respawnTime = respawnTimer;
                    }
                }
            }
        }
    }

    public bool IsWeapon()
    {
        return type == Type.WEAPON;
    }
}
