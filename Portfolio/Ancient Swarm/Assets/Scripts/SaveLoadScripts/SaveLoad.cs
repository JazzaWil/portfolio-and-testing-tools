﻿using UnityEngine;
using System.Collections.Generic;
using System;
using System.IO;
using ProtoBuf;


public class SaveLoad : MonoBehaviour
{
    public List<HighScores> highScores = new List<HighScores>();
    string filePath;
    // Start is called before the first frame update
    void Start()
    {
        filePath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments) + "\\Ancient Swarm";
        if (!Directory.Exists(filePath));
        {
            Directory.CreateDirectory(filePath);
        }

        filePath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments) + "\\Ancient Swarm\\SaveData.dat";

        if (!File.Exists(filePath))
        {
            File.Create(filePath);
        }
       
        if (highScores != null && highScores.Count != 0)
        {

            List<HighScores> temp = new List<HighScores>();
           // Debug.Log("Temp Count: " + temp.Count);
           // Debug.Log("File path = " + filePath);
            temp = Serializer.Deserialize<List<HighScores>>(new FileStream(filePath, FileMode.Open, FileAccess.Read));
            for (int i = 0; i < highScores.Count; i++)
            {
                highScores[i].highScores = temp[i].highScores;
                highScores[i].sortHighScores();
            }
            

        }
    }

    private void OnApplicationQuit()
    {
        if (highScores != null && highScores.Count != 0)
        {
            using (FileStream Stream = new FileStream(filePath, FileMode.Create, FileAccess.Write))
            {
                    Serializer.Serialize<List<HighScores>>(Stream, highScores);

                    Stream.Flush();
                
            }
        }

    }
}
