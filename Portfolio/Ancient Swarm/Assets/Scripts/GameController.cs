﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Rewired;

public class GameController : MonoBehaviour
{
    public enum Gamemode
    {
        SURVIVAL,
        DEATHMATCH
    }

    public Gamemode gamemode;

    public uint playerCount = 1;
    public bool keyboardEnabled;

    [Header("Survival Rules")]
    public float survivalRespawnTimer;

    [Header("Deathmatch Rules")]
    public float matchTimer = 0;
    public uint maxFragCount = 0;
    public float deathmatchRespawnTimer;
    public uint playerKillAddPoints;
    public uint playerKillTakePoints;
    public uint playerSuicideTakePoints;
    public uint enemyKillTakePoints;

    [Header("UI")]
    public KillText killText;
    public Text[] timerText;
    public Scoreboard scoreboard;

    [Header("Objects")]
    public GameObject[] survivalObjects;
    public GameObject[] deathmatchObjects;

    private GameObject[] players;
    private GameObject[] spawnPoints;

    private List<PlayerController> playerControllers;
    private List<float> playerRespawn;
    private List<uint> fragCount;
    private List<uint> deathCount;

    [HideInInspector] public bool matchEnd = false;
    private float remainingTime = 0;

    // Start is called before the first frame update
    void Start()
    {
        GameObject[] gc = GameObject.FindGameObjectsWithTag("GameController");

        if (gc.Length != 1)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);

        players = GameObject.FindGameObjectsWithTag("Player");
        spawnPoints = GameObject.FindGameObjectsWithTag("Respawn");

        playerRespawn = new List<float>();
        fragCount = new List<uint>();
        deathCount = new List<uint>();

        for (int i = 0; i < playerCount; i++)
        {
            playerRespawn.Add(0);
            fragCount.Add(0);
            deathCount.Add(0);

            playerRespawn[i] = 0.01f;
        }

        //sort players by controller id
        for (int j = 0; j <= players.Length - 2; j++)
        {
            for (int i = 0; i <= players.Length - 2; i++)
            {
                if (players[i].GetComponent<PlayerController>().playerID > players[i + 1].GetComponent<PlayerController>().playerID)
                {
                    (players[i], players[i + 1]) = (players[i + 1], players[i]);
                }
            }
        }

        playerControllers = new List<PlayerController>();

        for (int i = 0; i < playerCount; i++)
        {
            playerControllers.Add(players[i].GetComponent<PlayerController>());
        }

        for (int i = (int)playerCount; i < players.Length; i++)
        {
            Destroy(players[i].GetComponent<PlayerController>().playerCanvas.gameObject);
            Destroy(players[i]);
        }

        if (keyboardEnabled)
        {
            for (int i = 1; i < playerCount; i++)
            {
                if (ReInput.players.GetPlayer(0).controllers.joystickCount != 0 && ReInput.players.GetPlayer(i).controllers.joystickCount == 0)
                {
                    ReInput.players.GetPlayer(0).controllers.RemoveController(ControllerType.Joystick, 0);
                    ReInput.players.GetPlayer(i).controllers.AddController(ControllerType.Joystick, 0, false);
                }
            }
        }

        if (playerCount == 2)
        {
            foreach (Camera cam in playerControllers[0].cameras)
            {
                Rect r = cam.rect;
                r.y = 0.5f;

                cam.rect = r;
            }

            foreach (Camera cam in playerControllers[1].cameras)
            {
                Rect r = cam.rect;
                r.y = -0.5f;

                cam.rect = r;
            }

            foreach (PlayerController playerController in playerControllers)
            {
                playerController.playerCanvas.transform.GetChild(0).GetComponent<RectTransform>().sizeDelta = new Vector2(100, -80);
            }
        }
        else if(playerCount > 2)
        {
            foreach (Camera cam in playerControllers[0].cameras)
            {
                Rect r = cam.rect;
                r.x = -0.5f;
                r.y = 0.5f;

                cam.rect = r;
            }

            foreach (Camera cam in playerControllers[1].cameras)
            {
                Rect r = cam.rect;
                r.x = 0.5f;
                r.y = 0.5f;

                cam.rect = r;
            }

            foreach (Camera cam in playerControllers[2].cameras)
            {
                Rect r = cam.rect;
                r.x = -0.5f;
                r.y = -0.5f;

                cam.rect = r;
            }

            foreach (Camera cam in playerControllers[3].cameras)
            {
                Rect r = cam.rect;
                r.x = 0.5f;
                r.y = -0.5f;

                cam.rect = r;
            }
        }

        switch (gamemode)
        {
            case Gamemode.SURVIVAL:

                foreach (GameObject obj in deathmatchObjects)
                {
                    obj.SetActive(false);
                }

                break;

            case Gamemode.DEATHMATCH:

                MatchBegin();

                foreach(GameObject obj in survivalObjects)
                {
                    obj.SetActive(false);
                }

            break;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!matchEnd)
        {
            for (int i = 0; i < playerCount; i++)
            {
                if (playerRespawn[i] > 0)
                {
                    playerRespawn[i] -= Time.deltaTime;

                    if (playerRespawn[i] <= 0)
                    {
                        float bestDistance = 0;
                        Transform bestSpawn = spawnPoints[0].transform;

                        foreach (GameObject spawnPoint in spawnPoints)
                        {
                            float distance = 0;

                            for (int p = 0; p < playerCount; p++)
                            {
                                distance += Vector3.Distance(players[p].transform.position, spawnPoint.transform.position);
                            }

                            if (distance > bestDistance)
                            {
                                bestDistance = distance;
                                bestSpawn = spawnPoint.transform;
                            }
                        }

                        players[i].transform.position = bestSpawn.position;
                        players[i].transform.eulerAngles = Vector3.up * bestSpawn.eulerAngles.y;

                        playerControllers[i].Respawn();
                    }
                }
            }
        }

        switch (gamemode)
        {
            case Gamemode.SURVIVAL:

                foreach (Text text in timerText)
                {
                    text.gameObject.SetActive(false);
                }

                if (AlivePlayerCount() == 0)
                {
                    MatchEnd();
                }

                break;

            case Gamemode.DEATHMATCH:

                if (remainingTime > 0)
                {
                    remainingTime -= Time.deltaTime;

                    string timeText;
                    uint seconds = GetRemainingSeconds();

                    if (seconds < 10)
                    {
                        timeText = GetRemainingMinutes() + ":0" + seconds;
                    }
                    else
                    {
                        timeText = GetRemainingMinutes() + ":" + seconds;
                    }

                    foreach (Text text in timerText)
                    {
                        text.gameObject.SetActive(true);
                        text.text = timeText;
                    }

                    if (remainingTime <= 0)
                    {
                        remainingTime = 0;
                        MatchEnd();
                    }
                }
                else
                {
                    foreach (Text text in timerText)
                    {
                        text.gameObject.SetActive(false);
                    }
                }

                break;
        }
    }

    private void MatchBegin()
    {
        remainingTime = matchTimer;

        foreach (PlayerController player in playerControllers)
        {
            player.disableMovement = false;
        }
    }

    private void MatchEnd()
    {
        if (!matchEnd)
        {
            for (int i = 0; i < playerControllers.Count; i++)
            {
                playerControllers[i].disableMovement = true;

                if (gamemode == Gamemode.DEATHMATCH)
                {
                    scoreboard.AddPlayer(playerControllers[i].GetName(), playerControllers[i].GetPoints(), fragCount[i], deathCount[i]);
                }
                else
                {
                    scoreboard.AddPlayer(playerControllers[i].GetName(), (int)playerControllers[i].GetTotalPoints(), fragCount[i], deathCount[i]);
                }
            }

            WaveSpawner wavespawner = FindObjectOfType<WaveSpawner>();

            if (wavespawner != null)
            {
                wavespawner.gameObject.SetActive(false);
            }

            GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");

            foreach (GameObject enemy in enemies)
            {
                enemy.GetComponent<Entity>().Kill();
            }

            scoreboard.Display();
            matchEnd = true;
        }
    }

    public uint GetRemainingMinutes()
    {
        return (uint)((remainingTime + 1) / 60);
    }

    public uint GetRemainingSeconds()
    {
        return ((uint)remainingTime + 1) % 60;
    }

    public uint AlivePlayerCount()
    {
        uint count = 0;

        foreach (PlayerController player in playerControllers)
        {
            if (player.IsAlive())
            {
                count++;
            }
        }

        return count;
    }

    public void PlayerDie(int index)
    {
        if (gamemode == Gamemode.DEATHMATCH)
        {
            playerRespawn[index - 1] = deathmatchRespawnTimer;
        }
        else
        {
            playerRespawn[index - 1] = survivalRespawnTimer;
        }
    }

    public void DisplayDeath(string killerName, string victimName, uint weaponIndex)
    {
        killText.AddText(killerName, victimName, weaponIndex);
    }

    public void AddFrag(uint player)
    {
        fragCount[(int)player]++;

        if (maxFragCount != 0 && fragCount[(int)player] == maxFragCount)
        {
            remainingTime = 0;
            MatchEnd();
        }
    }

    public uint GetFrags(uint player)
    {
        return fragCount[(int)player];
    }

    public void AddDeath(uint player)
    {
        deathCount[(int)player]++;
    }

    public uint GetDeaths(uint player)
    {
        return deathCount[(int)player];
    }

    public uint GetRespawnTime(uint player)
    {
        return (uint)playerRespawn[(int)player] + 1;
    }
}