﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

[CreateAssetMenu(fileName = "New Weapon", menuName = "Weapon")]
public class Weapon : ScriptableObject
{
    public new string name;

    [Header("Ammunition")]
    public bool infiniteAmmo = false;

    [ShowIf("InfiniteAmmoCheck")]
    [Tooltip("How much ammo the player will recieve when receiving the weapon")]
    public uint startingAmmo;

    [ShowIf("ClipCheck")]
    [Tooltip("Set to true for loading rounds individually")]
    public bool singleReload = false;

    [ShowIf("NoClipCheck")]
    public bool throwable = false;
    [ShowIf("ThrowableCheck")]
    [Tooltip("If the projectile needs to be returned to the player")]
    public bool reusable = false;
    [ShowIf("NoConstantDamage")]
    public bool dualWieldable = false;

    [Header("Firing")]
    [Range(0, 1)] public float accuracy = 1;
    public bool constantFire = false;
    [Tooltip("If the projectile is adjusted to hit the raycasted point")]
    public bool projectileAdjust = true;
    public float fireFOVChange = 1;
    public float fireFOVSpeed = 20;

    [Header("Vibration")]
    public int motorIndex = 0;
    [Range(0, 1)] public float vibrationIntensity = 0.5f;
    public float vibrationDuration = 0.1f;

    [Header("Upgrades")]
    public uint[] upgradeCost;
    [Tooltip("Element 0 is default")]
    public float[] damage = { 0 };
    [ShowIf("InfiniteAmmoCheck")]
    [Tooltip("Element 0 is default")]
    public uint[] clipSize;
    [ShowIf("InfiniteAmmoCheck")]
    [Tooltip("Element 0 is default")]
    public uint[] ammoCapacity;

    [Header("Deathmatch")]
    public float deathmatchDamage;
    [Range(0, 1)] public float deathmatchAccuracy = 1;
    [ShowIf("InfiniteAmmoCheck")]
    public uint deathmatchClipSize;
    [ShowIf("InfiniteAmmoCheck")]
    public uint deathmatchAmmoCapacity;

    [Header("Projectile")]
    public GameObject projectile;

    [Tooltip("-RAYCAST/MELEE-\nThe force applied to physics objects on hit\n-PROJECTILE-\nThe forward force applied to the projectile when spawned")]
    public float projectileForce = 0;
    [Tooltip("How many bullets are fired from the weapon in one shot")]
    public uint projectileFireCount = 1;

    [Tooltip("Time taken for projectile to despawn after firing (unused for melee weapons)")]
    [ShowIf("NoConstantDamage")]
    public float projectileDestroyTimer = 0.5f;

    [Header("Player Effects")]
    [Tooltip("How resistant to damage the player is while having this weapon equipped")]
    [Range(0, 1)] public float damageResistance = 0;

    [Header("Particle Effects")]
    [Tooltip("Optional")]
    public GameObject muzzleFlash;

    [ShowIf("MuzzleCheck")]
    public float muzzleDestroyTimer = 0.5f;

    [Header("Sounds")]
    public AudioClip[] fire;
    public AudioClip[] unload;
    public AudioClip[] reload;
    public AudioClip[] equip;
    public AudioClip[] unequip;
    public AudioClip extra1;
    public AudioClip extra2;
    public AudioClip extra3;

    public bool InfiniteAmmoCheck()
    {
        return !infiniteAmmo;
    }

    public bool ClipCheck()
    {
        return clipSize.Length != 0 && !infiniteAmmo;
    }

    public bool NoClipCheck()
    {
        return clipSize.Length == 0;
    }

    public bool MuzzleCheck()
    {
        return muzzleFlash != null;
    }

    public bool NoConstantDamage()
    {
        return !constantFire;
    }

    public bool ThrowableCheck()
    {
        return throwable;
    }
}