﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrameDestroy : MonoBehaviour
{
    private bool wait = true;

    // Update is called once per frame
    void Update()
    {
        if (wait)
        {
            wait = false;
        }
        else
        {
            Destroy(gameObject);
        }
    }
}