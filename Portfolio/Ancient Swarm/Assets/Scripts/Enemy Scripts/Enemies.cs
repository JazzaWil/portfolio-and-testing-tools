﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UtilityAI;

[CreateAssetMenu(fileName = "enemies_", menuName = "Mechanics/Wave Mechanics/Enemies")]
public class Enemies : ScriptableObject
{
    [Tooltip("Put the prefab for the enemies in here")]
    public Agent enemy;
    [Tooltip("Make sure your deviding this by the multiplyer as this number gets multiplied")]
    public int Amount;
}
