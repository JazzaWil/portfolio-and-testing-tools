﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace UtilityAI
{
    public class MiniBoss : Agent
    {
        public float ShieldTimer;

        // Start is called before the first frame update
        public void Start()
        {
            base.Start();
            myFlockingBehaviour = FlockingBehaviour.LEADER;

        }

        // Update is called once per frame
        public void Update()
        {
            base.Update();
        }

        public override void Damage(float damageAmount, GameObject attacker = null, bool selfDamage = false)
        {
            base.Damage(damageAmount, attacker, selfDamage);

            if(animator.GetBool("Shielding") == false)
            {
                StartCoroutine("Wait");
            }
        }

        IEnumerator Wait()
        {
            animator.SetBool("Shielding",true);
            yield return new WaitForSeconds(ShieldTimer);
            animator.SetBool("Shielding", false);

        }

        public override void Die(GameObject attacker = null)
        {
            base.Die(attacker);

            if (canDie)
            {
                Destroy(gameObject);
            }
        }
    }
}
