using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace UtilityAI
{
    public class Skeleton : Agent
    {
        [Header("SKELETON")]
        [Range(0, 1)] public float swordChance;
        [Range(0, 1)] public float armScrapsChance;
        [Range(0, 1)] public float lowerScrapsChance;
        public float dissolveSpeed;
        public float destroyTimer;

        [Header("Components")]
        public SphereCollider headCollider;
        public GameObject sword;
        public GameObject skull;
        public GameObject body;
        public GameObject leftArm;
        public GameObject rightArm;
        public GameObject leftLeg;
        public GameObject rightLeg;
        public GameObject armScraps;
        public GameObject lowerScraps;

        [Header("Physics Objects")]
        public GameObject skullPhysics;
        public GameObject bodyPhysics;
        public GameObject leftArmPhysics;
        public GameObject rightArmPhysics;
        public GameObject leftLegPhysics;
        public GameObject rightLegPhysics;

        private new CapsuleCollider collider;
        private new Rigidbody rigidbody;

        private float dissolveTimer = 0;
        private float dissolveAmount;

        public override void Start() { }

        public void Awake()
        {
            base.Start();

            if (Random.value > swordChance)
            {
                sword.SetActive(false);
            }

            if (Random.value > armScrapsChance)
            {
                armScraps.SetActive(false);
            }

            if (Random.value > lowerScrapsChance)
            {
                lowerScraps.SetActive(false);
            }

            collider = GetComponent<CapsuleCollider>();
            rigidbody = GetComponent<Rigidbody>();
        }

        protected override void Update()
        {
            if (IsDead())
            {
                dissolveTimer -= Time.deltaTime;

                if (dissolveTimer < 0)
                {
                    dissolveAmount += dissolveSpeed * Time.deltaTime;

                    skullPhysics.GetComponent<MeshRenderer>().material.SetFloat("Vector1_F9150D8", dissolveAmount);
                    skullPhysics.transform.GetChild(0).GetComponent<MeshRenderer>().material.SetFloat("Vector1_F9150D8", dissolveAmount);
                    bodyPhysics.GetComponent<MeshRenderer>().material.SetFloat("Vector1_F9150D8", dissolveAmount);
                    leftArmPhysics.GetComponent<MeshRenderer>().material.SetFloat("Vector1_F9150D8", dissolveAmount);
                    leftArmPhysics.transform.GetChild(0).GetComponent<MeshRenderer>().material.SetFloat("Vector1_F9150D8", dissolveAmount);
                    rightArmPhysics.GetComponent<MeshRenderer>().material.SetFloat("Vector1_F9150D8", dissolveAmount);
                    leftLegPhysics.GetComponent<MeshRenderer>().material.SetFloat("Vector1_F9150D8", dissolveAmount);
                    rightLegPhysics.GetComponent<MeshRenderer>().material.SetFloat("Vector1_F9150D8", dissolveAmount);

                    if (dissolveAmount > 0.5f)
                    {
                        Destroy(gameObject);
                    }
                }
            }
            else
            {
                base.Update();
            }
        }

        public override void Die(GameObject attacker = null)
        {
            base.Die(attacker);

            GivePoints(attacker);
            tag = "Untagged";

            PlayDeathSound();

            GetComponent<Animator>().enabled = false;
            collider.enabled = false;
            headCollider.enabled = false;
            rigidbody.isKinematic = true;
            navAgent.enabled = false;

            skull.GetComponent<SkinnedMeshRenderer>().enabled = false;
            skullPhysics.SetActive(true);
            skullPhysics.transform.GetChild(0).gameObject.SetActive(sword.activeInHierarchy);

            body.GetComponent<SkinnedMeshRenderer>().enabled = false;
            bodyPhysics.SetActive(true);

            leftArm.GetComponent<SkinnedMeshRenderer>().enabled = false;
            leftArmPhysics.SetActive(true);
            leftArmPhysics.transform.GetChild(0).gameObject.SetActive(armScraps.activeInHierarchy);

            rightArm.GetComponent<SkinnedMeshRenderer>().enabled = false;
            rightArmPhysics.SetActive(true);

            leftLeg.GetComponent<SkinnedMeshRenderer>().enabled = false;
            leftLegPhysics.SetActive(true);

            rightLeg.GetComponent<SkinnedMeshRenderer>().enabled = false;
            rightLegPhysics.SetActive(true);

            dissolveAmount = skull.GetComponent<SkinnedMeshRenderer>().material.GetFloat("Vector1_F9150D8");

            Destroy(sword);
            Destroy(armScraps);
            Destroy(lowerScraps);

            dissolveTimer = destroyTimer;
        }
    }
}