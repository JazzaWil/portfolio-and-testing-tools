﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace UtilityAI
{
    public class MoveAway : Action
    {
        public List<Agent> nearbyAgents = new List<Agent>();
        public List<Transform> nearbyAgentsList = new List<Transform>();
        public Vector3 offset;
        public Transform bestOutcome;
        public float minDist;

        public override void Enter(Agent agent)
        {

        }

        public override float Evaluate(Agent a)
        {
            nearbyAgents.AddRange(FindObjectsOfType<Agent>());
            for (int i = 0; i < nearbyAgents.Count; i++)
            {
                nearbyAgentsList.Add(nearbyAgents[i].transform);
            }

            Transform bestTarget = null;
            float closestDistanceSqr = Mathf.Infinity;
            Vector3 currentPosition = transform.position;
            foreach (Transform potentialTarget in nearbyAgentsList)
            {
                Vector3 directionToTarget = potentialTarget.position - currentPosition;
                float dSqrToTarget = directionToTarget.sqrMagnitude;
                if (dSqrToTarget < closestDistanceSqr)
                {
                    closestDistanceSqr = dSqrToTarget;
                    bestTarget = bestOutcome = potentialTarget;
                }
            }


             offset = new Vector3(a.target.transform.position.x - bestTarget.transform.position.x, 0f, a.target.transform.position.z - bestTarget.transform.position.z);
            //offset = new Vector3(transform.position.x - bestTarget.transform.position.x, 0f, transform.position.z - bestTarget.transform.position.z);

            NavMeshHit hit;
            if (NavMesh.SamplePosition(offset, out hit, minDist, NavMesh.AllAreas))
            {
                nearbyAgents.Clear();
                nearbyAgentsList.Clear();
                if (Vector3.Distance(offset, transform.position) < minDist )
                {
                    return 4;
                }
                else
                {
                    return -1;
                }
            }
            else
            {
                nearbyAgents.Clear();
                nearbyAgentsList.Clear();
                if (Vector3.Distance(hit.position, transform.position) < minDist)
                {
                    offset = hit.position;
                    return 4;
                }
                else
                {
                    return -1;
                }
            }
        }

        public override void Exit(Agent agent)
        {

        }

        public override void UpdateAction(Agent agent)
        {
            agent.navAgent.Move(Vector3.back * 2);
        }
    }
}

