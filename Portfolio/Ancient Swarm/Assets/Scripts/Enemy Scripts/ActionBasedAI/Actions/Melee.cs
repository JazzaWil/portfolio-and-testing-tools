using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

namespace UtilityAI
{
    [RequireComponent(typeof(Agent))]

    public class Melee : Action
    {
        [Required]
        public enemyAttacks attackAttributes;
        public float localCooldown;
        public float force;
        public float upwardForce;

        public override void Enter(Agent agent)
        {
            agent.navAgent.isStopped = true;
        }

        public override float Evaluate(Agent a)
        {
            if (a.currentDistanceFromTarget > attackAttributes.rangeMin && a.currentDistanceFromTarget < attackAttributes.rangeMax && localCooldown <= 0)
            {
                return 2;
            }
            else
            {
                return -1;
            }
        }

        public override void Exit(Agent agent)
        {
            agent.navAgent.isStopped = false;
        }

        public override void UpdateAction(Agent agent)
        {
            agent.navAgent.isStopped = true;

            transform.LookAt(new Vector3(agent.target.transform.position.x, transform.position.y, agent.target.transform.position.z));

            if (localCooldown <= 0)
            {
                agent.animator.SetTrigger("Attack");
                if(agent.target.GetComponent<Rigidbody>() != null)
                {
                    agent.target.GetComponent<Rigidbody>().AddForceAtPosition((-agent.target.transform.forward * force) + (Vector3.up * upwardForce), agent.target.transform.position, ForceMode.Impulse);
                }
                agent.target.GetComponent<Entity>().Damage(attackAttributes.damage, gameObject);

                AudioRandomiser.PlayAudio(agent.audioSource, attackAttributes.attackSounds);

                localCooldown = attackAttributes.cooldown;
            }


        }

        private void Update()
        {
            localCooldown -= Time.deltaTime;
        }
    }
}