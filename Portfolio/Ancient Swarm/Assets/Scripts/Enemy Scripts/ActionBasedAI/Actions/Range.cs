using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using NaughtyAttributes;

namespace UtilityAI
{
    [RequireComponent(typeof(Agent))]

    public class Range : Action
    {
        [Required]
        public enemyAttacks attackAttributes;
        public float localCooldown;

        [Required]
        public GameObject projectileSpawn;

        public float Force;

        Agent localAgent;

        public override void Enter(Agent agent)
        {
            localAgent = agent;
        }

        public override float Evaluate(Agent a)
        {
            //Movement movement = GetComponent<Movement>();
            //Melee melee = GetComponent<Melee>();
            //Flee flee = GetComponent<Flee>();

            //if (movement != null && attackAttributes.rangeMax >= movement.maxDistance)
            //{
            //    attackAttributes.rangeMax = movement.maxDistance - 0.1f;
            //}

            //if (melee != null && attackAttributes.rangeMin <= melee.attackAttributes.rangeMax)
            //{
            //    attackAttributes.rangeMin = melee.attackAttributes.rangeMax + 0.1f;
            //}

            //if (flee != null && attackAttributes.rangeMin <= flee.minDistanceUntilFlee)
            //{
            //    attackAttributes.rangeMin = flee.minDistanceUntilFlee + 0.1f;
            //}


            if (a.currentDistanceFromTarget > attackAttributes.rangeMin && a.currentDistanceFromTarget < attackAttributes.rangeMax && localCooldown <= 0)
            {
                return 3;
            }
            else
            {
                return -1;
            }
        }

        public override void Exit(Agent agent)
        {
            agent.navAgent.isStopped = false;
            //localAgent = null;

        }

        public override void UpdateAction(Agent agent)
        {
            agent.navAgent.isStopped = true;

            transform.LookAt(new Vector3(agent.target.transform.position.x, transform.position.y, agent.target.transform.position.z));

            if (localCooldown <= 0)
            {
                if(agent.animator.GetBool("RangedAttack") == false)
                {
                    agent.animator.SetTrigger("RangedAttack");
                }
                //fireProjectile(agent);
            }
        }

        private void Update()
        {
            localCooldown -= Time.deltaTime;
        }

        void fireProjectile()
        {
            //Instantiate Bullet
            GameObject tempBullet = Instantiate(attackAttributes.prefab, projectileSpawn.transform.position, attackAttributes.prefab.transform.rotation) as GameObject;
            tempBullet.transform.rotation = Quaternion.Euler(0, 0, 0);
            tempBullet.GetComponent<Projectile>().Init(localAgent.damageFlags, attackAttributes.damage, Force, gameObject);


            Rigidbody tempRigidbody;
            tempRigidbody = tempBullet.GetComponent<Rigidbody>();

            tempRigidbody.AddForce(transform.forward * Force);

            AudioRandomiser.PlayAudio(localAgent.audioSource, attackAttributes.attackSounds);

            Destroy(tempBullet, 3f);

            localCooldown = attackAttributes.cooldown;
        }
    }
}