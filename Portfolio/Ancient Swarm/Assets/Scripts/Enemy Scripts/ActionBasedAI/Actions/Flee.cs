﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UtilityAI
{
    [RequireComponent(typeof(Movement))]

    public class Flee : Action
    {
        public float minDistanceUntilFlee;

        public override float Evaluate(Agent a)
        {
            //if (minDistanceUntilFlee == GetComponent<Movement>().minDistance)
            //{
            //    minDistanceUntilFlee -= .1f;
            //}

            if (a.currentDistanceFromTarget < minDistanceUntilFlee)
            {
                return 0;
            }
            else
            {
                return -1;
            }
        }

        public override void UpdateAction(Agent agent)
        {
            Vector3 temp = transform.position - (agent.target.transform.position - transform.position).normalized * 5;
            temp.y = transform.position.y;

            agent.navAgent.isStopped = false;
            agent.navAgent.SetDestination(temp);
        }

        public override void Enter(Agent agent)
        {

            agent.navAgent.isStopped = false;

        }

        public override void Exit(Agent agent)
        {

        }
    }
}