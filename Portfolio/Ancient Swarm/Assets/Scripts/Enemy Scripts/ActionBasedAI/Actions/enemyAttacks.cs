using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "enemyAttribute", menuName = "enemyAttacks/Attribute", order = 1)]
public class enemyAttacks : ScriptableObject
{
    public AudioClip[] attackSounds;
    Animation animationController;
    public List<ParticleSystem> particleSystems;
    public float damage;
    public float cooldown;
    public List<GameObject> targets;
    public float rangeMin;
    public float rangeMax;
    public float duration;
    public GameObject prefab;
}