﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UtilityAI;

public class sendMinions : Action
{
    public int distanceUntilAttack;
    List<Agent> servents = new List<Agent>();

    public override void Enter(Agent agent)
    {
    }

    public override float Evaluate(Agent a)
    {
        if (a.GetComponent<Pharaoh>() == null)
        {
            return -1;
        }
        else
        {
            for (int i = 0; i < a.GetComponent<Pharaoh>().serventEnemies.Count; i++)
            {
                if (Vector3.Distance(transform.position, a.target.transform.position) <= distanceUntilAttack && a.GetComponent<Pharaoh>().serventEnemies[i].currentAction != a.GetComponent<Pharaoh>().serventEnemies[i].GetComponent<Melee>())
                {
                    servents = a.GetComponent<Pharaoh>().serventEnemies;
                    return 2;
                }
                    return -1;
            }
        }
        return -1;

    }

    public override void Exit(Agent agent)
    {
    }

    public override void UpdateAction(Agent agent)
    {
        for (int i = 0; i < servents.Count; i++)
        {
            if(servents[i].GetComponent<Melee>().Evaluate(servents[i]) == 2)
            {
                servents[i].currentAction = servents[i].GetComponent<Melee>();
            }
            else
            {
                servents[i].currentAction = servents[i].GetComponent<Movement>();
            }                 
        }
    }
}
