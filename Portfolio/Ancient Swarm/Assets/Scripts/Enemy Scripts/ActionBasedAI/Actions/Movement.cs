﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
namespace UtilityAI
{
    [RequireComponent(typeof(Agent))]

    public class Movement : Action
    {
        public float minDistance;
        public float maxDistance;




        public override void Enter(Agent agent)
        {
            agent.navAgent.isStopped = false;
            if (minDistance == maxDistance)
            {
                maxDistance += 2;
            }
        }

        public override float Evaluate(Agent a)
        {

            //if (minDistance <= GetComponent<Range>().attackAttributes.rangeMax)
            //{
            //    minDistance = GetComponent<Range>().attackAttributes.rangeMax + 0.1f;
            //}

            if (minDistance >= maxDistance)
            {
                maxDistance ++;
            }
            if (a.currentDistanceFromTarget > maxDistance)
            {
                return 1;
            }
            else if (a.currentDistanceFromTarget < minDistance)
            {
                return -1;
            }
            else return -1;
        }

        public override void Exit(Agent agent)
        {
        }

        public override void UpdateAction(Agent agent)
        {
            Vector3 forward = agent.navAgent.velocity.normalized;

            if (forward.magnitude != 0)
            {
                transform.forward = forward;
                transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, transform.eulerAngles.z);
            }

            if (minDistance == maxDistance)
            {
                maxDistance++;
            }

            if(agent.myFlockingBehaviour == Agent.FlockingBehaviour.OUTLIER || agent.myFlockingBehaviour == Agent.FlockingBehaviour.LEADER)
            {
                agent.navAgent.SetDestination(agent.target.transform.position);
            }
            else
            {
                agent.navAgent.SetDestination(agent.currentLeader.transform.position);
            }
        }
    }
}