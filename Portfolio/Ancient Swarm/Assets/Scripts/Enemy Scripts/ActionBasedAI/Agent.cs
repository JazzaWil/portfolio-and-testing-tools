using NaughtyAttributes;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.AI;

namespace UtilityAI
{
    [RequireComponent(typeof(Collider))]
    [RequireComponent(typeof(NavMeshAgent))]
    [RequireComponent(typeof(Rigidbody))]
    [RequireComponent(typeof(Animator))]
    [RequireComponent(typeof(AudioSource))]

    public class Agent : Entity
    {
        [Header("AGENT")]
        //Array of actions
        Action[] actions;

        public enum FlockingBehaviour
        {
            OUTLIER,
            FOLLOWER,
            LEADER
        }

        [Range(0, 1)]
        public float[] behaviourPercentages;
        public float followRadious;
        public FlockingBehaviour myFlockingBehaviour;
        public Agent currentLeader;
        public List<Agent> leadersInScene = new List<Agent>();
        //Your current Action
        public Action currentAction;

        [Required]
        public DamageFlags damageFlags;

        //Global Player
        [HideInInspector]
        public List<GameObject> players = new List<GameObject>();

        //Current Target
        public GameObject target;

        //Global Navmesh Agent
        [HideInInspector]
        public NavMeshAgent navAgent;

        //Best outcome from selection of actions
        float bestValue;

        //Distance from target
        //[HideInInspector]
        public float currentDistanceFromTarget;

        //Distance from player
        float distanceFromPlayer;

        //The Enemies Stats
        public Stats myStats;
        public bool canDie;
        public GameObject deathFeedback;

        //Points
        public uint pointsEarned;

        [Header("Sounds")]
        //All Audio Related Content
        public AudioClip[] ambientSounds;
        public Vector2 ambientSoundTimerRange;
        public AudioClip[] hurtSounds;
        public AudioClip[] deathSounds;
        public List<AudioClip> walkingClips = new List<AudioClip>();

        //Animator
        [HideInInspector] public Animator animator;
        [HideInInspector] public AudioSource audioSource;

        private float timer;

        //Health Feedback
        public float currentHealth;
        public TMP_Text healthText;

        //Hit Feedback 
        public GameObject hitFeedback;


        public AudioSource footAudioSource;

        private float ambientSoundTimer;

        private CapsuleCollider myCollider;
        private int defaultDirection;
        private Vector3 defaultCentre;
        private WaveSpawner waveSpawner;

        private List<float> realBehaviourPercentages;

        // Start is called before the first frame update
        public override void Start()
        {
            healthText.gameObject.SetActive(false);
            maxHealth = myStats.health;
            base.Start();

            actions = GetComponents<Action>();
            waveSpawner = FindObjectOfType<WaveSpawner>();

            animator = GetComponent<Animator>();
            audioSource = GetComponent<AudioSource>();

            navAgent = GetComponent<NavMeshAgent>();
            navAgent.speed = myStats.speed;

            PlayerController[] temp = FindObjectsOfType<PlayerController>();
            for (int i = 0; i < temp.Length; i++)
            {
                players.Add(temp[i].gameObject);

            }
            GetComponent<Rigidbody>().isKinematic = true;

            if (GetComponent<Movement>() != null)
            {
                navAgent.stoppingDistance = GetComponent<Movement>().minDistance;
            }

            audioSource = GetComponent<AudioSource>();
            ambientSoundTimer = Random.Range(ambientSoundTimerRange.x, ambientSoundTimerRange.y);

            target = players[0];

            //Determine if you run or walk
            if (navAgent.speed == myStats.defaultSpeed)
            {
                animator.SetFloat("MoveSpeed", 0.5f);
            }
            else if (navAgent.speed == myStats.speedCap)
            {
                animator.SetFloat("MoveSpeed", 1f);
            }

            myCollider = GetComponent<CapsuleCollider>();
            defaultCentre = myCollider.center;
            defaultDirection = myCollider.direction;

            if (navAgent.speed == myStats.speedCap && IsCrawling())
            {
                navAgent.speed /= 2;
            }

            float randomAmount = Random.Range(0, 1);

            if (randomAmount >= behaviourPercentages[0] && randomAmount < behaviourPercentages[1])
            {
                myFlockingBehaviour = FlockingBehaviour.FOLLOWER;
            }
            else if (randomAmount >= behaviourPercentages[1] && randomAmount < behaviourPercentages[2])
            {
                myFlockingBehaviour = FlockingBehaviour.LEADER;
            }
            else if (randomAmount >= behaviourPercentages[2])
            {
                myFlockingBehaviour = FlockingBehaviour.OUTLIER;
            }
        }

        // Update is called once per frame
        protected virtual void Update()
        {
#if UNITY_EDITOR
            currentHealth = GetHealth();
            healthText.text = currentHealth.ToString();
            healthText.gameObject.SetActive(true);
#endif
            
            if (IsAlive())
            {
                if (IsCrawling())
                {
                    myCollider.center = GetLocalCenter();
                    myCollider.direction = 2;
                }
                else
                {
                    myCollider.center = defaultCentre;
                    myCollider.direction = defaultDirection;

                    if (waveSpawner.currentWave >= waveSpawner.switchToSpeedCap)
                    {
                        navAgent.speed = myStats.speedCap;
                    }
                    else
                    {
                        navAgent.speed = myStats.defaultSpeed;
                    }
                }

                if(players[0].GetComponent<PlayerController>().deathmatch)
                {
                    SetCrawling(false);
                }

                currentDistanceFromTarget = Vector3.Distance(target.transform.position, transform.position);

                Vector3 forward = navAgent.velocity.normalized;

                //Have the agent always facing towards their velocity
                if (forward.magnitude != 0)
                {
                    transform.forward = forward;
                    transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, transform.eulerAngles.z);
                }

                FindBestTarget();

                for (int i = leadersInScene.Count - 1; i >= 0; i--)
                {
                    if (leadersInScene[i].IsDead() || leadersInScene[i] == null)
                    {
                        leadersInScene.Remove(leadersInScene[i]);
                    }
                }
                if(leadersInScene != null || leadersInScene.Count > 0)
                {
                    FindClosestLeader();
                }
                else
                {
                    myFlockingBehaviour = FlockingBehaviour.OUTLIER;
                }


                Evaluate();

                if (ambientSoundTimer <= 0)
                {
                    AudioRandomiser.PlayAudio(audioSource, ambientSounds);
                    ambientSoundTimer = Random.Range(ambientSoundTimerRange.x, ambientSoundTimerRange.y);
                }
                else
                {
                    ambientSoundTimer -= Time.deltaTime;
                }
            }



        }

        public override void Damage(float damageAmount, GameObject attacker = null, bool selfDamage = false)
        {
            base.Damage(damageAmount, attacker, selfDamage);

            if (GetHealth() > 0)
            {
                animator.SetTrigger("wasShot");
                AudioRandomiser.PlayAudio(audioSource, hurtSounds);
            }
        }

        public override void Die(GameObject attacker = null)
        {
            if (attacker != null)
            {
                PlayerController playerController = attacker.GetComponent<PlayerController>();

                if (playerController != null)
                {
                    playerController.RegisterKill();
                    GameObject hitParticle = Instantiate(deathFeedback, transform.position, transform.rotation);
                    hitParticle.GetComponentInChildren<ParticleSeek>().SetPlayer(attacker);
                }
            }
        }

        public bool IsCrawling()
        {
            return animator.GetBool("Crawling");
        }

        public void SetCrawling(bool crawl)
        {
            animator.SetBool("Crawling", crawl);
        }

        public void PlayDeathSound()
        {
            AudioRandomiser.PlayAudio(audioSource, deathSounds);
        }

        public void GivePoints(GameObject player)
        {
            if (player != null)
            {
                PlayerController playerController = player.GetComponent<PlayerController>();

                if (playerController != null)
                {
                    playerController.GivePoints(pointsEarned);
                }
            }
        }

        protected Action GetBestAction()
        {
            Action action = null;
            bestValue = 0;

            foreach (Action a in actions)
            {
                float value = a.Evaluate(this);
                a.lastEval = value;

                // commitment tweak
                if (a == currentAction)
                    value += 0.5f;

                if (action == null || value > bestValue)
                {
                    action = a;
                    bestValue = value;
                }
            }

            return action;
        }

        protected void Evaluate()
        {
            timer -= Time.deltaTime;
            if (timer <= 0)
            {

                //Find the best action each frame
                Action best = GetBestAction();

                //If action is different to previous action
                if (best != currentAction)
                {
                    if (currentAction)
                    {
                        currentAction.Exit(this);
                    }
                    currentAction = best;
                    if (currentAction)
                    {
                        currentAction.Enter(this);
                    }
                }

                timer = currentAction.GetDuration();
            }

            //Update the current action
            if (currentAction)
            {
                currentAction.UpdateAction(this);
            }
        }

        protected void playWalkingSounds()
        {
            footAudioSource.clip = walkingClips[UnityEngine.Random.Range(0, walkingClips.Count - 1)];
            footAudioSource.Play();
        }

        public void PersonalFeedback(RaycastHit raycastHit)
        {
            Instantiate(hitFeedback, raycastHit.point, raycastHit.transform.rotation);
        }

        void FindBestTarget()
        {
            Transform bestTarget = null;
            float closestDistanceSqr = Mathf.Infinity;
            Vector3 currentPosition = transform.position;
            foreach (GameObject potentialTarget in players)
            {
                Vector3 directionToTarget = potentialTarget.transform.position - currentPosition;
                float dSqrToTarget = directionToTarget.sqrMagnitude;
                if (dSqrToTarget < closestDistanceSqr)
                {
                    closestDistanceSqr = dSqrToTarget;
                    if(potentialTarget.GetComponent<PlayerController>().IsAlive())
                    {
                        bestTarget = potentialTarget.transform;
                    }
                }
            }
            if (bestTarget != null)
            {
                target = bestTarget.gameObject;
            }
        }
        void FindClosestLeader()
        {
            Transform bestLeader = null;
            float closestDistanceSqr = Mathf.Infinity;
            Vector3 currentPosition = transform.position;
            if(currentLeader == null || currentLeader.IsDead())
            {
                foreach (Agent potentialTarget in leadersInScene)
                {
                    Vector3 directionToTarget = potentialTarget.transform.position - currentPosition;
                    float dSqrToTarget = directionToTarget.sqrMagnitude;
                    if (dSqrToTarget < closestDistanceSqr)
                    {
                        closestDistanceSqr = dSqrToTarget;
                        if (potentialTarget.IsAlive())
                        {
                            bestLeader = potentialTarget.transform;
                        }
                    }
                }
                if (bestLeader != null)
                {
                    target = bestLeader.gameObject;
                }
            }
        }

    }
}