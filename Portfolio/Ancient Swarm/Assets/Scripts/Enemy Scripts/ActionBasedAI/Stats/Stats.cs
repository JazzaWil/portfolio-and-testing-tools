﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "enemyAttribute", menuName = "enemyAttacks/Stats", order = 1)]
public class Stats : ScriptableObject
{
    public float health;
    public float healthCap;
    public float defaultHealth;
    public float speed;
    public float midSpeed;
    public float speedCap;
    public float defaultSpeed;
}
