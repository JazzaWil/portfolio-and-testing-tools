﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace UtilityAI
{
    public class Pharaoh : Agent
    {
        [Header("PHARAOH")]
        public float ragdollDrag;
        public float dissolveSpeed;
        public float destroyTimer;

        [Header("Components")]
        public GameObject body;
        public GameObject eyes;
        public GameObject staff;
        public MeshRenderer staffMesh;
        public GameObject sunSphere;

        private new CapsuleCollider collider;
        private new Rigidbody rigidbody;
        private Rigidbody[] ragdoll;
        private CapsuleCollider[] ragdollCapsules;
        private BoxCollider[] ragdollBoxes;

        private SkinnedMeshRenderer meshRenderer;

        private float dissolveTimer = 0;
        private float dissolveAmount;

        public List<Agent> serventEnemies = new List<Agent>();

        public override void Start()
        {
            base.Start();

            collider = GetComponent<CapsuleCollider>();
            rigidbody = GetComponent<Rigidbody>();
            ragdoll = body.GetComponentsInChildren<Rigidbody>();
            ragdollCapsules = body.GetComponentsInChildren<CapsuleCollider>();
            ragdollBoxes = body.GetComponentsInChildren<BoxCollider>();
            meshRenderer = body.GetComponent<SkinnedMeshRenderer>();

            staff.GetComponent<Rigidbody>().isKinematic = true;
            staff.GetComponent<CapsuleCollider>().enabled = false;
            staff.GetComponent<SphereCollider>().enabled = false;

            foreach (Rigidbody rb in ragdoll)
            {
                rb.isKinematic = true;
                rb.drag = ragdollDrag;
            }

            foreach (CapsuleCollider capsule in ragdollCapsules)
            {
                capsule.enabled = false;
            }

            foreach (BoxCollider box in ragdollBoxes)
            {
                box.enabled = false;
            }

            rigidbody.isKinematic = false;
            collider.enabled = true;
            myFlockingBehaviour = FlockingBehaviour.LEADER;
        }

        // Update is called once per frame
        protected override void Update()
        {
            base.Update();

            if(IsAlive() && currentAction != GetComponent<sendMinions>())
            {
                
                for (int i = serventEnemies.Count - 1; i >= 0; i--)
                {
                    if(serventEnemies[i].IsDead() || serventEnemies[i] == null)
                    {
                        serventEnemies.Remove(serventEnemies[i]);
                    }
                }
                for (int i = 0; i < serventEnemies.Count; i++)
                {
                    serventEnemies[i].navAgent.SetDestination(transform.position);
                }
            }

            if (IsDead())
            {
                dissolveTimer -= Time.deltaTime;

                if (dissolveTimer < 0)
                {
                    dissolveAmount += dissolveSpeed * Time.deltaTime;

                    meshRenderer.material.SetFloat("Vector1_F9150D8", dissolveAmount);
                    staffMesh.material.SetFloat("Vector1_F9150D8", dissolveAmount);

                    if (dissolveAmount > 0.5f)
                    {
                        Destroy(gameObject);
                    }
                }
            }
        }

        public override void Die(GameObject attacker = null)
        {
            base.Die(attacker);

            GivePoints(attacker);
            tag = "Untagged";

            PlayDeathSound();

            GetComponent<Animator>().enabled = false;
            collider.enabled = false;
            navAgent.enabled = false;

            dissolveTimer = destroyTimer;
            dissolveAmount = meshRenderer.material.GetFloat("Vector1_F9150D8");

            foreach (Rigidbody rb in ragdoll)
            {
                rb.isKinematic = false;
            }

            foreach (CapsuleCollider capsule in ragdollCapsules)
            {
                capsule.enabled = true;
            }

            foreach (BoxCollider box in ragdollBoxes)
            {
                box.enabled = true;
            }

            rigidbody.isKinematic = true;
            collider.enabled = false;

            staff.transform.parent = transform;
            staff.GetComponent<Rigidbody>().isKinematic = false;
            staff.GetComponent<CapsuleCollider>().enabled = true;
            staff.GetComponent<SphereCollider>().enabled = true;
            staff.GetComponentInChildren<Light>().enabled = false;

            Destroy(sunSphere);
            Destroy(eyes);
        }
        private void OnTriggerEnter(Collider other)
        {
            if (other.GetComponent<Agent>() != null && other.GetComponent<Pharaoh>() == null)
            {
                serventEnemies.Add(other.GetComponent<Agent>());
            }
        }
    }
}