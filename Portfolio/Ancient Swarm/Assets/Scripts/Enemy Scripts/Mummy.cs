using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
namespace UtilityAI
{
    public class Mummy : Agent
    {
        [Header("MUMMY")]
        public float dissolveSpeed;
        public float destroyTimer;

        [Header("Components")]
        public GameObject body;
        public GameObject eyes;
        public GameObject[] bandages;

        private new CapsuleCollider collider;
        private new Rigidbody rigidbody;
        private Rigidbody[] ragdoll;
        private CapsuleCollider[] ragdollCapsules;
        private BoxCollider[] ragdollBoxes;

        private SkinnedMeshRenderer meshRenderer;

        private float dissolveTimer = 0;
        private float dissolveAmount;

        public override void Start()
        {
            base.Start();

            uint bandageAmount = (uint)Math.Round(UnityEngine.Random.value * bandages.Length, MidpointRounding.ToEven);

            for (int i = 0; i < bandageAmount; i++)
            {
                bandages[i].SetActive(false);
            }

            collider = GetComponent<CapsuleCollider>();
            rigidbody = GetComponent<Rigidbody>();
            meshRenderer = body.GetComponent<SkinnedMeshRenderer>();

            ragdoll = GetComponentsInChildren<Rigidbody>();
            ragdollCapsules = GetComponentsInChildren<CapsuleCollider>();
            ragdollBoxes = GetComponentsInChildren<BoxCollider>();

            foreach (Rigidbody rb in ragdoll)
            {
                rb.isKinematic = true;
            }

            foreach (CapsuleCollider capsule in ragdollCapsules)
            {
                capsule.enabled = false;
            }

            foreach (BoxCollider box in ragdollBoxes)
            {
                box.enabled = false;
            }

            collider.enabled = true;
        }

        // Update is called once per frame
        protected override void Update()
        {
            base.Update();

            if(IsDead())
            {
                dissolveTimer -= Time.deltaTime;

                if (dissolveTimer < 0)
                {
                    dissolveAmount += dissolveSpeed * Time.deltaTime;

                    meshRenderer.material.SetFloat("Vector1_F9150D8", dissolveAmount);

                    foreach (GameObject bandage in bandages)
                    {
                        bandage.GetComponent<SkinnedMeshRenderer>().material.SetFloat("Vector1_F9150D8", dissolveAmount);
                    }

                    if (dissolveAmount > 0.5f)
                    {
                        Destroy(gameObject);
                    }
                }
            }
        }

        public override void Die(GameObject attacker = null)
        {
            base.Die(attacker);

            GivePoints(attacker);
            tag = "Untagged";

            PlayDeathSound();

            GetComponent<Animator>().enabled = false;
            rigidbody.isKinematic = true;
            navAgent.enabled = false;

            dissolveTimer = destroyTimer;

            dissolveAmount = meshRenderer.material.GetFloat("Vector1_F9150D8");

            foreach (Rigidbody rb in ragdoll)
            {
                rb.isKinematic = false;
            }

            foreach (CapsuleCollider capsule in ragdollCapsules)
            {
                capsule.enabled = true;
            }

            foreach (BoxCollider box in ragdollBoxes)
            {
                box.enabled = true;
            }

            collider.enabled = false;

            Destroy(eyes);
        }
    }
}