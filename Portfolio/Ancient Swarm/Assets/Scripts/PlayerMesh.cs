﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMesh : MonoBehaviour
{
    public uint rocketIndex;
    public GameObject rocketGrenade;
    public float spikeSpinSpeed;
    public GameObject shieldSpikes;

    public GameObject[] weapons;
    public GameObject[] dualWeapons;

    private WeaponController weaponController;

    // Start is called before the first frame update
    void Start()
    {
        weaponController = GetComponentInParent<PlayerController>().weaponController;
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < weapons.Length; i++)
        {
            if (weapons[i] != null)
            {
                weapons[i].SetActive(false);

                if (dualWeapons[i] != null)
                {
                    dualWeapons[i].SetActive(false);
                }
            }
        }

        uint index = weaponController.GetCurrentWeaponIndex();
        weapons[index].SetActive(true);

        if (weaponController.DualWielding())
        {
            dualWeapons[index].SetActive(true);
        }

        if (index == rocketIndex)
        {
            rocketGrenade.SetActive(weaponController.CurrentClip() != 0 || weaponController.reloading);
        }
        else if (index == weaponController.shieldIndex)
        {
            shieldSpikes.transform.eulerAngles += Vector3.forward * spikeSpinSpeed * Time.deltaTime;
            weapons[weaponController.shieldIndex].SetActive(weaponController.CurrentAmmo() != 0);
        }
    }
}