﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Idol : MonoBehaviour
{
    public string text;
    public float lowerSpeed;

    public GameObject pedestal;
    public GameObject waveSpawner;

    private Rooms[] rooms;
    private Vector3 pedestalLowerPos;

    private MeshRenderer meshRenderer;
    private new CapsuleCollider collider;

    public List<GameObject> torches = new List<GameObject>();

    public List<GameObject> doors = new List<GameObject>();
    public Material doorOn;

    private void Awake()
    {
        rooms = FindObjectsOfType<Rooms>();
        pedestalLowerPos = pedestal.transform.position - Vector3.up * pedestal.transform.localScale.y;

        meshRenderer = GetComponent<MeshRenderer>();
        collider = GetComponent<CapsuleCollider>();

        foreach(Rooms room in rooms)
        {
            room.gameObject.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!meshRenderer.enabled)
        {
            pedestal.transform.position -= Vector3.up * lowerSpeed * Time.deltaTime;

            if (pedestal.transform.position.y < pedestalLowerPos.y)
            {
                pedestal.transform.position = pedestalLowerPos;
                Destroy(gameObject);
            }
        }
    }

    public void Interact()
    {
        pedestal.GetComponent<AudioSource>().Play();

        meshRenderer.enabled = false;
        collider.enabled = false;

        for (int i = 0; i < transform.childCount; i++)
        {
            Destroy(transform.GetChild(i).gameObject);
        }
    }

    public void StartWave()
    {
        foreach (Rooms room in rooms)
        {
            room.gameObject.SetActive(true);
        }

        for (int i = 0; i < doors.Count; i++)
        {
            doors[i].GetComponent<MeshRenderer>().material = (doorOn);
        }

        for (int i = 0; i < torches.Count; i++)
        {
            torches[i].SetActive(true);

            ParticleSystem particleSystem = torches[i].GetComponentInChildren<ParticleSystem>();


            if (particleSystem != null)
            {
                particleSystem.Play();
            }
        }
    }
}