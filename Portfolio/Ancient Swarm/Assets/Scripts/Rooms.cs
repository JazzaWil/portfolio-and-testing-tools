﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rooms : MonoBehaviour
{
    public GameObject waveSpawner;
    public List<Transform> spawnPoints = new List<Transform>();
    public bool isOpened;

    [Space]

    public List<Rooms> adjacentRooms = new List<Rooms>();

    private void Start()
    {
        waveSpawner = FindObjectOfType<WaveSpawner>().gameObject;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            isOpened = true;
            waveSpawner.GetComponent<WaveSpawner>().currentSpawnPoints = spawnPoints;
            for (int i = 0; i < adjacentRooms.Count; i++)
            {
                if (adjacentRooms[i].isOpened)
                {
                    for (int j = 0; j < adjacentRooms[i].spawnPoints.Count; j++)
                    {
                        if (!spawnPoints.Contains(adjacentRooms[i].spawnPoints[j]))
                        {
                            spawnPoints.Add(adjacentRooms[i].spawnPoints[j]);
                        }
                    }
                }
            }
 
        }
    }
}
