﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crawling : MonoBehaviour
{
    private bool inRoom;
    public int chance;

    private void OnTriggerStay(Collider other)
    {
        if(other.GetComponent<UtilityAI.Agent>() != null && inRoom == false)
        {
            other.GetComponentInParent<Animator>().SetBool("Crawling", true);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        int a = Random.Range(1, chance + 1);

        if (other.GetComponentInParent<UtilityAI.Agent>() != null)
        {
            if(a != 1)
            {
                other.GetComponentInParent<Animator>().SetBool("Crawling", false);
            }

            inRoom = true;
        }
    }
}