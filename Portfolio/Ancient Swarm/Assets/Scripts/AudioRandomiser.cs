﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class AudioRandomiser
{
    public static bool PlayAudio(AudioSource audioSource, AudioClip[] audioClips)
    {
        if (audioClips.Length != 0)
        {
            audioSource.clip = audioClips[(int)Math.Round(UnityEngine.Random.value * (audioClips.Length - 1), MidpointRounding.ToEven)];
            audioSource.Play();

            return true;
        }

        return false;
    }
}