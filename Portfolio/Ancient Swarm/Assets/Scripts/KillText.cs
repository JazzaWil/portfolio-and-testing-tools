﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KillText : MonoBehaviour
{
    //public uint maxTextCount;
    public float textSpace;
    public float textLerp;
    public float displayTimer;
    public float fadeSpeed;
    public KillTextTemplate killTextTemplate;

    public Sprite[] weaponIcons;

    private List<GameObject> textList;
    private List<float> timers;

    private Vector3 defaultPos;

    // Start is called before the first frame update
    void Start()
    {
        killTextTemplate.gameObject.SetActive(false);

        textList = new List<GameObject>();
        timers = new List<float>();

        defaultPos = killTextTemplate.GetComponent<RectTransform>().localPosition;
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = timers.Count - 1; i >= 0; i--)
        {
            timers[i] -= Time.deltaTime;

            if (timers[i] <= 0)
            {
                KillTextTemplate killText = textList[i].GetComponent<KillTextTemplate>();

                foreach (Text text in killText.killer)
                {
                    text.color = new Color(text.color.r, text.color.g, text.color.b, text.color.a - fadeSpeed * Time.deltaTime);
                }

                foreach (Text text in killText.victim)
                {
                    text.color = new Color(text.color.r, text.color.g, text.color.b, killText.killer[0].color.a);
                }

                foreach (Image image in killText.weapon)
                {
                    image.color = new Color(image.color.r, image.color.g, image.color.b, killText.killer[0].color.a);
                }

                if (killText.killer[0].color.a <= 0)
                {
                    Destroy(textList[i]);
                    textList.RemoveAt(i);
                    timers.RemoveAt(i);
                }
            }
        }

        for (int i = 0; i < textList.Count; i++)
        {
            textList[i].GetComponent<RectTransform>().localPosition = Vector3.Lerp(textList[i].GetComponent<RectTransform>().localPosition, defaultPos + Vector3.down * i * textSpace, textLerp * Time.deltaTime);
        }
    }

    public void AddText(string killerName, string victimName, uint weaponIndex)
    {
        GameObject newText = Instantiate(killTextTemplate.gameObject, transform);
        KillTextTemplate killText = newText.GetComponent<KillTextTemplate>();

        newText.SetActive(true);
        newText.GetComponent<RectTransform>().localPosition = defaultPos + Vector3.down * textList.Count * textSpace;

        foreach (Text text in killText.killer)
        {
            text.text = killerName;
        }

        foreach (Text text in killText.victim)
        {
            text.text = victimName;
        }

        foreach (Image image in killText.weapon)
        {
            image.sprite = weaponIcons[weaponIndex];
        }

        textList.Add(newText);
        timers.Add(displayTimer);

        /*if (textList.Count > maxTextCount)
        {
            for (int i = 0; i < textList.Count - maxTextCount; i++)
            {
                Destroy(textList[i]);
                textList.RemoveAt(i);
                timers.RemoveAt(i);
            }
        }*/
    }
}
