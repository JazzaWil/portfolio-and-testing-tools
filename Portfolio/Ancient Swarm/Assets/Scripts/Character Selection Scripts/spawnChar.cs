﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawnChar : MonoBehaviour
{

    public setup Manager;
    public int pos;
	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        if(pos < 0)
        {
            pos = Manager.positions.Count - 1;
        }

        if(pos >  Manager.positions.Count - 1)
        {
            pos = 0;
        }

        Manager.Characters[pos].transform.position = transform.position;
    }
}
