﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class setup : MonoBehaviour
{
    public List<GameObject> Characters = new List<GameObject>();
    public List<spawnChar> positions = new List<spawnChar>();
    public Text Name;

    public GameObject currentSelectedCharcater;
    public GameObject spawnPoint;

    public List<GameObject> thingsToTurnOff = new List<GameObject>();
    public List<GameObject> thingsToTurnOn = new List<GameObject>();

    public bool skipSelection;


    // Use this for initialization
    void Start()
    {
        currentSelectedCharcater = Characters[positions[1].pos];

        if (skipSelection == true)
        {
            Select();
        }
    }

    // Update is called once per frame
    void Update()
    {
        Name.text = Characters[positions[1].pos].name;
        currentSelectedCharcater = Characters[positions[1].pos];
    }

    public void Next()
    {
        for(int i = 0; i < positions.Count; i++)
        {
            positions[i].pos ++;
        }
    }

    public void Prev()
    {
        for (int i = 0; i < positions.Count; i++)
        {
            positions[i].pos -= 1;
        }
    }

    public void Select()
    {
        currentSelectedCharcater.transform.position = spawnPoint.transform.position;

        for (int i = 0; i < thingsToTurnOff.Count; i++)
        {
            thingsToTurnOff[i].SetActive(false);
        }

        for (int i = 0; i < thingsToTurnOn.Count; i++)
        {
            thingsToTurnOn[i].SetActive(true);
        }
    }
}
