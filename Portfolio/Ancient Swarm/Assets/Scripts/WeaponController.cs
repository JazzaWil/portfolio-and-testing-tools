﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Rewired;
using VolumetricLines;

[RequireComponent(typeof(AudioSource))]
public class WeaponController : MonoBehaviour
{
    public DamageFlags damageFlags;
    public DamageFlags deathmatchDamageFlags;
    [Range(0, 45)] public float maxProjectileAngle = 10;
    public float lineLength = 20;
    public float projectileAdjustMinDistance;
    public ParticleSystem hitFeedback;
    public GameObject bulletDecal;

    [Tooltip("List of weapon gameobjects\n\nMelee weapon needs to be element 0")]
    public GameObject idol;
    public GameObject[] weapons;
    public GameObject[] dualWeapons;

    [Header("UI")]
    public Text[] ammoText;
    public float nameFadeSpeed;
    public Text[] nameText;
    public GameObject keyboardIconContainer;
    public WeaponUI[] keyboardWeaponIcons;
    public GameObject controllerIconContainer;
    public WeaponUI[] controllerWeaponIcons;

    [Header("Input")]
    public string fireButton = "Fire";
    public string reloadButton = "Reload";

    public string equipMeleeButton = "Melee";
    public string[] equipWeaponButton = { "Weapon1", "Weapon2", "Weapon3", "Weapon4" };

    [Header("Starting Weapons")]
    public uint equippedWeapon = 1;
    public bool hasShield = false;
    public uint shieldIndex;
    public uint[] inventory = { 1, 0, 0, 0 };
    public bool[] dualWield = { false, false, false, false };
    public uint[] upgrade = { 0, 0, 0, 0 };

    [HideInInspector] public int weaponSwapIndex = -1; //-1 for no weapon
    private int weaponReplaceIndex = -1; //-1 for no weapon
    private bool weaponReplaceDual = false;
    private int lastWeaponIndex = -1; //-1 for no weapon
    private int holdWeaponIndex = -1; //-1 for no weapon

    [HideInInspector] public PlayerController playerController;
    private AudioSource[] audioSources;
    private uint freeAudioSourceIndex;

    [HideInInspector] public Weapon[] weaponAttributes;
    private uint[] clips;
    private uint[] ammo;

    private bool idolGrab = false;
    private bool secondShot = false;
    private uint singleReloadAmount = 0;
    private bool secondReload = false;
    private bool endSingleReload = false;

    [HideInInspector] public bool canFire = false;
    [HideInInspector] public bool canFire2 = false;
    [HideInInspector] public bool reloading = false;
    [HideInInspector] public bool firing = false;
    [HideInInspector] public bool equipping = false;

    private float defaultFOV;

    // Start is called before the first frame update
    void Start()
    {
        if (playerController == null)
            return;

        audioSources = GetComponents<AudioSource>();

        ammo = new uint[weapons.Length];
        clips = new uint[weapons.Length];
        weaponAttributes = new Weapon[weapons.Length];

        defaultFOV = playerController.cameras[0].fieldOfView;

        if (playerController.deathmatch)
        {
            dualWield[0] = true;
        }

        for (uint i = 0; i < weapons.Length; i++)
        {
            weaponAttributes[i] = weapons[i].GetComponent<WeaponBehaviour>().attributes;

            weapons[i].SetActive(false);

            if (dualWeapons[i] != null)
            {
                dualWeapons[i].SetActive(false);
            }
        }

        idol.SetActive(false);

        CurrentWeapon().SetActive(true);

        if (dualWield[equippedWeapon - 1] && CurrentDualWeapon() != null)
        {
            CurrentDualWeapon().SetActive(true);
        }

        for (int i = 0; i < inventory.Length; i++)
        {
            ammo[inventory[i]] += weaponAttributes[inventory[i]].startingAmmo;

            if (weaponAttributes[inventory[i]].ammoCapacity.Length != 0 && ammo[inventory[i]] > weaponAttributes[inventory[i]].ammoCapacity[0])
            {
                ammo[inventory[i]] = weaponAttributes[inventory[i]].ammoCapacity[0];
            }

            clips[inventory[i]] = ClipSize(inventory[i]);
        }

        GiveAmmo(shieldIndex, weaponAttributes[shieldIndex].ammoCapacity[0]);

        foreach (Text text in nameText)
        {
            text.text = CurrentAttributes().name;
            text.color = new Color32((byte)(text.color.r * 255), (byte)(text.color.g * 255), (byte)(text.color.b * 255), 255);
        }

        UpdateWeaponIcons();

        if (DualWielding())
        {
            playerController.animator.SetTrigger("Dual " + CurrentAttributes().name);
        }
        else
        {
            playerController.animator.SetTrigger(CurrentAttributes().name);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!playerController.isGamePaused)
        {
            //cheats
            if (Input.GetKey(KeyCode.LeftControl))
            {
                //full ammo cheat
                if (Input.GetKey(KeyCode.C))
                {
                    CurrentClip() = CurrentClipSize();

                    for (int i = 0; i < ammo.Length; i++)
                    {
                        if (weaponAttributes[i].ammoCapacity.Length != 0)
                        {
                            ammo[i] = AmmoCapacity((uint)i);
                        }
                    }
                }
            }

            if (DualWielding() && secondReload)
            {
                Reload();
            }

            bool firePress = playerController.player.GetButton(fireButton);

            if (idol.activeInHierarchy)
            {
                firePress = false;
            }

            if (playerController.player.GetButtonDown(equipMeleeButton))
            {
                if (equippedWeapon != 0)
                {
                    EquipWeapon(0, true);
                }
                else
                {
                    EquipWeapon((uint)lastWeaponIndex);
                }
            }
            else
            {
                for (int i = 0; i < equipWeaponButton.Length; i++)
                {
                    if (playerController.player.GetButtonDown(equipWeaponButton[i]))
                    {
                        EquipWeapon((uint)i + 1);
                    }
                }
            }

            if (Idle())
            {
                if (holdWeaponIndex != -1)
                {
                    GiveWeapon((uint)holdWeaponIndex, weaponReplaceDual);
                    holdWeaponIndex = -1;
                }
                else if (weaponSwapIndex != -1)
                {
                    CurrentWeapon().SetActive(false);

                    if (DualWielding())
                    {
                        CurrentDualWeapon().SetActive(false);
                    }

                    if (idolGrab)
                    {
                        idol.SetActive(true);
                        idolGrab = false;
                        weaponSwapIndex = -1;
                    }
                    else
                    {
                        if (weaponReplaceIndex != shieldIndex)
                        {
                            if (weaponSwapIndex == 0 && weaponReplaceIndex != -1)
                            {
                                equippedWeapon = (uint)inventory.Length;
                            }
                            else
                            {
                                equippedWeapon = (uint)weaponSwapIndex;
                            }

                            if (weaponReplaceIndex != -1)
                            {
                                inventory[equippedWeapon - 1] = (uint)weaponReplaceIndex;
                                dualWield[equippedWeapon - 1] = weaponReplaceDual;
                                CurrentClip() = CurrentClipSize();
                                GiveAmmo(inventory[equippedWeapon - 1], weaponAttributes[inventory[equippedWeapon - 1]].startingAmmo);

                                GiveAmmo((uint)weaponReplaceIndex, weaponAttributes[weaponReplaceIndex].startingAmmo);
                            }
                        }
                        else
                        {
                            weapons[0].SetActive(false);
                        }

                        weaponReplaceIndex = -1;

                        foreach (Text text in nameText)
                        {
                            text.text = CurrentAttributes().name;
                            text.color = new Color32((byte)(text.color.r * 255), (byte)(text.color.g * 255), (byte)(text.color.b * 255), 255);
                        }

                        CurrentWeapon().SetActive(true);

                        if (DualWielding())
                        {
                            CurrentDualWeapon().SetActive(true);
                            playerController.animator.SetTrigger("Dual " + CurrentAttributes().name);
                        }
                        else
                        {
                            playerController.animator.SetTrigger(CurrentAttributes().name);
                        }

                        UpdateWeaponIcons();

                        weaponSwapIndex = -1;

                        canFire = false;
                        canFire2 = false;
                        secondShot = false;
                        equipping = true;
                    }
                }
            }

            if (CurrentAttributes().constantFire)
            {
                if (firePress)
                {
                    if (firing)
                    {
                        SpawnProjectiles();
                    }
                    else
                    {
                        Fire();
                    }
                }
                else
                {
                    if (firing)
                    {
                        CurrentWeapon().GetComponent<Animator>().SetTrigger("FireEnd");
                        playerController.animator.SetTrigger("FireEnd");
                        reloading = true;
                        firing = false;
                    }
                }
            }
            else if (firePress)
            {
                Fire();
            }

            if (playerController.player.GetButton(reloadButton) && !secondReload)
            {
                Reload();
            }

            if (playerController.deathmatch && CurrentClipSize() != 0 && CurrentClip() == 0)
            {
                //Switch weapons when out of ammo
                /*if (CurrentAmmo() == 0)
                {
                    Fire();
                }
                else*/
                {
                    Reload();
                }
            }

            /*if (reloading && firePress && !DualWielding())
            {
                endSingleReload = true;
            }*/

            if (playerController.cameras[0].fieldOfView > defaultFOV)
            {
                foreach (Camera cam in playerController.cameras)
                {
                    cam.fieldOfView -= CurrentAttributes().fireFOVSpeed * Time.deltaTime;

                    if (cam.fieldOfView < defaultFOV)
                    {
                        cam.fieldOfView = defaultFOV;
                    }
                }
            }

            foreach (Text text in ammoText)
            {
                if (CurrentAttributes().infiniteAmmo || playerController.god)
                {
                    text.text = "";
                }
                else if (CurrentClipSize() == 0)
                {
                    if (CurrentAmmoCapacity() == 1)
                    {
                        text.text = "";
                    }
                    else
                    {
                        text.text = CurrentAmmo().ToString();
                    }
                }
                else if (CurrentClipSize() == 1)
                {
                    text.text = (CurrentAmmo() + CurrentClip()).ToString();
                }
                else
                {
                    text.text = CurrentClip().ToString() + "/" + CurrentAmmo().ToString();
                }
            }

            foreach (Text text in nameText)
            {
                text.color -= new Color32(0, 0, 0, (byte)(nameFadeSpeed * Time.deltaTime * 255));
            }
        }
    }

    public bool GiveAmmo(uint amount)
    {
        if (equippedWeapon == 0 || CurrentAmmo() == CurrentAmmoCapacity())
        {
            return false;
        }

        CurrentAmmo() += amount;

        if (CurrentAmmo() > CurrentAmmoCapacity())
        {
            CurrentAmmo() = CurrentAmmoCapacity();
        }

        return true;
    }

    public bool GiveAmmo(uint index, uint amount)
    {
        uint ammoCapacity = AmmoCapacity(index);

        if (ammoCapacity == 0 || ammo[index] == ammoCapacity)
        {
            return false;
        }

        ammo[index] += amount;

        if (ammo[index] > ammoCapacity)
        {
            ammo[index] = ammoCapacity;
        }

        return true;
    }

    public void GiveAmmo(float percentage)
    {
        for (int i = 0; i < inventory.Length; i++)
        {
            Weapon attributes = weaponAttributes[inventory[i]];

            if (!attributes.infiniteAmmo)
            {
                GiveAmmo(inventory[i], (uint)Math.Round(percentage * AmmoCapacity(inventory[i]), MidpointRounding.AwayFromZero));
            }
        }
    }

    public void GiveAmmo(uint index, float percentage)
    {
        if (!weaponAttributes[index].infiniteAmmo)
        {
            int inventoryIndex = GetInventoryIndex(index);

            if (inventoryIndex != -1)
            {
                GiveAmmo(inventory[inventoryIndex], (uint)Math.Round(percentage * AmmoCapacity(inventory[inventoryIndex]), MidpointRounding.AwayFromZero));
            }
        }
    }

    public bool GiveWeapon(uint index, bool giveDual = false)
    {
        if (CanPickupWeapon(index, !playerController.deathmatch))
        {
            if (Idle())
            {
                if (index == shieldIndex)
                {
                    hasShield = true;
                    EquipWeapon(0, true);

                    return true;
                }
                else if (InventoryFull() && !(weaponAttributes[index].dualWieldable && HasWeapon(index)))
                {
                    weaponReplaceIndex = (int)index;
                    weaponReplaceDual = giveDual;
                    EquipWeapon(equippedWeapon, true);

                    return true;
                }
                else
                {
                    for (int i = 0; i < inventory.Length; i++)
                    {
                        if (inventory[i] == 0)
                        {
                            inventory[i] = index;
                            dualWield[i] = giveDual;
                            clips[inventory[i]] = ClipSize(inventory[i]);
                            GiveAmmo(index, weaponAttributes[index].startingAmmo);
                            EquipWeapon((uint)i + 1);

                            return true;
                        }
                        else if (inventory[i] == index && weaponAttributes[inventory[i]].dualWieldable && !dualWield[i])
                        {
                            dualWield[i] = true;
                            clips[inventory[i]] += ClipSize(inventory[i]) / 2;
                            GiveAmmo(inventory[i], weaponAttributes[inventory[i]].startingAmmo);

                            if (i == equippedWeapon - 1)
                            {
                                EquipWeapon((uint)i + 1, true);
                            }
                            else
                            {
                                EquipWeapon((uint)i + 1);
                            }

                            return true;
                        }
                    }
                }
            }
            else if (holdWeaponIndex == -1)
            {
                holdWeaponIndex = (int)index;
                weaponReplaceDual = giveDual;

                return true;
            }
        }

        return false;
    }

    public bool HasWeapon(uint index)
    {
        for (int i = 0; i < inventory.Length; i++)
        {
            if (inventory[i] == index)
            {
                return true;
            }
        }

        return false;
    }

    public bool HasDualWeapon(uint index)
    {
        for (int i = 0; i < inventory.Length; i++)
        {
            if (inventory[i] == index && dualWield[i])
            {
                return true;
            }
        }

        return false;
    }

    public bool CanPickupWeapon(uint index, bool checkIdle = true)
    {
        if (Idle() || !checkIdle)
        {
            if (shieldIndex == index)
            {
                return !hasShield;
            }

            for (int i = 0; i < inventory.Length; i++)
            {
                if (inventory[i] == index && (!weaponAttributes[inventory[i]].dualWieldable ||
                    weaponAttributes[inventory[i]].dualWieldable && dualWield[i]))
                {
                    return false;
                }
            }

            return true;
        }

        return false;
    }

    public bool Upgrade()
    {
        if (equippedWeapon == 0)
        {
            return false;
        }

        uint upgradeLength = GetUpgradeMax();

        if (upgrade[equippedWeapon - 1] == upgradeLength - 1)
        {
            return false;
        }

        uint oldClipSize = CurrentClipSize();
        upgrade[equippedWeapon - 1]++;

        CurrentClip() += CurrentClipSize() - oldClipSize;

        return true;
    }

    public bool Upgrade(uint index)
    {
        int inventoryIndex = GetInventoryIndex(index);

        if (inventoryIndex == -1)
        {
            return false;
        }

        uint upgradeLength = GetUpgradeMax(inventory[inventoryIndex]);

        if (upgrade[inventoryIndex] == upgradeLength - 1)
        {
            return false;
        }

        uint oldClipSize = ClipSize(index);
        upgrade[inventoryIndex]++;

        clips[index] += ClipSize(index) - oldClipSize;

        return true;
    }

    public bool SprintIdle()
    {
        return !firing && !reloading && !equipping && !secondReload && weaponSwapIndex == -1;
    }

    public bool Idle()
    {
        return !firing && !reloading && !equipping && !playerController.sprinting && !playerController.disableMovement;
    }

    public void Fire()
    {
        if ((canFire || canFire2) && !reloading && !equipping && !playerController.sprinting && !playerController.disableMovement)
        {
            if (CurrentClip() == 0 && !(CurrentClipSize() == 0 && CurrentAmmo() != 0) && !CurrentAttributes().infiniteAmmo)
            {
                if (CurrentAmmo() == 0)
                {
                    uint equipNext = equippedWeapon;

                    for (int i = 0; i < inventory.Length; i++)
                    {
                        equipNext--;

                        if (equipNext == 0)
                        {
                            equipNext = (uint)inventory.Length;
                        }

                        if (inventory[(int)equipNext - 1] != 0)
                        {
                            if (ammo[inventory[(int)equipNext - 1]] != 0)
                            {
                                i = inventory.Length;
                            }
                            else if (i == inventory.Length - 1)
                            {
                                equipNext = 0;
                                i = inventory.Length;
                            }
                        }
                    }

                    EquipWeapon(equipNext);
                }
                else
                {
                    Reload();
                }

            }
            else
            {
                if (!(CurrentAttributes().infiniteAmmo || playerController.god) && CurrentClipSize() == 0 &&
                    CurrentAmmo() == 0)
                {
                    return;
                }

                secondShot = !secondShot;

                if (!CurrentAttributes().constantFire)
                {
                    firing = true;
                }

                if (DualWielding() && !secondShot)
                {
                    CurrentDualWeapon().GetComponent<Animator>().SetTrigger("Fire");
                    playerController.animator.SetTrigger("FireAlt");
                }
                else
                {
                    CurrentWeapon().GetComponent<Animator>().SetTrigger("Fire");
                    playerController.animator.SetTrigger("Fire");
                }

                canFire = false;
                canFire2 = false;
            }
        }
    }

    public void Reload()
    {
        if (Idle() && CurrentClip() < CurrentClipSize() && CurrentAmmo() != 0)
        {
            uint difference = CurrentClipSize() - CurrentClip();

            if (secondReload)
            {
                CurrentDualWeapon().GetComponent<Animator>().SetTrigger("Reload");
            }
            else
            {
                CurrentWeapon().GetComponent<Animator>().SetTrigger("Reload");
                playerController.animator.SetTrigger("Reload");
            }

            canFire = false;
            canFire2 = false;
            reloading = true;
            secondShot = false;

            if (DualWielding() && !secondReload)
            {
                if (difference < CurrentAmmo())
                {
                    singleReloadAmount = (uint)Math.Round(difference / 2.0f, MidpointRounding.AwayFromZero);
                }
                else
                {
                    singleReloadAmount = (uint)Math.Round(CurrentAmmo() / 2.0f, MidpointRounding.AwayFromZero);
                }
            }
            else
            {
                if (difference < CurrentAmmo())
                {
                    singleReloadAmount = difference;
                }
                else
                {
                    singleReloadAmount = CurrentAmmo();
                }
            }
        }
    }

    public void SpawnProjectiles()
    {
        if (!(CurrentAttributes().infiniteAmmo || playerController.god))
        {
            if (CurrentClipSize() == 0 && CurrentAmmo() == 0)
            {
                return;
            }

            if (CurrentClipSize() == 0)
            {
                CurrentAmmo()--;
            }
            else
            {
                CurrentClip()--;
            }
        }

        if (playerController.controllerSettings.vibration)
        {
            playerController.player.SetVibration(CurrentAttributes().motorIndex, CurrentAttributes().vibrationIntensity, CurrentAttributes().vibrationDuration);
        }

        if (CurrentAttributes().constantFire)
        {
            foreach (Camera cam in playerController.cameras)
            {
                cam.fieldOfView = UnityEngine.Random.Range(defaultFOV, defaultFOV + CurrentAttributes().fireFOVChange * (1 + playerController.GetMoveFactor()));
            }
        }
        else
        {
            foreach (Camera cam in playerController.cameras)
            {
                cam.fieldOfView = defaultFOV + CurrentAttributes().fireFOVChange * (1 + playerController.GetMoveFactor());
            }
        }

        playerController.head.gameObject.SetActive(false);

        Transform barrelEnd = CurrentWeapon().GetComponent<WeaponBehaviour>().barrelEnd;

        if (DualWielding() && !secondShot)
        {
            barrelEnd = CurrentDualWeapon().GetComponent<WeaponBehaviour>().barrelEnd;
        }

        float damage = CurrentDamage();

        if (CurrentAttributes().constantFire)
        {
            damage *= Time.deltaTime;
        }

        List<GameObject> hitObjects = new List<GameObject>();
        List<Vector3> hitPoints = new List<Vector3>();
        List<Vector3> rotations = new List<Vector3>();

        for (int i = 0; i < CurrentAttributes().projectileFireCount; i++)
        {
            RaycastHit raycastHit;

            Vector3 rotationOffset = new Vector3((UnityEngine.Random.value * 2 - 1) * ((1.0f - CurrentAccuracy()) * maxProjectileAngle), (UnityEngine.Random.value * 2 - 1) * ((1.0f - CurrentAccuracy()) * maxProjectileAngle), 0);

            Vector3 oldForward = transform.forward;
            Vector3 finalRotation = transform.eulerAngles + rotationOffset;

            rotations.Add(finalRotation);

            transform.eulerAngles = finalRotation;

            if (Physics.Raycast(transform.position + playerController.transform.forward * playerController.collider.radius, transform.forward, out raycastHit, Mathf.Infinity, ~Physics.IgnoreRaycastLayer, QueryTriggerInteraction.Ignore))
            {
                hitObjects.Add(raycastHit.transform.gameObject);
                hitPoints.Add(raycastHit.point);

                if (CurrentAttributes().projectile.GetComponent<Rigidbody>() == null &&
                    CurrentAttributes().projectile.GetComponent<BoxCollider>() == null)
                {
                    if (raycastHit.transform.tag != "Shield")
                    {
                        Entity en = raycastHit.transform.GetComponentInParent<Entity>();

                        if (en != null && GetDamageFlags().flags[(int)en.type])
                        {
                            UtilityAI.Agent agent = raycastHit.transform.GetComponentInParent<UtilityAI.Agent>();

                            if (agent != null)
                            {
                                agent.PersonalFeedback(raycastHit);
                            }

                            SetProjectile setProjectile = CurrentAttributes().projectile.GetComponent<SetProjectile>();

                            if (setProjectile == null)
                            {
                                if (raycastHit.collider.tag == "Head")
                                {
                                    en.Damage(CurrentDamage() * GetDamageFlags().headShotMultiplier, playerController.gameObject);
                                }
                                else
                                {
                                    en.Damage(CurrentDamage(), playerController.gameObject);
                                }

                                if (en.IsDead())
                                {
                                    foreach (Rigidbody rb in en.GetComponentsInChildren<Rigidbody>())
                                    {
                                        rb.AddForce(transform.forward * CurrentAttributes().projectileForce);
                                    }
                                }
                            }
                        }
                        else
                        {
                            Rigidbody rb = raycastHit.transform.GetComponent<Rigidbody>();

                            if (rb != null)
                            {
                                rb.AddForceAtPosition(transform.forward * CurrentAttributes().projectileForce, transform.position);
                            }

                            Instantiate(hitFeedback, raycastHit.point, raycastHit.transform.rotation);
                            //Instantiate(bulletDecal, raycastHit.point, raycastHit.transform.rotation);
                        }
                    }
                }
            }
            else
            {
                hitObjects.Add(null);
                hitPoints.Add(transform.position + transform.forward * lineLength);
            }

            transform.forward = oldForward;
        }

        if (CurrentAttributes().muzzleFlash != null)
        {
            GameObject muzzleFlash;

            if (DualWielding() && !secondShot)
            {
                muzzleFlash = Instantiate(CurrentAttributes().muzzleFlash, CurrentDualWeapon().GetComponent<WeaponBehaviour>().muzzleFlashParent);
            }
            else
            {
                muzzleFlash = Instantiate(CurrentAttributes().muzzleFlash, CurrentWeapon().GetComponent<WeaponBehaviour>().muzzleFlashParent);
            }

            muzzleFlash.layer = gameObject.layer;

            muzzleFlash.transform.localPosition = CurrentAttributes().muzzleFlash.transform.position;
            muzzleFlash.transform.localScale = transform.localScale;
            Destroy(muzzleFlash, CurrentAttributes().muzzleDestroyTimer);
        }

        for (int i = 0; i < hitPoints.Count; i++)
        {
            GameObject bulletObject = Instantiate(CurrentAttributes().projectile, barrelEnd.position, barrelEnd.rotation);

            if (CurrentAttributes().projectileAdjust && Vector3.Distance(hitPoints[i], transform.position) >= projectileAdjustMinDistance)
            {
                bulletObject.transform.LookAt(hitPoints[i]);
            }
            else
            {
                bulletObject.transform.eulerAngles = rotations[i];
            }

            bulletObject.transform.eulerAngles += CurrentAttributes().projectile.transform.eulerAngles;

            VolumetricLineBehavior line = bulletObject.GetComponent<VolumetricLineBehavior>();

            if (line != null)
            {
                line.m_endPos = hitPoints[i] + Vector3.forward * (1 / bulletObject.transform.localScale.z);
            }

            Projectile projectile = bulletObject.GetComponent<Projectile>();

            if (projectile != null)
            {
                projectile.Init(GetDamageFlags(), CurrentDamage(), CurrentAttributes().projectileForce, playerController.gameObject);

                Shield shield = bulletObject.GetComponent<Shield>();

                if (shield != null)
                {
                    shield.SetOwner(playerController.gameObject);
                }
                else
                {
                    SetProjectile setProjectile = projectile.GetComponent<SetProjectile>();

                    if (setProjectile != null)
                    {
                        Transform startPos = CurrentWeapon().GetComponent<WeaponBehaviour>().barrelEnd.transform;

                        if (hitObjects[i] != null)
                        {
                            GameObject setHit = hitObjects[i];

                            if (setHit.GetComponentInParent<Entity>() != null)
                            {
                                setProjectile.ChainHit(startPos, setHit);
                            }
                            else
                            {
                                setProjectile.HitFail(startPos, hitPoints[i], true);
                            }
                        }
                        else
                        {
                            setProjectile.HitFail(startPos, hitPoints[i], false);
                        }
                    }
                    else
                    {
                        Laser laser = projectile.GetComponent<Laser>();
                        Transform startPos = CurrentWeapon().GetComponent<WeaponBehaviour>().barrelEnd;

                        if (laser != null)
                        {
                            laser.SetPoints(startPos, hitPoints[i]);
                        }
                    }
                }
            }
            else
            {
                Explosive explosive = bulletObject.GetComponent<Explosive>();

                if (explosive != null)
                {
                    explosive.Init(GetDamageFlags(), CurrentDamage(), playerController.gameObject);
                }
            }

            Rigidbody bulletRB = bulletObject.GetComponent<Rigidbody>();

            if (bulletRB != null)
            {
                bulletRB.AddForce(bulletObject.transform.forward * CurrentAttributes().projectileForce);
            }

            if (!CurrentAttributes().constantFire && CurrentAttributes().projectileDestroyTimer != 0.0f)
            {
                Destroy(bulletObject, CurrentAttributes().projectileDestroyTimer);
            }
        }

        playerController.head.gameObject.SetActive(true);
        hitPoints.Clear();
    }

    public void EndReload()
    {
        if (CurrentAttributes().singleReload)
        {
            CurrentClip()++;
            CurrentAmmo()--;
            singleReloadAmount--;

            if (endSingleReload || singleReloadAmount == 0)
            {
                if (secondReload)
                {
                    CurrentDualWeapon().GetComponent<Animator>().SetTrigger("EndReload");
                    secondReload = false;
                    reloading = true;
                }
                else
                {
                    CurrentWeapon().GetComponent<Animator>().SetTrigger("EndReload");

                    if (DualWielding())
                    {
                        if (CurrentClip() < CurrentClipSize())
                        {
                            secondReload = true;
                        }

                        reloading = true;
                    }
                }

                playerController.animator.SetTrigger("EndReload");
            }
        }
        else
        {
            if (DualWielding() && !secondReload)
            {
                uint difference = CurrentClipSize() - CurrentClip();

                if (difference > CurrentAmmo())
                {
                    difference = (uint)Math.Round(CurrentAmmo() / 2.0f, MidpointRounding.AwayFromZero);
                    CurrentClip() += difference;
                    CurrentAmmo() -= difference;
                }
                else
                {
                    difference = (uint)Math.Round(difference / 2.0f, MidpointRounding.AwayFromZero);
                    CurrentAmmo() -= difference;
                    CurrentClip() += difference;
                }

                if (CurrentClipSize() != CurrentClip())
                {
                    secondReload = true;
                }

                reloading = true;
            }
            else
            {
                if (CurrentClipSize() - CurrentClip() > CurrentAmmo())
                {
                    CurrentClip() += CurrentAmmo();
                    CurrentAmmo() = 0;
                }
                else
                {
                    CurrentAmmo() -= CurrentClipSize() - CurrentClip();
                    CurrentClip() = CurrentClipSize();
                }

                secondReload = false;
            }
        }

        endSingleReload = false;
    }

    public bool EquipWeapon(uint inventoryIndex, bool force = false)
    {
        if ((Idle() || (playerController.deathmatch && force)) && weaponSwapIndex == -1)
        {
            if (inventoryIndex == 0 && (equippedWeapon != 0 || force))
            {
                weaponSwapIndex = 0;
                lastWeaponIndex = (int)equippedWeapon;

                if (hasShield && weapons[0].activeInHierarchy)
                {
                    weapons[0].GetComponent<Animator>().SetTrigger("Unequip");
                    weaponReplaceIndex = (int)shieldIndex;
                }
                else
                {
                    CurrentWeapon().GetComponent<Animator>().SetTrigger("Unequip");

                    if (DualWielding())
                    {
                        CurrentDualWeapon().GetComponent<Animator>().SetTrigger("Unequip");
                    }
                }

                equipping = true;
                canFire = false;
                canFire2 = false;

                return true;
            }
            else if (inventory[inventoryIndex - 1] != 0 && (inventoryIndex != equippedWeapon || force))
            {
                //stop player from switching to a weapon with no ammo
                /*if (!force && clips[inventory[inventoryIndex - 1]] == 0 && ammo[inventory[inventoryIndex - 1]] == 0 && weaponReplaceIndex == -1)
                {
                    UpdateWeaponIcons();
                    return false;
                }*/

                weaponSwapIndex = (int)inventoryIndex;

                CurrentWeapon().GetComponent<Animator>().SetTrigger("Unequip");

                if (DualWielding())
                {
                    CurrentDualWeapon().GetComponent<Animator>().SetTrigger("Unequip");
                }

                if (!(CurrentAttributes().throwable && CurrentAmmo() == 0))
                {
                    equipping = true;
                }

                canFire = false;
                canFire2 = false;

                return true;
            }
        }

        UpdateWeaponIcons();
        return false;
    }

    public void UpdateWeaponIcons()
    {
        if (playerController.controller)
        {
            keyboardIconContainer.SetActive(false);
            controllerIconContainer.SetActive(true);

            for (int i = 0; i < controllerWeaponIcons.Length; i++)
            {
                controllerWeaponIcons[i].UpdateIcon(inventory[i], clips[inventory[i]] == 0 && ammo[inventory[i]] == 0 && !weaponAttributes[inventory[i]].infiniteAmmo && weaponSwapIndex != inventory[i]);
            }
        }
        else
        {
            keyboardIconContainer.SetActive(true);
            controllerIconContainer.SetActive(false);

            for (int i = 0; i < keyboardWeaponIcons.Length; i++)
            {
                keyboardWeaponIcons[i].UpdateIcon(inventory[i], clips[inventory[i]] == 0 && ammo[inventory[i]] == 0 && !weaponAttributes[inventory[i]].infiniteAmmo && weaponSwapIndex != inventory[i]);
            }
        }
    }

    public GameObject CurrentWeapon()
    {
        if (equippedWeapon == 0)
        {
            if (hasShield)
            {
                return weapons[shieldIndex];
            }
            else
            {
                return weapons[0];
            }
        }

        return weapons[inventory[equippedWeapon - 1]];
    }

    private GameObject CurrentDualWeapon()
    {
        if (equippedWeapon == 0)
        {
            return null;
        }

        return dualWeapons[inventory[equippedWeapon - 1]];
    }

    public Weapon CurrentAttributes()
    {
        if (equippedWeapon == 0)
        {
            if (hasShield)
            {
                return weaponAttributes[shieldIndex];
            }

            return weaponAttributes[0];
        }

        return weaponAttributes[inventory[equippedWeapon - 1]];
    }

    public float CurrentDamage()
    {
        if (playerController.deathmatch)
        {
            return CurrentAttributes().deathmatchDamage;
        }

        if (equippedWeapon == 0)
        {
            return CurrentAttributes().damage[0];
        }

        uint upgradeIndex = GetUpgrade(inventory[equippedWeapon - 1]);

        if (upgradeIndex > CurrentAttributes().damage.Length - 1)
        {
            upgradeIndex = (uint)CurrentAttributes().damage.Length - 1;
        }

        return CurrentAttributes().damage[upgrade[equippedWeapon - 1]];
    }

    public float GetDamage(uint index)
    {
        if (playerController.deathmatch)
        {
            return weaponAttributes[index].deathmatchDamage;
        }

        if (equippedWeapon == 0)
        {
            return weaponAttributes[index].damage[0];
        }

        uint upgradeIndex = GetUpgrade(index);

        if (upgradeIndex > weaponAttributes[index].damage.Length - 1)
        {
            upgradeIndex = (uint)weaponAttributes[index].damage.Length - 1;
        }

        return weaponAttributes[index].damage[upgrade[index]];
    }

    public ref uint CurrentClip()
    {
        if (equippedWeapon == 0)
        {
            if (hasShield)
            {
                return ref clips[shieldIndex];
            }
            else
            {
                return ref clips[0];
            }
        }

        return ref clips[inventory[equippedWeapon - 1]];
    }

    public float CurrentAccuracy()
    {
        if (playerController.deathmatch)
        {
            return CurrentAttributes().deathmatchAccuracy;
        }

        return CurrentAttributes().accuracy;
    }

    public uint CurrentClipSize()
    {
        if (playerController.deathmatch)
        {
            if (DualWielding())
            {
                return CurrentAttributes().deathmatchClipSize * 2;
            }

            return CurrentAttributes().deathmatchClipSize;
        }

        if (CurrentAttributes().clipSize.Length == 0)
        {
            return 0;
        }

        uint upgradeIndex = upgrade[equippedWeapon - 1];
        uint clipSize = 0;

        if (upgradeIndex > CurrentAttributes().clipSize.Length - 1)
        {
            upgradeIndex = (uint)CurrentAttributes().clipSize.Length - 1;
        }

        clipSize = CurrentAttributes().clipSize[upgradeIndex];

        if (DualWielding())
        {
            return clipSize * 2;
        }

        return clipSize;
    }

    public uint ClipSize(uint index)
    {
        int inventoryIndex = GetInventoryIndex(index);

        if (inventoryIndex == -1)
        {
            return 0;
        }

        if (playerController.deathmatch)
        {
            if (dualWield[inventoryIndex])
            {
                return weaponAttributes[index].deathmatchClipSize * 2;
            }

            return weaponAttributes[index].deathmatchClipSize;
        }

        if (weaponAttributes[index].clipSize.Length == 0)
        {
            return 0;
        }

        uint upgradeIndex = upgrade[inventoryIndex];
        uint clipSize = 0;

        if (upgradeIndex > weaponAttributes[index].clipSize.Length - 1)
        {
            upgradeIndex = (uint)weaponAttributes[index].clipSize.Length - 1;
        }

        clipSize = weaponAttributes[index].clipSize[upgradeIndex];

        if (dualWield[inventoryIndex])
        {
            return clipSize * 2;
        }

        return clipSize;
    }

    public ref uint CurrentAmmo()
    {
        if (equippedWeapon == 0)
        {
            if (hasShield)
            {
                return ref ammo[shieldIndex];
            }

            return ref ammo[0];
        }

        return ref ammo[inventory[equippedWeapon - 1]];
    }

    public uint CurrentAmmoCapacity()
    {
        if (CurrentAttributes().constantFire)
            return 0;

        if (equippedWeapon == 0)
        {
            if (hasShield)
            {
                if (playerController.deathmatch)
                {
                    return weaponAttributes[shieldIndex].deathmatchAmmoCapacity;
                }

                return weaponAttributes[shieldIndex].ammoCapacity[0];
            }

            return 0;
        }

        if (playerController.deathmatch)
        {
            uint temp = CurrentAttributes().deathmatchAmmoCapacity;

            if (DualWielding())
            {
                temp *= 2;
            }

            return temp;
        }

        uint upgradeIndex = GetUpgrade(inventory[equippedWeapon - 1]);

        if (upgradeIndex > CurrentAttributes().ammoCapacity.Length - 1)
            upgradeIndex = (uint)CurrentAttributes().ammoCapacity.Length - 1;

        uint capacity = CurrentAttributes().ammoCapacity[upgradeIndex];

        if (DualWielding())
        {
            capacity *= 2;
        }

        return capacity;
    }

    public uint AmmoCapacity(uint index)
    {
        if (weaponAttributes[index].ammoCapacity.Length == 0)
        {
            return 0;
        }

        uint upgradeIndex = GetUpgrade(index);

        if (upgradeIndex > weaponAttributes[index].ammoCapacity.Length)
        {
            upgradeIndex = (uint)weaponAttributes[index].ammoCapacity.Length;
        }

        int inventoryIndex = GetInventoryIndex(index);
        uint capacity = weaponAttributes[index].ammoCapacity[upgradeIndex];

        if (inventoryIndex == -1)
        {
            return capacity;
        }

        if (dualWield[inventoryIndex])
        {
            capacity *= 2;
        }

        return capacity;
    }

    public int GetInventoryIndex(uint weaponIndex)
    {
        int inventoryIndex = -1;

        for (int i = 0; i < inventory.Length; i++)
        {
            if (inventory[i] == weaponIndex)
            {
                inventoryIndex = i;
                i = inventory.Length;
            }
        }

        return inventoryIndex;
    }

    public uint GetUpgrade()
    {
        if (equippedWeapon == 0)
        {
            return 0;
        }

        return upgrade[equippedWeapon - 1];
    }

    public uint GetUpgrade(uint index)
    {
        int upgradeIndex = -1;

        for (int i = 0; i < inventory.Length; i++)
        {
            if (inventory[i] == index)
            {
                upgradeIndex = i;
                i = inventory.Length;
            }
        }

        if (upgradeIndex == -1)
        {
            return 0;
        }

        return upgrade[upgradeIndex];
    }

    public uint GetUpgradeMax()
    {
        return (uint)CurrentAttributes().upgradeCost.Length + 1;
    }

    public uint GetUpgradeMax(uint index)
    {
        return (uint)weaponAttributes[index].upgradeCost.Length + 1;
    }

    public bool DualWielding()
    {
        if (equippedWeapon == 0)
        {
            return false;
        }

        return dualWield[equippedWeapon - 1];
    }

    public bool InventoryEmpty()
    {
        for (int i = 0; i < inventory.Length; i++)
        {
            if (inventory[i] != 0)
            {
                return false;
            }
        }

        return true;
    }

    public bool InventoryFull()
    {
        for (int i = 0; i < inventory.Length; i++)
        {
            if (inventory[i] == 0)
            {
                return false;
            }
        }

        return true;
    }

    public float GetDamageResistance()
    {
        if (CurrentAttributes().reusable && CurrentAmmo() == 0)
        {
            return 0;
        }

        return CurrentAttributes().damageResistance;
    }

    public void Respawn()
    {
        equippedWeapon = 1;
        hasShield = false;

        inventory[0] = 1;

        for (int i = 1; i < inventory.Length; i++)
        {
            inventory[i] = 0;
            dualWield[i] = false;
            upgrade[i] = 0;
        }

        weaponSwapIndex = -1;
        weaponReplaceIndex = -1;
        lastWeaponIndex = -1;

        secondShot = false;
        singleReloadAmount = 0;
        secondReload = false;
        endSingleReload = false;

        canFire = true;
        canFire2 = false;
        reloading = false;
        firing = false;
        equipping = false;

        Start();
    }

    public void GrabIdol()
    {
        if (equippedWeapon == 0)
        {
            EquipWeapon(0, true);
        }
        else
        {
            EquipWeapon(equippedWeapon - 1, true);
        }

        idolGrab = true;
    }

    public void IdolUnequip()
    {
        idol.SetActive(false);
        weaponSwapIndex = (int)equippedWeapon;
    }

    public uint GetCurrentWeaponIndex()
    {
        if (equippedWeapon == 0)
        {
            if (hasShield)
            {
                return shieldIndex;
            }

            return 0;
        }

        return inventory[equippedWeapon - 1];
    }

    public DamageFlags GetDamageFlags()
    {
        if (playerController.deathmatch)
        {
            return deathmatchDamageFlags;
        }

        return damageFlags;
    }

    public AudioSource GetAudioSource()
    {
        freeAudioSourceIndex++;

        if (freeAudioSourceIndex == audioSources.Length)
        {
            freeAudioSourceIndex = 0;
        }

        return audioSources[freeAudioSourceIndex];
    }

    public AudioSource GetAudioSource(uint index)
    {
        return audioSources[index];
    }

    public void SetNextAudioSource(uint index)
    {
        if (index == 0)
        {
            freeAudioSourceIndex = (uint)audioSources.Length - 1;
        }
        else
        {
            freeAudioSourceIndex = index - 1;
        }
    }
}