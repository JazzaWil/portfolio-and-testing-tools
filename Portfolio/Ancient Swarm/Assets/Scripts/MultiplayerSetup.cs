﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Rewired;

public class MultiplayerSetup : MonoBehaviour
{
    public uint playerID;
    public Text[] name;

    [HideInInspector] public Player player;

    private char[] nameValues;
    private List<uint> nameIndices;

    // Start is called before the first frame update
    void Start()
    {
        player = ReInput.players.GetPlayer((int)playerID - 1);

        nameIndices = new List<uint>();

        for (int i = 0; i < name.Length; i++)
        {
            nameIndices.Add(0);
        }

        nameValues = new char[36];

        for (int i = 0; i < 26; i++)
        {
            nameValues[i] = (char)(i + 65);
        }

        for (int i = 26; i < 36; i++)
        {
            nameValues[i] = (char)(i + 48);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void IncreaseName(uint index)
    {
        nameIndices[(int)index]++;

        if (nameIndices[(int)index] == 36)
        {
            nameIndices[(int)index] = 0;
        }

        name[(int)index].text = nameValues[nameIndices[(int)index]].ToString();
    }

    public void DecreaseName(uint index)
    {
        if (nameIndices[(int)index] == 0)
        {
            nameIndices[(int)index] = 36;
        }

        nameIndices[(int)index]--;
        name[(int)index].text = nameValues[nameIndices[(int)index]].ToString();
    }
}
