﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportScript : MonoBehaviour
{
    public GameObject player;
    public Transform teleportTo;
    // Update is called once per frame
    void Update()
    {
        if(player.transform.position.y <= -10)
        {
            player.transform.position = teleportTo.position;
        }
    }
}
