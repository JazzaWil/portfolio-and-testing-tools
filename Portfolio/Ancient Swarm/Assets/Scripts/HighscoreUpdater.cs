﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class HighscoreUpdater : MonoBehaviour
{
    public Transform entryContainer;
    public Transform entryTemplate;
    public HighScores scores;
    public int scoreToDisplay;

    private void Awake()
    {
        entryTemplate.gameObject.SetActive(false);

        float templateHeight = 20;
        for (int i = 0; i < scoreToDisplay; i++)
        {
            if (i < scores.highScores.Count)
            {
                Transform entryTransform = Instantiate(entryTemplate, entryContainer);
                RectTransform entryRectTransform = entryTransform.GetComponent<RectTransform>();

                entryRectTransform.anchoredPosition = new Vector2(0, -templateHeight * i);
                entryTransform.gameObject.SetActive(true);


                entryTransform.Find("PosText").GetComponent<TMP_Text>().text = "";
                entryTransform.Find("ScoreText").GetComponent<TMP_Text>().text = scores.highScores[i].score.ToString();
                entryTransform.Find("NameText").GetComponent<TMP_Text>().text = scores.highScores[i].name;
            }

        }

    }
}
