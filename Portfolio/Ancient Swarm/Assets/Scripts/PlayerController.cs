﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Rewired;
using TMPro;
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(CapsuleCollider))]
[RequireComponent(typeof(BoxCollider))]
[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(AudioSource))]
public class PlayerController : Entity
{
    [Header("Rewired")]
    public uint playerID;
    public optionsVariables controllerSettings;

    [Header("Appearance")]
    public string playerName;
    public GameObject playerMesh;
    public uint playerMaterialIndex;
    public Material[] playerMaterials;

    [Header("UI")]
    public Text[] pointText;
    public Sprite charImage;
    public RawImage uiImage;
    public Canvas pauseMenu;
    public bool isGamePaused;
    public Canvas playerCanvas;
    public uint highscoreCapacity = 5;
    public GameObject nameObject;
    public List<Text> letters = new List<Text>();

    [Header("Interact")]
    public Text[] interactText;
    public float interactFadeSpeed;
    [Tooltip("How far away you are before you see shop text appear")]
    public float interactDistance;
    public float interactHoldTimer;
    public Image[] interactCircle;
    public LayerMask interactLayer;
    public float textLerp;

    [Header("Health")]
    [Range(0, 1)] public float healthRegenRate;
    public float healthRegenTimer;
    [Range(0, 1)] public float deathmatchHealthRegenRate;
    public float deathmatchHealthRegenTimer;
    public Image healthBar;
    public Text[] healthText;
    public float invincibilityFramesTimer;
    public RawImage damageIndicator;
    public RawImage hitIndicator;

    [Header("Input")]
    public string verticalMovementAxis = "Vertical";
    public string horizontalMovementAxis = "Horizontal";
    public string verticalLookAxis = "Mouse Y";
    public string horizontalLookAxis = "Mouse X";
    public string jumpButton = "Jump";
    public string sprintButton = "Sprint";
    public string sprintToggleButton = "SprintToggle";
    public string interactButton = "Interact";
    public string pauseButton = "Pause";

    [Header("Movement")]
    public LayerMask floorLayer;
    public float walkSpeed;
    public float sprintSpeed;
    public float deathmatchSpeed;
    public float deathmatchAnimSpeed;
    [Range(0, 1)] public float airMoveFactor;
    [Range(0, 1)] public float minSprintFactor;
    [Range(0, 1)] public float sprintHorizontalMax;
    [Range(-90, 90)] public float sprintWeaponRotate;
    public float groundedOffset;
    public float jumpSpeed;
    public float deathmatchJumpSpeed;
    public Vector2 aimAssistFactor;
    public float maxYLookAngle = 90;
    public float deadCameraLerp;

    [Header("Movement Effects")]
    public Transform spineJoint;
    public Vector2 bobSize;
    public float bobSpeed;
    public Vector2 deathmatchBobSize;
    public float deathmatchBobSpeed;
    public float headBobRadius;
    public float headBobSpeed;
    public float deathmatchHeadBobRadius;
    public float deathmatchHeadBobSpeed;
    public float headBobLerp;
    public float bobLerp;
    public Vector2 rotateSize;
    public float rotateLerp;

    [Header("Sounds")]
    public AudioClip[] hurtSounds;

    [HideInInspector] public Player player;
    private new Rigidbody rigidbody;
    [HideInInspector] public new CapsuleCollider collider;
    [HideInInspector] public WeaponController weaponController;
    [HideInInspector] public Animator animator;
    private GameController gameController;
    private AudioSource audioSource;

    private GameObject killer = null;

    private Rigidbody[] ragdollRigidbodies;
    private CapsuleCollider[] ragdollCapsules;
    private BoxCollider[] ragdollBoxes;
    [HideInInspector] public SphereCollider head;

    [HideInInspector] public bool god = false;
    [HideInInspector] public bool deathmatch = false;
    [HideInInspector] public bool controller = false;

    [HideInInspector] public bool sprinting = false;
    private bool sprintToggle = false;
    private bool grounded = true;
    private RigidbodyConstraints rigidbodyConstraints;

    [HideInInspector] public bool disableMovement = false;
    private bool weaponBought = false;

    private float healthUI = 0;
    private float healthRegenTime;
    private float invincibilityFrames = 0;
    private float interactHold = 0;

    private float pointsUI = 0;

    private float pitch = 0.0f;
    private float yaw = 0.0f;

    private float moveFactor = 0;
    private float bobAngle = 0;

    [Header("Misc")]
    public GameObject weaponTransform;
    public Camera[] cameras;

    public HighScores WaveHighScore;
    public HighScores SwarmHighScore;
    public WaveSpawner spawner;

    public uint startingPoints;

    [HideInInspector] public GameObject waveSpawner;
    [HideInInspector] public Idol idol;

    private int points = 0;
    private uint totalPoints = 0;

    private Vector3 cameraDefaultPos;

    private void Awake()
    {
        controllerSettings = Instantiate(controllerSettings);
        controllerSettings.controllerConnected = false;

        ReInput.ControllerConnectedEvent += onControllerConnect;
        ReInput.ControllerDisconnectedEvent += onControllerDisconnect;
        ReInput.ControllerPreDisconnectEvent += onControllerDisconnect;
    }

    void onControllerConnect(ControllerStatusChangedEventArgs args)
    {
        Debug.Log("Controller connected: " + args.name + " , " + args.controllerId + " , " + args.controllerType);

        controllerSettings.controllerConnected = true;
    }

    void onControllerDisconnect(ControllerStatusChangedEventArgs args)
    {
        Debug.Log("Controller disconnected: " + args.name + " , " + args.controllerId + " , " + args.controllerType);
        controllerSettings.controllerConnected = false;
    }

    // Start is called before the first frame update
    public override void Start()
    {
        ReInput.ControllerConnectedEvent += onControllerConnect;
        ReInput.ControllerDisconnectedEvent += onControllerDisconnect;

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

        player = ReInput.players.GetPlayer((int)playerID - 1);
        rigidbody = GetComponent<Rigidbody>();
        rigidbodyConstraints = rigidbody.constraints;
        collider = GetComponent<CapsuleCollider>();
        weaponController = weaponTransform.GetComponent<WeaponController>();
        weaponController.playerController = this;
        animator = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();
        gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();

        deathmatch = gameController.gamemode == GameController.Gamemode.DEATHMATCH;

        if (deathmatch)
            animator.SetFloat("AnimSpeed", deathmatchAnimSpeed);

        ragdollRigidbodies = playerMesh.GetComponentsInChildren<Rigidbody>();
        ragdollCapsules = playerMesh.GetComponentsInChildren<CapsuleCollider>();
        ragdollBoxes = playerMesh.GetComponentsInChildren<BoxCollider>();
        head = playerMesh.GetComponentInChildren<SphereCollider>();

        healthUI = GetHealth();

        if (deathmatch)
        {
            healthRegenTime = deathmatchHealthRegenTimer;
        }
        else
        {
            healthRegenTime = healthRegenTimer;
            points = (int)startingPoints;
        }

        cameraDefaultPos = cameras[0].transform.localPosition;
        uiImage.texture = charImage.texture;

        Init();
    }

    private void Init()
    {
        base.Start();

        foreach (Rigidbody rigidbody in ragdollRigidbodies)
        {
            rigidbody.isKinematic = true;
        }

        foreach (CapsuleCollider collider in ragdollCapsules)
        {
            collider.enabled = false;
        }

        foreach (BoxCollider collider in ragdollBoxes)
        {
            collider.enabled = false;
        }

        pitch = transform.eulerAngles.x;
        yaw = transform.eulerAngles.y;

        foreach (Text text in interactText)
        {
            text.color = new Color32((byte)(text.color.r * 255), (byte)(text.color.g * 255), (byte)(text.color.b * 255), 0);
        }
    }

    private void FixedUpdate()
    {
        if (IsAlive())
        {
            float moveMagnitude = new Vector3(rigidbody.velocity.x, 0, rigidbody.velocity.z).magnitude;

            Vector3 forwardVector = cameras[0].transform.forward;
            transform.forward = new Vector3(forwardVector.x, 0, forwardVector.z);
            cameras[0].transform.forward = forwardVector;

            float verticalMove = player.GetAxis(verticalMovementAxis);
            float horizontalMove = player.GetAxis(horizontalMovementAxis);

            Vector3 rightVector = transform.right;
            rightVector.y = 0;

            moveFactor = Mathf.Clamp01(new Vector2(horizontalMove, verticalMove).magnitude);

            if (disableMovement)
            {
                moveFactor = 0;
            }

            RaycastHit raycastHit;

            bool jumping = rigidbody.velocity.y > 0 && !grounded;

            grounded = Physics.Raycast(transform.position, Vector3.down, out raycastHit, collider.height / 2 + groundedOffset, floorLayer);

            if (jumping)
            {
                grounded = false;
            }

            if (grounded)
            {
                transform.position = new Vector3(transform.position.x, raycastHit.point.y + collider.height / 2, transform.position.z);
                rigidbody.velocity = Vector3.zero;

                if (moveFactor == 0.0f)
                {
                    rigidbody.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ | rigidbodyConstraints;
                }
                else
                {
                    rigidbody.constraints = rigidbodyConstraints;
                }
            }
            else
            {
                rigidbody.constraints = rigidbodyConstraints;
            }

            sprinting = false;

            if (player.GetButton(sprintToggleButton))
            {
                sprintToggle = true;
            }

            Vector3 moveDirection = (verticalMove * transform.forward + horizontalMove * rightVector).normalized;
            Vector3 moveVelocity = Vector3.zero;

            if ((player.GetButton(sprintButton) || sprintToggle) && !deathmatch && (moveMagnitude / walkSpeed) > minSprintFactor && verticalMove > 0
                    && Mathf.Abs(horizontalMove) <= sprintHorizontalMax && weaponController.SprintIdle())
            {
                sprinting = true;
            }
            else
            {
                sprintToggle = false;
            }

            animator.SetFloat("VelX", horizontalMove);

            if (deathmatch)
            {
                animator.SetFloat("VelY", verticalMove);
            }
            else
            {
                if (sprinting)
                    animator.SetFloat("VelY", verticalMove / 2 + 0.5f);
                else
                    animator.SetFloat("VelY", verticalMove / 2);
            }

            float moveSpeed;

            if (sprinting)
            {
                moveSpeed = sprintSpeed * moveFactor;
            }
            else
            {
                if (deathmatch)
                {
                    moveSpeed = deathmatchSpeed * moveFactor;
                }
                else
                {
                    moveSpeed = walkSpeed * moveFactor;
                }
            }

            if (grounded)
            {
                if (player.GetButton(jumpButton) && !disableMovement)
                {
                    if (deathmatch)
                    {
                        rigidbody.velocity = new Vector3(rigidbody.velocity.x, deathmatchJumpSpeed, rigidbody.velocity.z);
                    }
                    else
                    {
                        rigidbody.velocity = new Vector3(rigidbody.velocity.x, jumpSpeed, rigidbody.velocity.z);
                    }

                    grounded = false;
                }

                Vector3 slopeDirection = Vector3.Cross(Vector3.up, Vector3.Cross(Vector3.up, raycastHit.normal));
                float slope = Vector3.Dot(moveDirection, slopeDirection) * 1.5f;

                if (slope < 0)
                {
                    slope = 0;
                }

                slope++;

                moveVelocity = moveDirection * moveSpeed * moveFactor * slope;
                rigidbody.velocity = moveVelocity + Vector3.up * rigidbody.velocity.y;
            }
            else
            {
                Vector3 planeVelocity = new Vector3(rigidbody.velocity.x, 0, rigidbody.velocity.z);
                Vector3 airVelocity = moveDirection * airMoveFactor * moveSpeed * moveFactor;

                float speed;

                if (sprinting)
                {
                    speed = sprintSpeed;
                }
                else
                {
                    if (deathmatch)
                    {
                        speed = deathmatchSpeed;
                    }
                    else
                    {
                        speed = walkSpeed;
                    }
                }

                float magnitude = (planeVelocity + airVelocity).magnitude;

                if (magnitude > speed)
                {
                    rigidbody.velocity = (planeVelocity + airVelocity).normalized * speed * moveFactor + Vector3.up * rigidbody.velocity.y;
                }
                else
                {
                    rigidbody.velocity += airVelocity;
                }
            }

            animator.SetBool("isGrounded", grounded);

            if (deathmatch)
            {
                bobAngle += moveMagnitude * deathmatchBobSpeed;
            }
            else
            {
                bobAngle += moveMagnitude * bobSpeed;
            }

            if (!grounded || moveVelocity.magnitude < 0.1f)
            {
                bobAngle = 0.0f;
            }

            Vector3 bobTarget = Vector3.zero;

            if (deathmatch)
            {
                int yAxis = 1;

                if (deathmatchBobSize.y < 0)
                {
                    yAxis = -1;
                }

                bobTarget = new Vector3(Mathf.Sin(bobAngle) * deathmatchBobSize.x * moveFactor, yAxis * Mathf.Abs(Mathf.Sin(bobAngle) * deathmatchBobSize.y * moveFactor), 0);
            }
            else
            {
                int yAxis = 1;

                if (bobSize.y < 0)
                {
                    yAxis = -1;
                }

                bobTarget = new Vector3(Mathf.Sin(bobAngle) * bobSize.x * moveFactor, yAxis * Mathf.Abs(Mathf.Sin(bobAngle) * bobSize.y * moveFactor), 0);
            }

            weaponTransform.transform.localPosition = Vector3.Lerp(weaponTransform.transform.localPosition, bobTarget, bobLerp * Time.deltaTime);

            float headRadius;
            float headSpeed;

            if (deathmatch)
            {
                headRadius = deathmatchHeadBobRadius;
                headSpeed = deathmatchHeadBobSpeed;
            }
            else
            {
                headRadius = headBobRadius;
                headSpeed = headBobSpeed;
            }

            cameras[0].transform.localPosition = Vector3.Lerp(cameras[0].transform.localPosition, Vector3.up * Mathf.Sin(bobAngle * headSpeed) * headRadius * moveSpeed + cameraDefaultPos, headBobLerp);
        }
        else
        {
            foreach (Camera camera in cameras)
            {
                if (killer != null)
                {
                    Quaternion startRotation = camera.transform.rotation;
                    Entity killerEntity = killer.GetComponent<Entity>();

                    if (killerEntity != null)
                    {
                        camera.transform.LookAt(killerEntity.GetCenter());
                    }

                    Quaternion targetRotation = camera.transform.rotation;
                    camera.transform.rotation = Quaternion.Lerp(camera.transform.rotation, targetRotation, deadCameraLerp);
                }
                else
                {
                    camera.transform.rotation = Quaternion.Lerp(camera.transform.rotation, Quaternion.Euler(0, camera.transform.eulerAngles.y, 0), deadCameraLerp);
                }

                camera.transform.eulerAngles = new Vector3(camera.transform.eulerAngles.x, camera.transform.eulerAngles.y, 0);
            }
        }
    }

    // Update is called once per frame
    public void Update()
    {
        if (IsAlive())
        {
            //cheats
            if (Input.GetKey(KeyCode.LeftControl))
            {
                if (Input.GetKey(KeyCode.C))
                {
                    FullyHeal();

                    if (points < 1000000)
                    {
                        points = 1000000;
                    }
                }

                if (Input.GetKey(KeyCode.G) && !deathmatch)
                {
                    god = true;
                    FullyHeal();
                }

                if (Input.GetKey(KeyCode.K))
                {
                    Kill();
                }
            }

            if ((!deathmatch && healthRegenTime < healthRegenTimer) || (deathmatch && healthRegenTime < deathmatchHealthRegenTimer))
            {
                healthRegenTime += Time.deltaTime;
            }
            else
            {
                if (deathmatch)
                {
                    Heal(deathmatchHealthRegenRate * maxHealth * Time.deltaTime);
                }
                else
                {
                    Heal(healthRegenRate * maxHealth * Time.deltaTime);
                }
            }

            if (invincibilityFrames > 0)
            {
                invincibilityFrames -= Time.deltaTime;
            }

            float verticalLook = player.GetAxis(verticalLookAxis);
            float horizontalLook = player.GetAxis(horizontalLookAxis);

            if (player.controllers.joystickCount != 0)
            {
                controller = true;

                if (gameController.playerCount == 1)
                {
                    player.controllers.Mouse.enabled = false;
                    player.controllers.Keyboard.enabled = false;
                }

                verticalLook *= controllerSettings.controllerSensitivity.y * Time.deltaTime;
                horizontalLook *= controllerSettings.controllerSensitivity.x * Time.deltaTime;

                if (controllerSettings.aimAssist)
                {
                    RaycastHit aimHit;

                    if (Physics.Raycast(cameras[0].transform.position + transform.forward * collider.radius, cameras[0].transform.forward, out aimHit))
                    {
                        Entity entity = aimHit.transform.GetComponentInParent<Entity>();

                        if (entity != null && weaponController.GetDamageFlags().flags[(int)entity.type] && entity.gameObject.GetInstanceID() != gameObject.GetInstanceID())
                        {
                            verticalLook *= aimAssistFactor.y;
                            horizontalLook *= aimAssistFactor.x;
                        }
                    }
                }
            }
            else
            {
                controller = false;

                player.controllers.Mouse.enabled = true;
                player.controllers.Keyboard.enabled = true;

                verticalLook *= controllerSettings.mouseSensitivity.y;
                horizontalLook *= controllerSettings.mouseSensitivity.x;
            }

            if (controllerSettings.invertX)
            {
                horizontalLook = -horizontalLook;
            }

            if (controllerSettings.invertY)
            {
                verticalLook = -verticalLook;
            }

            if (!isGamePaused && !disableMovement)
            {
                //adjust yaw and pitch based on input
                yaw += horizontalLook;
                //prevent camera from rotating past 180 degrees vertically
                pitch = Mathf.Min(maxYLookAngle, Mathf.Max(-maxYLookAngle, pitch - verticalLook));
            }

            Quaternion rotateTarget;

            if (disableMovement)
            {
                rotateTarget = Quaternion.Euler(0, 0, 0);
            }
            else
            {
                if (sprinting)
                {
                    rotateTarget = Quaternion.Euler(verticalLook * rotateSize.y + sprintWeaponRotate, horizontalLook * rotateSize.x, 0);
                }
                else
                {
                    rotateTarget = Quaternion.Euler(verticalLook * rotateSize.y, horizontalLook * rotateSize.x, 0);
                }
            }

            weaponTransform.transform.localRotation = Quaternion.Lerp(weaponTransform.transform.localRotation, rotateTarget, rotateLerp * Time.deltaTime);

            //apply new yaw and pitch
            foreach (Camera cam in cameras)
            {
                cam.transform.eulerAngles = new Vector3(pitch, yaw, 0.0f);
            }

            foreach (Text text in interactText)
            {
                text.color -= new Color32(0, 0, 0, (byte)(interactFadeSpeed * Time.deltaTime * 255));
            }

            RaycastHit hit;

            if (!player.GetButton(interactButton))
            {
                interactHold = 0;
            }

            foreach (Image circle in interactCircle)
            {
                circle.fillAmount = 0;
            } 

            if (weaponController.Idle())
            {
                if (Physics.Raycast(cameras[0].transform.position, cameras[0].transform.forward, out hit, interactDistance, interactLayer))
                {
                    if (hit.transform.gameObject.layer == LayerMask.NameToLayer("Interact"))
                    {
                        Store store = hit.transform.GetComponent<Store>();

                        if (store != null)
                        {
                            if (!weaponBought)
                            {
                                if (player.GetButton(interactButton))
                                {
                                    if (store.CanHold(this))
                                    {
                                        interactHold += Time.deltaTime;

                                        if (interactHold >= interactHoldTimer && !weaponBought)
                                        {
                                            weaponBought = store.Interact(this, true);
                                        }

                                        float interactFillAmount = interactHold / interactHoldTimer;

                                        foreach (Image circle in interactCircle)
                                        {
                                            circle.fillAmount = interactFillAmount;
                                        }
                                    }
                                }
                                else if (player.GetButtonUp(interactButton))
                                {
                                    store.Interact(this, false);
                                }
                            }

                            string outputText = store.GetOutputText(this);

                            foreach (Text text in interactText)
                            {
                                text.color = new Color32((byte)(text.color.r * 255), (byte)(text.color.g * 255), (byte)(text.color.b * 255), 255);
                                text.text = outputText;
                            }
                        }
                        else
                        {
                            idol = hit.transform.GetComponent<Idol>();

                            if (idol != null)
                            {
                                if (player.GetButtonUp(interactButton))
                                {
                                    idol.Interact();
                                    weaponController.GrabIdol();
                                    waveSpawner = idol.waveSpawner;
                                }

                                foreach (Text text in interactText)
                                {
                                    text.color = new Color32((byte)(text.color.r * 255), (byte)(text.color.g * 255), (byte)(text.color.b * 255), 255);
                                    text.text = idol.text + "\nPress " + GetButtonName(interactButton);
                                }
                            }
                            else
                            {
                                interactHold = 0;
                            }
                        }
                    }
                }

                if (weaponBought && !player.GetButton(interactButton))
                {
                    weaponBought = false;
                }
            }

            if (player.GetButtonUp(pauseButton) && !disableMovement)
            {
                if (isGamePaused)
                {
                    Resume();
                }
                else
                {
                    Pause();
                }
            }
        }
        else
        {
            if (!gameController.matchEnd)
            {
                string deathText = "Respawning in " + gameController.GetRespawnTime(playerID - 1);

                foreach (Text text in interactText)
                {
                    text.text = deathText;
                    text.color = new Color32((byte)(text.color.r * 255), (byte)(text.color.g * 255), (byte)(text.color.b * 255), 255);
                }
            }

            /*if (player.GetButtonUp(interactButton))
            {
                foreach (Text text in interactText)
                {
                    text.gameObject.SetActive(false);
                }

                nameObject.SetActive(true);
                Cursor.lockState = CursorLockMode.Confined;
                Cursor.visible = true;
            }*/
        }

        float currentHealth = GetHealth();

        healthUI = Mathf.Lerp(healthUI, currentHealth, textLerp * Time.deltaTime);

        damageIndicator.color = new Color32(255, 255, 255, (byte)(((maxHealth - healthUI) / maxHealth) * 255));

        if (Mathf.Abs(currentHealth - healthUI) < 0.5f)
        {
            healthUI = currentHealth;
        }

        int healthUIInt = (int)healthUI;

        if (healthUIInt == 0 && healthUI > 0)
        {
            healthUIInt = 1;
        }

        foreach (Text text in healthText)
        {
            text.text = healthUIInt.ToString();
        }

        float healthPercentage = healthUI / maxHealth;

        if (healthPercentage < 0)
        {
            healthBar.fillAmount = 0;
        }
        else
        {
            healthBar.fillAmount = healthPercentage;
        }

        pointsUI = Mathf.Lerp(pointsUI, points, textLerp * Time.deltaTime);

        if (Mathf.Abs(points - pointsUI) < 0.5f)
        {
            pointsUI = points;
        }

        foreach (Text text in pointText)
        {
            text.text = ((int)pointsUI).ToString();
        }

        //if(controllerSettings.controllerConnected && !isGamePaused)
        //{
        //    Cursor.lockState = CursorLockMode.Locked;
        //    Cursor.visible = false;
        //}
    }

    public int GetPoints()
    {
        return points;
    }

    public uint GetTotalPoints()
    {
        return totalPoints;
    }

    public void GivePoints(uint givePoints)
    {
        // Instantiate Prefab assign text to be givePoints
        PlayerCanvas.instance.InstantiateAddedPoints(givePoints);
        points += (int)givePoints;
        totalPoints += givePoints;
    }

    public bool TakePoints(uint takePoints)
    {
        if (takePoints > points)
        {
            return false;
        }

        points -= (int)takePoints;

        return true;
    }

    public override void Damage(float damageAmount, GameObject attacker = null, bool selfDamage = false)
    {
        if ((selfDamage || attacker.GetInstanceID() != gameObject.GetInstanceID()) && !god && !disableMovement && (invincibilityFrames <= 0 || deathmatch) && GetHealth() > 0)
        {
            damageAmount *= 1 - weaponController.GetDamageResistance();
            base.Damage(damageAmount, attacker, selfDamage);

            AudioRandomiser.PlayAudio(audioSource, hurtSounds);

            healthRegenTime = 0;
            invincibilityFrames = invincibilityFramesTimer;

            //flash red screen here for better damage perception
            hitIndicator.GetComponent<Animator>().SetTrigger("WasHit");
        }
    }

    public override void Die(GameObject attacker = null)
    {
        animator.enabled = false;
        collider.enabled = false;
        rigidbody.isKinematic = true;

        weaponController.gameObject.SetActive(false);

        /*foreach (Camera cam in cameras)
        {
            cam.transform.SetParent(spineJoint);
        }*/

        foreach (Rigidbody rigidbody in ragdollRigidbodies)
        {
            rigidbody.isKinematic = false;
        }

        foreach (CapsuleCollider collider in ragdollCapsules)
        {
            collider.enabled = true;
        }

        foreach (BoxCollider collider in ragdollBoxes)
        {
            collider.enabled = true;
        }

        if (attacker != null && attacker.GetInstanceID() != gameObject.GetInstanceID())
        {
            killer = attacker;
        }

        if (deathmatch)
        {
            if (attacker != null)
            {
                PlayerController fragger = attacker.GetComponent<PlayerController>();

                if (fragger != null)
                {
                    gameController.DisplayDeath(fragger.GetName(), GetName(), fragger.weaponController.GetCurrentWeaponIndex());

                    if (fragger.playerID == playerID)
                    {
                        points -= (int)gameController.playerSuicideTakePoints;
                    }
                    else
                    {
                        fragger.points += (int)gameController.playerKillAddPoints;
                        points -= (int)gameController.playerKillTakePoints;

                        gameController.AddFrag(fragger.playerID - 1);
                    }
                }
                else
                {
                    points -= (int)gameController.enemyKillTakePoints;
                }
            }
        }

        gameController.AddDeath(playerID - 1);

        foreach (Text text in weaponController.ammoText)
        {
            text.text = "";
        }

        /*if (!deathmatch)
        {
            string deathText = "Score: " + totalPoints.ToString() + "\n\nPress " + GetButtonName(interactButton) + " to Enter Your Name";

            foreach (Text text in interactText)
            {
                text.text = deathText;
                text.color = new Color32((byte)(text.color.r * 255), (byte)(text.color.g * 255), (byte)(text.color.b * 255), 255);
            }
        }*/

        spawner = FindObjectOfType<WaveSpawner>();
        gameController.PlayerDie((int)playerID);
    }

    public void RegisterKill()
    {
        if (!deathmatch)
        {
            gameController.AddFrag(playerID - 1);
        }
    }

    public void Respawn()
    {
        Init();

        animator.enabled = true;
        collider.enabled = true;
        rigidbody.isKinematic = false;

        foreach (Text text in interactText)
        {
            text.text = "";
            text.color = new Color32((byte)(text.color.r * 255), (byte)(text.color.g * 255), (byte)(text.color.b * 255), 255);
        }

        foreach (Camera cam in cameras)
        {
            cam.transform.SetParent(transform);
        }

        foreach (Rigidbody rigidbody in ragdollRigidbodies)
        {
            rigidbody.isKinematic = true;
        }

        foreach (CapsuleCollider collider in ragdollCapsules)
        {
            collider.enabled = false;
        }

        foreach (BoxCollider collider in ragdollBoxes)
        {
            collider.enabled = false;
        }

        weaponController.gameObject.SetActive(true);
        weaponController.Respawn();
    }

    public string GetName()
    {
        return playerName;
    }

    public string GetButtonName(string button)
    {
        try
        {
            return player.controllers.maps.GetFirstElementMapWithAction(button, true).elementIdentifierName;
        }
        catch
        {
            return "Error";
        }
    }

    public float GetMoveFactor()
    {
        return moveFactor;
    }

    public void Resume()
    {
        pauseMenu.gameObject.SetActive(false);
        playerCanvas.gameObject.SetActive(true);
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        Time.timeScale = 1f;
        isGamePaused = false;
    }

    public void LoadSceneByName(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }

    public void Pause()
    {
        playerCanvas.gameObject.SetActive(false);
        pauseMenu.gameObject.SetActive(true);
        Cursor.lockState = CursorLockMode.Confined;
        Cursor.visible = false;
        Time.timeScale = 0f;
        isGamePaused = true;
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void HighScore()
    {
        string name = "";

        for (int i = 0; i < letters.Count; i++)
        {
            name = name += letters[i].text;
        }

        if (FindObjectOfType<WaveSpawner>().timedWaves)
        {
            SwarmHighScore.highScores.Add(new highScore { name = name, score = (int)totalPoints });
            SwarmHighScore.sortHighScores();

            if (SwarmHighScore.highScores.Capacity > highscoreCapacity)
            {
                SwarmHighScore.sortHighScores();
            }
        }
        else
        {
            WaveHighScore.highScores.Add(new highScore { name = name, score = FindObjectOfType<WaveSpawner>().currentWave });
            WaveHighScore.sortHighScores();

            if (WaveHighScore.highScores.Capacity > highscoreCapacity)
            {
                WaveHighScore.sortHighScores();
            }
        }

        SceneManager.LoadScene(0);
    }
}