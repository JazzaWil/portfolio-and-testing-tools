﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerCanvas : MonoBehaviour
{
    public static PlayerCanvas instance;

    public Transform pointToSpawnPoints;
    public GameObject pointsToInstantiate;

    private void Awake()
    {
        instance = this;
    }

    public void InstantiateAddedPoints(uint pointsToAdd)
    {
        GameObject pointFeedback = Instantiate(pointsToInstantiate, pointToSpawnPoints);
        pointFeedback.transform.localPosition = Vector3.zero;
        pointFeedback.GetComponentInChildren<TMPro.TextMeshProUGUI>().text = "+" + pointsToAdd;
    }
}