﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using TMPro;
using Rewired;
public class settingsMenu : MonoBehaviour
{
    public AudioMixer audioMixer;
    public optionsVariables options;
    public TMP_Dropdown qualityDropDown;
    public TMP_Dropdown resolutionDropDown;
    public TMP_InputField inputX;
    public TMP_InputField inputY;

    public bool controllerConnected;

    List<Resolution> resolutions = new List<Resolution>();

    public void EnableTimedMode()
    {
        options.timedMode = true;
    }

    public void DisableTimedMode()
    {
        options.timedMode = false;
    }

    public void SetVolume(float volume)
    {
        audioMixer.SetFloat("Volume", volume);
    }

    public void SetQuality(int index)
    {
        QualitySettings.SetQualityLevel(index);
    }

    public void SetResolution(int index)
    {
        Resolution resolution = resolutions[index];
        Screen.SetResolution(resolution.width, resolution.height, Screen.fullScreen);
    }

    public void ChangeSensitiviityX()
    {
        if(controllerConnected)
        {
            options.controllerSensitivity.x = float.Parse(inputX.text, System.Globalization.NumberStyles.AllowDecimalPoint); ;
        }
        else
        {
            options.mouseSensitivity.x = float.Parse(inputX.text, System.Globalization.NumberStyles.AllowDecimalPoint); ;
        }
    }

    public void ChangeSensitiviityY()
    {
        if (controllerConnected)
        {
            options.controllerSensitivity.y = float.Parse(inputY.text, System.Globalization.NumberStyles.AllowDecimalPoint); ;
        }
        else
        {
            options.mouseSensitivity.y = float.Parse(inputY.text, System.Globalization.NumberStyles.AllowDecimalPoint); ;
        }
    }

    public void updateSensitivityX()
    {
        if (controllerConnected)
        {
           inputX.text = options.controllerSensitivity.x.ToString();
        }
        else
        {
            inputX.text = options.mouseSensitivity.x.ToString();
        }
    }

    public void updateSensitivityY()
    {
        if (controllerConnected)
        {
            inputY.text = options.controllerSensitivity.y.ToString();
        }
        else
        {
            inputY.text = options.mouseSensitivity.y.ToString();
        }
    }

    //Aim Assist
    //Invert

    private void Start()
    {
        Cursor.lockState = CursorLockMode.Confined;
        Cursor.visible = true;

        resolutions.AddRange(Screen.resolutions);

        qualityDropDown.options.Clear();
        resolutionDropDown.options.Clear();

        List<TMPro.TMP_Dropdown.OptionData> optionsList = new List<TMP_Dropdown.OptionData>();
        List<TMPro.TMP_Dropdown.OptionData> resolutionsList = new List<TMP_Dropdown.OptionData>();

        foreach (string name in QualitySettings.names)
        {
            TMPro.TMP_Dropdown.OptionData option = new TMP_Dropdown.OptionData();
            option.text = name;
            optionsList.Add(option);
        }
        int currenResolution = 0;
        for (int i = 0; i < resolutions.Count; i++)
        {
            TMPro.TMP_Dropdown.OptionData resolution = new TMP_Dropdown.OptionData();
            resolution.text = resolutions[i].width + "  X " + resolutions[i].height;
            resolutionsList.Add(resolution);
             
            if(resolutions[i].width == Screen.currentResolution.width && 
               resolutions[i].height == Screen.currentResolution.height)
            {
                currenResolution = i;
            }
        }

        qualityDropDown.AddOptions(optionsList);
        resolutionDropDown.AddOptions(resolutionsList);
        resolutionDropDown.value = currenResolution;
        resolutionDropDown.RefreshShownValue();
    }
}
