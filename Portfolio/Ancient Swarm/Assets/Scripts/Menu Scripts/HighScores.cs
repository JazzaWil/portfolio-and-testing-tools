﻿using UnityEngine;
using System.Collections.Generic;
using System;
using ProtoBuf;

[CreateAssetMenu(fileName = "_highscores", menuName = "HighScore")][Serializable][ProtoContract]
public class HighScores : ScriptableObject
{
    [ProtoMember(1)]
    public List<highScore> highScores = new List<highScore>();
    //public List<Enemies> enemyList = new List<Enemies>();

    public void sortHighScores()
    {
        for (int i = 0; i < highScores.Count; i++)
            for (int j = 0; j < highScores.Count; j++)
                if (highScores[i].score > highScores[j].score)
                {
                    // swap temp and newHighscore[i] 
                    highScore temp = highScores[i];
                    highScores[i] = highScores[j];
                    highScores[j] = temp;
                }
    }
}
[Serializable][ProtoContract]
public class highScore
{
    [ProtoMember(1)]
    public string name;
    [ProtoMember(2)]
    public int score;
}