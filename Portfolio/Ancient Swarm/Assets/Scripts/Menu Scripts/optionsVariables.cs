﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "_menuSettings", menuName = "Settings/Options Settings", order = 1)]

public class optionsVariables : ScriptableObject
{
    public Vector2 mouseSensitivity;
    public Vector2 controllerSensitivity;

    public bool invertX;
    public bool invertY;
    public bool aimAssist;
    public bool vibration;

    public bool timedMode;
    public bool controllerConnected;

}
