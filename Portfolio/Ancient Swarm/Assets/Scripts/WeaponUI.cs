﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeaponUI : MonoBehaviour
{
    public float fadeSpeed;
    public Color emptyColor;
    public RawImage background;
    public RawImage foreground;
    public RawImage button;

    public Sprite[] icons;

    private Color defaultColor;
    private uint index = 0;

    // Start is called before the first frame update
    void Awake()
    {
        defaultColor = new Color(foreground.color.r, foreground.color.g, foreground.color.b, 1);

        background.color = new Color(background.color.r, background.color.g, background.color.b, 0);
        foreground.color = new Color(foreground.color.r, foreground.color.g, foreground.color.b, 0);

        if (button != null)
        {
            button.color = new Color(button.color.r, button.color.g, button.color.b, 0);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (background.color.a > 0)
        {
            background.color = new Color(background.color.r, background.color.g, background.color.b, background.color.a - (fadeSpeed * Time.deltaTime));
            foreground.color = new Color(foreground.color.r, foreground.color.g, foreground.color.b, background.color.a - (fadeSpeed * Time.deltaTime));

            if (button != null)
            {
                button.color = new Color(button.color.r, button.color.g, button.color.b, background.color.a - (fadeSpeed * Time.deltaTime));
            }
        }
    }

    public void UpdateIcon(uint newIndex, bool empty, bool show = true)
    {
        index = newIndex;

        if (icons[index].texture == null)
        {
            background.texture = null;
        }
        else
        {
            background.texture = icons[index].texture;
        }

        foreground.texture = background.texture;

        if (show)
        {
            background.color = new Color(background.color.r, background.color.g, background.color.b, 1);

            if (empty && newIndex != 0)
            {
                foreground.color = emptyColor;
            }
            else
            {
                foreground.color = defaultColor;
            }

            if (button != null)
            {
                button.color = new Color(button.color.r, button.color.g, button.color.b, 1);
            }
        }
    }
}