﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class changeText : MonoBehaviour
{
    public string usableLetters = "abcdefghijklmnopqrstuvwxyz";
    public int index = 0;
    public Text myText;

    public void Up()
    {
        index += 1;
        if (index > usableLetters.Length - 1)
        {
            index = 0;
        }
    }

    public void Down()
    {
        index -= 1;
        if (index < 0)
        {
            index = usableLetters.Length - 1;
        }
    }
    private void Start()
    {
        myText = GetComponent<Text>();
    }

    private void Update()
    {
        if (index > usableLetters.Length - 1)
        {
            index = 0;
        }
        if (index < 0)
        {
            index = usableLetters.Length - 1;

        }
        myText.text = usableLetters[index].ToString();

    }
}
