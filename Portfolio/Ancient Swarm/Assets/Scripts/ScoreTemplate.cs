﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreTemplate : MonoBehaviour
{
    public Text playerName;
    public Text score;
    public Text kills;
    public Text deaths;
}