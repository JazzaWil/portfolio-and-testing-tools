﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Rewired;

public class NameSelect : MonoBehaviour
{
    public uint index;
    public string valueInputAxis = "Vertical";

    private MultiplayerSetup multiplayerSetup;
    private Selectable selectable;
    private bool selected = false;

    // Start is called before the first frame update
    void Start()
    {
        multiplayerSetup = GetComponentInParent<MultiplayerSetup>();
        selectable = GetComponent<Selectable>();
    }

    // Update is called once per frame
    void Update()
    {
        if (selected)
        {
            if (multiplayerSetup.player.GetButtonUp(valueInputAxis))
            {
                if (multiplayerSetup.player.GetAxis(valueInputAxis) < 0)
                {
                    multiplayerSetup.IncreaseName(index);
                }
                else
                {
                    multiplayerSetup.DecreaseName(index);
                }
            }
        }
    }

    public void OnSelect(BaseEventData eventData)
    {
        selected = true;
    }

    public void OnDeselect(BaseEventData eventData)
    {
        selected = false;
    }
}
