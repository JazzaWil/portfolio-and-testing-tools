﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
public class Traps : MonoBehaviour
{
    public enum Trap
    {
        ClosingDoorTrap,
        ArrowDoorTrap,
        SpikedFloorsTrap,
        TrapDoorTrap
    }

    public Trap trap = Trap.ArrowDoorTrap;

    public bool test;

    public bool inUse;


    // Closing Door Variables

    [ShowIf("ClosingDoorTrap")]
    public Vector3 startPosition;

    [ShowIf("ClosingDoorTrap")]
    public Transform otherWall;

    [ShowIf("ClosingDoorTrap")]
    public float howLongUntilReOpen;

    [ShowIf("ClosingDoorTrap")]
    public float howLongUntilClose;

    //Arrow Door Variables
    [ShowIf("ArrowDoorTrap")]
    public List<GameObject> shootingPoints = new List<GameObject>();

    [ShowIf("ArrowDoorTrap")]
    public GameObject projectile;

    [ShowIf("ArrowDoorTrap")]
    public float lengthOfShoot;

    [ShowIf("ArrowDoorTrap")]
    public float bulletSpeed;

    [ShowIf("ArrowDoorTrap")]
    public float delayOfShoot;

    [ShowIf("ArrowDoorTrap")]
    public DamageFlags damageFlag;

    [ShowIf("ArrowDoorTrap")]
    public float damage;

    [ShowIf("ArrowDoorTrap")]
    public float force;


    // Spiked Floor Variables

    [ShowIf("SpikedFloorsTrap")]
    public List<Transform> spikeLocations = new List<Transform>();

    [ShowIf("SpikedFloorsTrap")]
    public float waitTimeBetweenUpAndDown;

    [ShowIf("SpikedFloorsTrap")][Slider(0, 1)]
    public float Y;

    [ShowIf("SpikedFloorsTrap")]
    public int numOfSpikesThatGoUp;

    [ShowIf("SpikedFloorsTrap")]
    public int numOfTimesSpikesTrigger;

    [ShowIf("SpikedFloorsTrap")]
    List<Transform> spikeList = new List<Transform>();

    [ShowIf("SpikedFloorsTrap")]
    List<Vector3> spikeListStartPosition = new List<Vector3>();

    [ShowIf("SpikedFloorsTrap")]
    List<Vector3> spikeListEndPosition = new List<Vector3>();

    // Pit Fall Variables

    [ShowIf("TrapDoorTrap")]
    public GameObject trapDoor;

    [ShowIf("TrapDoorTrap")]
    public float waitTime;

    [ShowIf("TrapDoorTrap")]
    public Vector3 goalRotation;

    [ShowIf("TrapDoorTrap")]
    public BoxCollider trapCollider;

    Vector3 startRotation;


    // Start is called before the first frame update
    void Start()
    {
        switch (trap)
        {
            case Trap.ClosingDoorTrap:
                startPosition = transform.position;
                break;
            case Trap.TrapDoorTrap:
                startRotation = trapDoor.transform.rotation.eulerAngles;
                //trapCollider.enabled = false;
                break;
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (test)
        {
            switch (trap)
            {
                case Trap.ClosingDoorTrap:
                    CloseDoors();
                    break;
                case Trap.ArrowDoorTrap:
                    ShootArrows();
                    break;
            }
        }
        if (numOfSpikesThatGoUp >= spikeLocations.Count)
        {
            numOfSpikesThatGoUp -= 1;
        }

    }

    public void Activate()
    {
        switch (trap)
        {
            case Trap.ClosingDoorTrap:
                StartCoroutine("ClosingDoor");
                break;
            case Trap.ArrowDoorTrap:
                StartCoroutine("ShootingArrows");
                break;
            case Trap.SpikedFloorsTrap:
                StartCoroutine("RaiseSpikes");
                break;
            case Trap.TrapDoorTrap:
                StartCoroutine("TrapDoor");
                break;
        }
    }

    [Button]
    public void CloseDoors()
    {
        StartCoroutine("ClosingDoors");
    }

    [Button]
    public void ShootArrows()
    {
        StartCoroutine("ShootingArrows");
    }

    [Button]
    public void RaiseSpikes()
    {
        StartCoroutine("SpikedFloor");
    }

    [Button]
    public void RotateDoor()
    {
        StartCoroutine("TrapDoor");
    }



    public IEnumerator ClosingDoors()
    {
        inUse = true;
        // close over the course of one second
        float alpha = 0;
        while (alpha < 1)
        {
            transform.position = Vector3.Lerp(startPosition, otherWall.position, alpha);
            alpha += Time.deltaTime / howLongUntilClose;
            yield return new WaitForEndOfFrame();
        }

        // pause for <howlonguntilrepopen> seconds
        yield return new WaitForSeconds(howLongUntilReOpen);

        // close over the course of one second
        alpha = 0;
        while (alpha < 1)
        {
            transform.position = Vector3.Lerp(otherWall.position, startPosition, alpha);
            alpha += Time.deltaTime / howLongUntilClose;
            yield return new WaitForEndOfFrame();
        }
        inUse = false;
    }
    public IEnumerator ShootingArrows()
    {
        inUse = true;
        for (int i = 0; i < lengthOfShoot; i++)
        {
            Shoot();
            yield return new WaitForSeconds(delayOfShoot);
        }
        inUse = false;
    }
    public IEnumerator SpikedFloor()
    {
        inUse = true;
        for (int b = 0; b < numOfTimesSpikesTrigger; b++)
        {

            spikeList = new List<Transform>();

            // grab random spikes
            int lastNumber = 0;
            int newNumber = 0;
            List<int> val = new List<int>();
            while (val.Count != numOfSpikesThatGoUp)
            {
                newNumber = Random.Range(0, spikeLocations.Count);
                if (newNumber != lastNumber)
                {
                    val.Add(newNumber);
                    lastNumber = newNumber;
                }
            }

            for (int i = 0; i < val.Count; i++)
            {
                spikeList.Add(spikeLocations[val[i]]);
            }

            spikeListStartPosition = new List<Vector3>();
            //Create The starting positions
            for (int i = 0; i < spikeList.Count; i++)
            {
                spikeListStartPosition.Add(spikeList[i].position);
            }

            spikeListEndPosition = new List<Vector3>();
            //Create The end positions
            for (int i = 0; i < spikeList.Count; i++)
            {
                spikeListEndPosition.Add(spikeList[i].position);
                Vector3 temp = spikeListStartPosition[i];
                temp += new Vector3(0, Y, 0);
                spikeListEndPosition[i] = temp;
            }

            float alpha = 0;
            while (alpha < 1)
            {
                alpha += Time.deltaTime / waitTimeBetweenUpAndDown;
                if (alpha > 1)
                    alpha = 1;
                for (int i = 0; i < spikeList.Count; i++)
                {
                    spikeList[i].position = Vector3.Lerp(spikeListStartPosition[i], spikeListEndPosition[i], alpha);
                }
                yield return new WaitForEndOfFrame();
            }

            yield return new WaitForSeconds(waitTimeBetweenUpAndDown);

            alpha = 0;
            while (alpha < 1)
            {
                alpha += Time.deltaTime / waitTimeBetweenUpAndDown;
                if (alpha > 1)
                    alpha = 1;
                for (int i = 0; i < spikeList.Count; i++)
                {
                    spikeList[i].position = Vector3.Lerp(spikeListEndPosition[i], spikeListStartPosition[i], alpha);
                }
                
                yield return new WaitForEndOfFrame();
            }
        }
        inUse = false;
    }
    public IEnumerator TrapDoor()
    {
        inUse = true;
        trapCollider.enabled = true;
        // close over the course of one second
        float alpha = 0;
        while (alpha < 1)
        {
            trapDoor.transform.rotation = Quaternion.Lerp(Quaternion.Euler(startRotation), Quaternion.Euler(goalRotation), alpha);
            alpha += Time.deltaTime / waitTime;
            yield return new WaitForSeconds(.00001f);
        }

        // pause for <howlonguntilrepopen> seconds
        yield return new WaitForSeconds(waitTime);

        // close over the course of one second
        alpha = 0;
        while (alpha < 1)
        {
            trapDoor.transform.rotation = Quaternion.Lerp(Quaternion.Euler(goalRotation), Quaternion.Euler(startRotation), alpha);
            alpha += Time.deltaTime / waitTime;
            yield return new WaitForSeconds(.00001f);
        }
        inUse = false;
        trapCollider.enabled = false;

    }

    private bool ClosingDoorTrap()
    {
        return trap == Trap.ClosingDoorTrap;
    }
    private bool ArrowDoorTrap()
    {
        return trap == Trap.ArrowDoorTrap;
    }
    private bool SpikedFloorsTrap()
    {
        return trap == Trap.SpikedFloorsTrap;
    }
    private bool TrapDoorTrap()
    {
        return trap == Trap.TrapDoorTrap;
    }

    void Shoot()
    {
        //Instanciate Bullet
        for (int i = 0; i < shootingPoints.Count; i++)
        {
            GameObject tempBullet = Instantiate(projectile, shootingPoints[i].transform.position, Quaternion.identity) as GameObject;
            tempBullet.transform.rotation = Quaternion.Euler(0, 0, 0);
            tempBullet.GetComponent<Projectile>().Init(damageFlag, damage, force, gameObject);
            Rigidbody tempRigidbody = tempBullet.GetComponent<Rigidbody>();
            tempRigidbody.AddForce(transform.forward * bulletSpeed);
            Destroy(tempBullet, .2f);
        }
    }

}